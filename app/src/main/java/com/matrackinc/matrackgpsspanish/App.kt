package com.matrackinc.matrackgpsspanish

import android.content.Context
import android.content.SharedPreferences
import com.matrackinc.matrackgpsspanish.db.DBConnection

class App : android.support.multidex.MultiDexApplication() {

    override fun onCreate() {

        super.onCreate()
        val settings = getSharedPreferences(PREFS_NAME, 0)
        val editor = settings.edit()
        editor.putBoolean("logEnabled", false)
        editor.commit()
        context = this
        dbConnection = DBConnection(context)
    }

    override fun onTerminate() {
        dbConnection.closeDB()
        super.onTerminate()
    }

    companion object {
        val PREFS_NAME = "logPrefFile"
        lateinit var dbConnection: DBConnection
        lateinit var context: Context
    }
}