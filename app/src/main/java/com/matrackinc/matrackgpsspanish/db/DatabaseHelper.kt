package com.matrackinc.matrackgpsspanish.db

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.util.Log

import com.matrackinc.matrackgpsspanish.App

class DatabaseHelper {

    val client: String
        get() {
            val db = App.dbConnection.readableDatabaseInstance
            val cursor = db.rawQuery("SELECT EMAIL FROM $TABLE_NAME WHERE id = 0 ;", null)
            var client = ""
            if (cursor != null && cursor.moveToFirst()) {
                client = cursor.getString(cursor.getColumnIndex(EMAIL))
            }
            db.close()
            return client
        }

    val passcode: String
        get() {
            val db = App.dbConnection.readableDatabaseInstance
            val cursor = db.rawQuery("SELECT PASSCODE FROM $TABLE_NAME WHERE id = 0;", null)
            var client = ""
            if (cursor != null && cursor.moveToFirst()) {
                client = cursor.getString(cursor.getColumnIndex(PASSCODE))
            }
            db.close()
            return client
        }

    val authcode: String
        get() {
            val db = App.dbConnection.readableDatabaseInstance
            val cursor = db.rawQuery("SELECT AUTHCODE FROM $TABLE_NAME WHERE id = 0 ;", null)
            var client = ""
            if (cursor != null && cursor.moveToFirst()) {
                client = cursor.getString(cursor.getColumnIndex(AUTHCODE))
            }
            db.close()
            return client
        }

    fun DatabaseHelper() {

    }

    fun setFlag(flag: Int?, email: String, passcode: String, authcode: String, trackpersmission: String, reportpermission: String): Boolean {
        val db = App.dbConnection.writableDatabaseInstance
        val contentValues = ContentValues()
        contentValues.put(ID, 0)
        contentValues.put(FLAG, flag)
        contentValues.put(EMAIL, email)
        contentValues.put(PASSCODE, passcode)
        contentValues.put(AUTHCODE, authcode)
        contentValues.put(TRACKPERMISSION, trackpersmission)
        contentValues.put(REPORTPERMISSION, reportpermission)
        val result = db.update(TABLE_NAME, contentValues, "id = 0", null).toLong()
        db.close()
        return if (result.equals(-1)) {
            false
        } else {
            true
        }
    }

    fun getData() {
        val db = App.dbConnection.readableDatabaseInstance
        val contentValues = ContentValues()
    }

    fun gettrackpermission(): String {
        val db = App.dbConnection.readableDatabaseInstance
        val cursor = db.rawQuery("SELECT TRACKPERMISSION FROM $TABLE_NAME WHERE id = 0 ;", null)
        var permission = ""
        if (cursor != null && cursor.moveToFirst()) {
            permission = cursor.getString(cursor.getColumnIndex(TRACKPERMISSION))
        }
        db.close()
        return permission
    }

    fun getreportpermission(): String {
        val db = App.dbConnection.readableDatabaseInstance
        val cursor = db.rawQuery("SELECT REPORTPERMISSION FROM $TABLE_NAME WHERE id = 0 ;", null)
        var permission = ""
        if (cursor != null && cursor.moveToFirst()) {
            permission = cursor.getString(cursor.getColumnIndex(REPORTPERMISSION))
        }
        db.close()
        return permission
    }

    fun checkFlag(): Boolean {
        val db = App.dbConnection.readableDatabaseInstance
        val cursor = db.rawQuery("SELECT FLAG FROM $TABLE_NAME WHERE id = 0 ;", null)

        if (cursor != null && cursor.moveToFirst()) {
            Log.d("DBhelper", cursor.getString(cursor.getColumnIndex("FLAG")))

            if (cursor.getString(cursor.getColumnIndex("FLAG")) == "1") {
                Log.d("DBhelper", cursor.getString(cursor.getColumnIndex("FLAG")))
                return true
            } else {
                return false
            }
        }
        return false
    }

    companion object {
        val DATABASE_NAME = "MatrackgpsFlag.db"
        val TABLE_NAME = "MATRACKGPS_FLAG_TABLE"
        val ID = "ID"
        val EMAIL = "EMAIL"
        val TRACKPERMISSION = "TRACKPERMISSION"
        val REPORTPERMISSION = "REPORTPERMISSION"
        val PASSCODE = "PASSCODE"
        val FLAG = "FLAG"
        val AUTHCODE = "AUTHCODE"
        var db: SQLiteDatabase? = null

        fun create(db: SQLiteDatabase) {
            val res = db.rawQuery("SELECT name FROM sqlite_master WHERE type='table' AND name='$TABLE_NAME'", null)
            if (res.count == 0) {
                db.execSQL("CREATE TABLE $TABLE_NAME(ID INTEGER, FLAG INTEGER, EMAIL VARCHAR, PASSCODE VARCHAR, AUTHCODE VARCHAR, TRACKPERMISSION VARCHAR, REPORTPERMISSION VARCHAR)")
                val contentValues = ContentValues()
                contentValues.put(ID, 0)
                contentValues.put(FLAG, 0)
                contentValues.put(EMAIL, "")
                contentValues.put(PASSCODE, "")
                contentValues.put(AUTHCODE, "")
                contentValues.put(TRACKPERMISSION, "")
                contentValues.put(REPORTPERMISSION, "")
                val result = db.insert(TABLE_NAME, null, contentValues)
            }
        }
    }
}
