package com.matrackinc.matrackgpsspanish.db

import android.content.Context
import android.database.SQLException
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.util.Log

import com.matrackinc.matrackgpsspanish.db.DatabaseHelper

class DBConnection(context: Context) : SQLiteOpenHelper(context, DBNAME, null, DATABASE_VERSION) {

    private val myDataBase: SQLiteDatabase? = null

    val writableDatabaseInstance: SQLiteDatabase
        get() = this.writableDatabase

    val readableDatabaseInstance: SQLiteDatabase
        get() = this.readableDatabase

    init {
    }

    // Creating database
    override fun onCreate(db: SQLiteDatabase) {
        Log.i("dbcreate", "db_version: " + db.version)
        DatabaseHelper.create(db)
    }

    // Upgrading database
    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {

    }

    // closing database
    fun closeDB() {
        val db = this.readableDatabase
        if (db != null && db.isOpen)
            db.close()
    }

    @Synchronized
    override fun close() {

        myDataBase?.close()
        super.close()
    }

    companion object {

        // All Static variables
        // Database Version
        private val DATABASE_VERSION = 4

        //The Android's default system path of your application database.
        var DBNAME = "androidmatrackgps.db.sql"
    }
}

