package com.matrackinc.matrackgpsspanish.activity

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.support.design.widget.NavigationView
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.*
import android.widget.*
import com.matrackinc.matrackgpsspanish.App
import com.matrackinc.matrackgpsspanish.R
import com.matrackinc.matrackgpsspanish.db.DatabaseHelper
import com.matrackinc.matrackgpsspanish.fragment.*
import java.util.*

class HomeActivity : AppCompatActivity() {

    private var navigationView: NavigationView? = null
    private var drawer: DrawerLayout? = null
    private var navHeader: View? = null
    private var imgNavHeaderBg: ImageView? = null
    private var imgProfile: ImageView? = null
    private var txtName: TextView? = null
    private var txtWebsite: TextView? = null
    private var toolbar: Toolbar? = null
    var isFragmentRendered: Boolean? = null

    internal lateinit var myDB: DatabaseHelper

    // flag to load home fragment when user presses back key
    private val shouldLoadHomeFragOnBackPress = true
    private var mHandler: Handler? = null
    var currentInstanceState: Bundle? = null

    private// logout fragment
    val homeFragment: Fragment
        get() {
            val CUR_TAG = pref.getString("CURRENT_TAG", TAG_HOME)
            when (CUR_TAG) {
                TAG_HELP -> {
                    return help()
                }
                TAG_HOME -> {
                    editor.putString("CURRENT_TAG", TAG_HOME)
                    editor.commit()
                    return home()
                }
                TAG_REPORTS -> {
                    return reports()
                }
                TAG_DTC -> {
                    return dtc()
                }
                TAG_DTC_FORM -> {
                    return dtcform()
                }
                TAG_INSTA -> {
                    return instafence()
                }
                TAG_INSTA_FORM -> {
                    return instafenceform()
                }
                TAG_FUELREPORT -> {
                    return fuelreport()
                }
                TAG_FUELREPORT_FORM -> {
                    return fuelreportform()
                }
                TAG_MILEAGE -> {
                    return mileage()
                }
                TAG_MILEAGE_FORM -> {
                    return mileageform()
                }
                TAG_TRAVELLOG -> {
                    return travellog()
                }
                TAG_TRAVELLOG_FORM -> {
                    return travellogform()
                }
                TAG_TRACKHISTORY -> {
                    return trackhistory_clustering()
                }
                TAG_TRACKHISTORY_FORM -> {
                    return trackhistoryform()
                }
                TAG_LOGOUT -> logoutAlert()
                else -> return home()
            }
            return home()
        }

    init {
        isFragmentRendered = false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        currentInstanceState = savedInstanceState
        activity = this

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            val window = activity.window

            // clear FLAG_TRANSLUCENT_STATUS flag:
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)

            // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)

            // finally change the color
            window.statusBarColor = ContextCompat.getColor(activity, R.color.colorNotification)
        }

        RenderCurrentFragment()
    }

    fun RenderCurrentFragment() {
        context = this
        myDB = DatabaseHelper()
        pref = context.getSharedPreferences("DataStore", Context.MODE_PRIVATE)
        editor = pref.edit()
        if (pref.getString("curfile", "") == null) {
            editor.putString("curfile", "home")
        }

        if (myDB.checkFlag() == false) {
            val i = Intent(this@HomeActivity, MainActivity::class.java)
            i.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(i)
        } else {
            setContentView(R.layout.activity_home)

            toolbar = findViewById<View>(R.id.toolbar) as Toolbar
            setSupportActionBar(toolbar)

            mHandler = Handler()

            drawer = findViewById<View>(R.id.drawer_layout) as DrawerLayout
            navigationView = findViewById<View>(R.id.nav_view) as NavigationView
            navigationView!!.itemIconTintList = null

            // Navigation view header
            navHeader = navigationView!!.getHeaderView(0)
            txtName = navHeader!!.findViewById<View>(R.id.name) as TextView
            txtWebsite = navHeader!!.findViewById<View>(R.id.website) as TextView
            imgNavHeaderBg = navHeader!!.findViewById<View>(R.id.img_header_bg) as ImageView
            imgProfile = navHeader!!.findViewById<View>(R.id.img_profile) as ImageView

            progressLayout = findViewById<View>(R.id.progressLayout) as LinearLayout
            progressBar = findViewById<View>(R.id.progressbar) as ProgressBar
            progressText = findViewById<View>(R.id.progressText) as TextView

            if (pref.getString("language", "0") == "1") navigationView!!.menu.getItem(3).setVisible(false)

            // load toolbar titles from string resources
            activityTitles = resources.getStringArray(if (pref.getString("language", "0") == "0") R.array.nav_item_activity_titles else R.array.nav_item_activity_titles_esp)

            setNavigationViewTitles()

            // load nav menu header data
            loadNavHeader()

            // initializing navigation menu
            setUpNavigationView()

            val orientation = resources.configuration.orientation

            //if (currentInstanceState == null) {
            //  editor.putString("CURRENT_TAG", TAG_HOME);
            //  loadHomeFragment();
            // } else if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
            loadHomeFragment()
            // }

            isFragmentRendered = true
        }
    }

    fun setNavigationViewTitles() {
        var it = 0;
        var menusize = navigationView!!.menu.size();
        for (title in activityTitles) {
            navigationView!!.menu.getItem(it).setTitle(title)
            it++;
        }

        navigationView!!.menu.getItem(menusize - 1).setTitle(if (pref.getString("language", "0") == "0") R.string.nav_logout else R.string.nav_logout_esp)
    }

    override fun onResume() {
        super.onResume()

        if ((!isFragmentRendered!!)!!) {
            // if (pref.getString("keepSession", "0") == "1") {
            RenderCurrentFragment()
            /* } else {
                val i = Intent(App.context, MainActivity::class.java)
                i.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                HomeActivity.context.startActivity(i)
            } */
        } else {
            isFragmentRendered = false
        }
    }

    override fun onPause() {
        super.onPause()
    }

    /***
     * Load navigation menu header information
     * like background image, profile image
     * name, website, notifications action view (dot)
     */
    private fun loadNavHeader() {
        // name, website
        txtName!!.setText(if (pref.getString("language", "0") == "0") R.string.app_name else R.string.app_name_esp)
        txtWebsite!!.text = "www.matrackgps.com"

        // loading header background image
    }

    /***
     * Returns respected fragment that user
     * selected from navigation menu
     */
    private fun loadHomeFragment() {
        // selecting appropriate nav menu item
        selectNavMenu()

        // set toolbar title


        // if user select the current navigation menu again, don't do anything
        // just close the navigation drawer
        // if (getSupportFragmentManager().findFragmentByTag(CURRENT_TAG) != null) {
        // drawer.closeDrawers();

        // show or hide the fab button
        //toggleFab();
        // return;
        // }

        // Sometimes, when fragment has huge data, screen seems hanging
        // when switching between navigation menus
        // So using runnable, the fragment is loaded with cross fade effect
        // This effect can be seen in GMail app
        //Runnable mPendingRunnable = new Runnable() {
        //@Override
        //public void run() {
        // update the main content by replacing fragments
        val fragment = homeFragment
        val CURRENT_TAG = pref.getString("CURRENT_TAG", TAG_HOME)
        if (fragment != null) {
            val fragmentTransaction = supportFragmentManager.beginTransaction()
            fragmentTransaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
            fragmentTransaction.replace(R.id.frame, fragment, CURRENT_TAG)
            fragmentTransaction.commitAllowingStateLoss()
        }
        //}
        //};
        setToolbarTitle()
        // If mPendingRunnable is not null, then add to the message queue
        //if (mPendingRunnable != null) {
        //mHandler.post(mPendingRunnable);
        //}

        // show or hide the fab button
        //toggleFab();

        //Closing drawer on item click
        drawer!!.closeDrawers()

        // refresh toolbar menu
        invalidateOptionsMenu()
    }

    private fun setToolbarTitle() {
        supportActionBar!!.setTitle(activityTitles!![getTitleIndex(pref.getString("CURRENT_TAG", TAG_HOME))])
        Log.d("fragment", activityTitles!![getTitleIndex(pref.getString("CURRENT_TAG", TAG_HOME))])
    }

    private fun selectNavMenu() {
        navigationView!!.menu.getItem(getTitleIndex(pref.getString("CURRENT_TAG", TAG_HOME))).isChecked = true
    }

    fun getTitleIndex(tag: String?): Int {
        var tag_index = 0

        if (tag === TAG_HOME) {
            tag_index = 0
        } else if (tag === TAG_REPORTS || tag === TAG_DTC || tag === TAG_DTC_FORM || tag === TAG_FUELREPORT || tag === TAG_FUELREPORT_FORM || tag === TAG_MILEAGE || tag === TAG_MILEAGE_FORM || tag === TAG_TRAVELLOG || tag === TAG_TRAVELLOG_FORM) {
            tag_index = 1
        } else if (tag === TAG_TRACKHISTORY || tag === TAG_TRACKHISTORY_FORM) {
            tag_index = 2
        } else if (tag === TAG_INSTA || tag === TAG_INSTA_FORM) {
            tag_index = 5
        } else if (tag === TAG_HELP) {
            tag_index = 6
        }
        return tag_index
    }

    private fun setUpNavigationView() {

        //Setting Navigation View Item Selected Listener to handle the item click of the navigation menu
        navigationView!!.setNavigationItemSelectedListener { menuItem ->
            // This method will trigger on item Click of navigation menu
            var flag = true
            var loadFrag = true
            var CURRENT_TAG = pref.getString("CURRENT_TAG", TAG_HOME)
            if (CURRENT_TAG!!.equals(TAG_TRACKHISTORY, ignoreCase = true)) {
                if (trackhistory_clustering.isInfoWindowOpen) {
                    Toast.makeText(context, if (pref.getString("language", "0") == "0") R.string.close_infowindow_before_navigating else R.string.close_infowindow_before_navigating_esp, Toast.LENGTH_LONG).show()
                    flag = false
                    editor.putString("CURRENT_TAG", TAG_TRACKHISTORY)
                    editor.commit()
                    setToolbarTitle()
                    selectNavMenu()
                    drawer!!.closeDrawers()
                    invalidateOptionsMenu()
                }
            }

            if (flag) {
                CURRENT_TAG = TAG_HOME
                //Check to see which item was being clicked and perform appropriate action
                when (menuItem.itemId) {
                    //Replacing the main content with ContentFragment Which is our Inbox View;
                    R.id.nav_home -> editor.putString("CURRENT_TAG", TAG_HOME)
                    R.id.nav_reports -> {
                        Log.d("-----------------", "permission " + myDB.getreportpermission())
                        if (myDB.getreportpermission() == "true") {
                            editor.putString("CURRENT_TAG", TAG_REPORTS)
                        } else {
                            Toast.makeText(applicationContext, if (pref.getString("language", "0") == "0") R.string.reports_permission_restricted else R.string.reports_permission_restricted_esp, Toast.LENGTH_SHORT).show()
                        }
                    }
                    R.id.nav_trackhistory -> {
                        Log.d("-----------------", "permission " + myDB.gettrackpermission())
                        if (myDB.gettrackpermission() == "true") {
                            editor.putString("CURRENT_TAG", TAG_TRACKHISTORY_FORM)
                        } else {
                            Toast.makeText(applicationContext, if (pref.getString("language", "0") == "0") R.string.trackhistory_permission_restricted else R.string.trackhistory_permission_restricted_esp, Toast.LENGTH_SHORT).show()
                        }
                    }
                    R.id.nav_roadside -> {
                        val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse("http://107.170.196.31//gpstracking/matrackApp/help.html"))
                        startActivity(browserIntent)
                        loadFrag = false
                    }
                    R.id.nav_navigation -> {
                        val format = "geo:0,0"
                        val uri = Uri.parse(format)
                        val intent = Intent(Intent.ACTION_VIEW, uri)
                        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                        startActivity(intent)
                        loadFrag = false
                    }
                    R.id.nav_insta -> {
                        Log.d("-----------------", "permission " + myDB.getreportpermission())
                        if (myDB.getreportpermission() == "true") {
                            editor.putString("CURRENT_TAG", TAG_INSTA_FORM)
                        } else {
                            Toast.makeText(applicationContext, if (pref.getString("language", "0") == "0") R.string.reports_permission_restricted else R.string.reports_permission_restricted_esp, Toast.LENGTH_SHORT).show()
                        }
                    }
                    R.id.nav_help -> editor.putString("CURRENT_TAG", TAG_HELP)
                    R.id.nav_logout -> logoutAlert()
                    else -> {
                        CURRENT_TAG = TAG_HOME
                        editor.putString("CURRENT_TAG", CURRENT_TAG)
                    }
                }
                editor.commit()

                //Checking if the item is in checked state or not, if not make it in checked state
                if (menuItem.isChecked) {
                    menuItem.isChecked = false
                } else {
                    menuItem.isChecked = true
                }
                menuItem.isChecked = true

                if (loadFrag) {
                    loadHomeFragment()
                }
            }

            true
        }

        val actionBarDrawerToggle = object : ActionBarDrawerToggle(this, drawer, toolbar, R.string.openDrawer, R.string.closeDrawer) {
            override fun onDrawerClosed(drawerView: View) {
                // Code here will be triggered once the drawer closes as we dont want anything to happen so we leave this blank
                super.onDrawerClosed(drawerView!!)
            }

            override fun onDrawerOpened(drawerView: View) {
                // Code here will be triggered once the drawer open as we dont want anything to happen so we leave this blank
                super.onDrawerOpened(drawerView!!)
            }
        }

        //Setting the actionbarToggle to drawer layout
        drawer!!.setDrawerListener(actionBarDrawerToggle)

        //calling sync state is necessary or else your hamburger icon wont show up
        actionBarDrawerToggle.syncState()
    }

    override fun onBackPressed() {
        if (drawer!!.isDrawerOpen(GravityCompat.START)) {
            drawer!!.closeDrawers()
            return
        }

        // This code loads home fragment when back key is pressed
        // when user is in other fragment than home
        if (shouldLoadHomeFragOnBackPress) {
            // checking if user is on other navigation menu
            // rather than home
            if (pref.getString("CURRENT_TAG", TAG_HOME) !== TAG_HOME) {
                editor.putString("CURRENT_TAG", TAG_HOME)
                loadHomeFragment()
                return
            }
        }

        super.onBackPressed()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.

        // show menu only when home fragment is selected
        menuInflater.inflate(R.menu.main, menu)
        menu.getItem(0).setTitle(if (pref.getString("language", "0") == "0") getString(R.string.logout) else getString(R.string.logout_esp))
        //Calling a method to set the backgournd color of the options menu
        //setMenuBackground();
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        val id = item.itemId
        if (id == R.id.action_logout) {
            logoutAlert()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    /**
     * Sets the menu items background.
     */
    private fun setMenuBackground() {
        /** Step 1. setting the custom LayoutInflater.Factory instance.  */
        layoutInflater.factory = LayoutInflater.Factory { name, context, attributeSet ->
            /**
             * Step 2. Implementing the onCreateView method
             * that will actually set the background selector.
             * {@inheritDoc}
             */
            /**
             * Step 3. Checking if the view that is to be created
             * is IconMenuItemView.
             * Notice that this is an internal class.
             */
            /**
             * Step 3. Checking if the view that is to be created
             * is IconMenuItemView.
             * Notice that this is an internal class.
             */
            if (name.equals
                    ("com.android.internal.view.menu.IconMenuItemView", ignoreCase = true)) {
                try {
                    /**
                     * Step 4. If the view is IconMenuItemView then
                     * create the view using the LayoutInflater.
                     */
                    /**
                     * Step 4. If the view is IconMenuItemView then
                     * create the view using the LayoutInflater.
                     */
                    val f = layoutInflater
                    val view = f.createView(name, null, attributeSet)
                    /**
                     * Step 5. This is the key part.
                     * The view instance that was created in step 4
                     * is an instance of IconMenuItemView.
                     * This is the view whose background color
                     * we want to change.  Unfortunately we just cannot
                     * change the background color at this place,
                     * since even if we change it here,
                     * framework overrides this value and
                     * we see the default background selector.
                     * Because of this reason the below line is commented.
                     * It does not work.
                     */
                    /**
                     * Step 5. This is the key part.
                     * The view instance that was created in step 4
                     * is an instance of IconMenuItemView.
                     * This is the view whose background color
                     * we want to change.  Unfortunately we just cannot
                     * change the background color at this place,
                     * since even if we change it here,
                     * framework overrides this value and
                     * we see the default background selector.
                     * Because of this reason the below line is commented.
                     * It does not work.
                     */
                    //view.setBackgroundResource(R.drawable.menu_selector);
                    /**
                     * Step 6. We have to change the background color
                     * after the view has rendered, using the Handler api.
                     */
                    /**
                     * Step 6. We have to change the background color
                     * after the view has rendered, using the Handler api.
                     */
                    Handler().post {
                        /** Step 7.  Changing the backgound color.  */
                        /** Step 7.  Changing the backgound color.  */
                        /** Step 7.  Changing the backgound color.  */

                        /** Step 7.  Changing the backgound color.  */
                        view.setBackgroundResource(
                                R.drawable.menu_selector)
                    }
                    return@Factory view
                } catch (e: Exception) {
                    /**
                     * Step 8.  Catching all exceptions that could occur
                     * in the process.  This is necessary since
                     * on Android 2.3, styling the internal
                     * IconMenuItemView throws an exception.
                     * Hence we have to fallback to the default menu styles.
                     */
                    /**
                     * Step 8.  Catching all exceptions that could occur
                     * in the process.  This is necessary since
                     * on Android 2.3, styling the internal
                     * IconMenuItemView throws an exception.
                     * Hence we have to fallback to the default menu styles.
                     */
                    Log.e("##Menu##", "Could not create a custom view for menu: " + e.message, e)
                }

            }
            null
        }
    }

    companion object {
        //private FloatingActionButton fab;

        // urls to load navigation header background image
        // and profile image
        //private static final String urlNavHeaderBg = "http://api.androidhive.info/images/nav-menu-header-bg.jpg";
        //private static final String urlProfileImg = "https://lh3.googleusercontent.com/eCtE_G34M9ygdkmOpYvCag1vBARCmZwnVS6rS5t4JLzJ6QgQSBquM0nuTsCpLhYbKljoyS-txg";

        // tags used to attach the fragments
        val TAG_HOME = "location"
        val TAG_REPORTS = "reports"
        val TAG_DTC = "dtc"
        val TAG_DTC_FORM = "dtcform"
        val TAG_INSTA = "instafence"
        val TAG_INSTA_FORM = "instafenceform"
        val TAG_FUELREPORT = "fuelreport"
        val TAG_FUELREPORT_FORM = "fuelreportform"
        val TAG_MILEAGE = "mileage"
        val TAG_MILEAGE_FORM = "mileageform"
        val TAG_TRACKHISTORY = "trackhistory"
        val TAG_TRACKHISTORY_FORM = "trackhistoryform"
        val TAG_TRAVELLOG = "travellog"
        val TAG_TRAVELLOG_FORM = "travellogform"
        val TAG_CHANGE_PASSWORD = "changepassword"
        val TAG_HELP = "help"
        val TAG_LOGOUT = "logout"
        lateinit var context: Context
        var values = ArrayList<String>()
        // toolbar titles respected to selected nav menu item
        private lateinit var activityTitles: Array<String>
        lateinit var progressLayout: LinearLayout
        lateinit var progressBar: ProgressBar
        lateinit var progressText: TextView
        lateinit var pref: SharedPreferences
        lateinit var editor: SharedPreferences.Editor
        lateinit var activity: Activity

        fun equalStrings(sb1: String, sb2: String): Boolean {
            val len = sb1.length
            if (len != sb2.length) {
                return false
            }
            for (i in 0 until len) {
                if (sb1[i] != sb2[i]) {
                    return false
                }
            }
            return true
        }

        fun logoutAlert() {
            val SignOutAlert = android.support.v7.app.AlertDialog.Builder(HomeActivity.context).create()
            SignOutAlert.setTitle(if (pref.getString("language", "0") == "0") R.string.logout else R.string.logout_esp)
            SignOutAlert.setButton(android.support.v7.app.AlertDialog.BUTTON_POSITIVE, context.getString(if (pref.getString("language", "0") == "0") R.string.yes else R.string.yes_esp)
            ) { dialog, which ->
                val myDB = DatabaseHelper()
                val isInserted = myDB.setFlag(0, "", "", "", "", "")
                val pref = HomeActivity.context.getSharedPreferences("DataStore", Context.MODE_PRIVATE)
                val editor = pref.edit()

                editor.putString("drivers", "")
                editor.putString("CURRENT_TAG", TAG_HOME)
                editor.commit()
                val i = Intent(App.context, MainActivity::class.java)
                i.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                HomeActivity.context.startActivity(i)
            }
            SignOutAlert.setButton(android.support.v7.app.AlertDialog.BUTTON_NEGATIVE, "No"
            ) { dialog, which -> dialog.dismiss() }
            SignOutAlert.show()
        }

        fun logout() {
            val myDB = DatabaseHelper()
            val isInserted = myDB.setFlag(0, "", "", "", "", "")
            val pref = HomeActivity.context.getSharedPreferences("DataStore", Context.MODE_PRIVATE)
            val editor = pref.edit()
            if (pref.getString("keepSession", "0")!!.equals("0", ignoreCase = true)) {
                editor.putString("drivers", "")
                editor.putString("CURRENT_TAG", TAG_HOME)
                editor.commit()
            }
        }
    }
}