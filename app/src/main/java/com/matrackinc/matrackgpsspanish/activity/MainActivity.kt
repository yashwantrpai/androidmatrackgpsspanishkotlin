package com.matrackinc.matrackgpsspanish.activity

import android.app.Activity
import android.app.ProgressDialog
import android.content.Context
import android.content.DialogInterface
import android.content.SharedPreferences
import android.net.ConnectivityManager
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.util.Pair
import android.view.View
import android.view.WindowManager
import android.widget.Button
import android.widget.EditText
import android.os.AsyncTask
import java.util.ArrayList

import android.content.Intent
import android.widget.TextView
import android.widget.Toast

import com.matrackinc.matrackgpsspanish.CreatePostString
import com.matrackinc.matrackgpsspanish.CustomHttpClient
import com.matrackinc.matrackgpsspanish.R
import com.matrackinc.matrackgpsspanish.db.DatabaseHelper
import com.matrackinc.matrackgpsspanish.util.Util

import org.json.JSONException
import org.json.JSONObject

class MainActivity : AppCompatActivity() {
    internal lateinit var mButton: Button
    internal lateinit var email: EditText
    internal lateinit var passcode: EditText
    internal var response: String? = "true"
    internal lateinit var credentialStore: DatabaseHelper
    internal lateinit var context: Context
    private var pdia: ProgressDialog? = null
    lateinit var pref: SharedPreferences
    lateinit var editor: SharedPreferences.Editor
    lateinit var forgot_password:TextView
    // lateinit var keep_session: com.suke.widget.SwitchButton

    override fun onCreate(savedInstanceState: Bundle?) {
        // Create new file with new values
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        context = this
        credentialStore = DatabaseHelper()
        pref = context.getSharedPreferences("DataStore", Context.MODE_PRIVATE)
        editor = pref.edit()

        activity = this

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            val window = activity.window

            // clear FLAG_TRANSLUCENT_STATUS flag:
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)

            // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)

            // finally change the color
            window.statusBarColor = ContextCompat.getColor(activity, R.color.colorNotification)
        }

        email = findViewById<View>(R.id.email_address) as EditText
        passcode = findViewById<View>(R.id.passcode) as EditText
        mButton = findViewById<View>(R.id.button) as Button
        forgot_password = findViewById<View>(R.id.forgot_password) as TextView

        email.setHint(if (pref.getString("language", "0") == "0") R.string.email else R.string.email_esp)
        passcode.setHint(if (pref.getString("language", "0") == "0") R.string.passcode else R.string.passcode_esp)
        mButton.setText(if (pref.getString("language", "0") == "0") R.string.sign_in else R.string.sign_in_esp)
        forgot_password.setText(if (pref.getString("language", "0") == "0") R.string.forgot_password else R.string.forgot_password_esp)
        // keep_session = findViewById<View>(R.id.keep_session) as com.suke.widget.SwitchButton
        // Terminate session if switch is off and app is launched from background
        // Display username in login page if switch is on

        /*
        if (pref.getString("keepSession", "0")!!.equals("1", ignoreCase = true)) {
            // keep_session.isChecked = true
            val saved_username = pref.getString("savedUsername", "")
            email.setText(saved_username)
            val saved_password = pref.getString("savedPassword", "")
            passcode.setText(saved_password)
        } */

        /*
        keep_session.setOnCheckedChangeListener { view, isChecked ->
            if (isChecked) {
                editor.putString("keepSession", "1")
            } else {
                editor.putString("keepSession", "0")
            }
            editor.commit()
        } */

        mButton.setOnClickListener {
            if (email.text.toString() != "" && passcode.text.toString() != "") {
                val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
                val activeNetworkInfo = connectivityManager.activeNetworkInfo
                if (activeNetworkInfo != null && activeNetworkInfo.isConnected) {
                    login()
                } else {
                    Toast.makeText(context, if (pref.getString("language", "0") == "0") R.string.internet_connectivity_check else R.string.internet_connectivity_check_esp, Toast.LENGTH_SHORT).show()
                }
            } else {
                val WrongPasswordAlert = AlertDialog.Builder(this@MainActivity).create()
                WrongPasswordAlert.setTitle(if (pref.getString("language", "0") == "0") R.string.empty_credentials else R.string.empty_credentials_esp)
                WrongPasswordAlert.setMessage(getString(if (pref.getString("language", "0") == "0") R.string.enter_credentials_to_login else R.string.enter_credentials_to_login_esp))
                WrongPasswordAlert.setButton(AlertDialog.BUTTON_NEUTRAL, getString(if (pref.getString("language", "0") == "0") R.string.ok else R.string.ok_esp)
                ) { dialog, which -> dialog.dismiss() }
                WrongPasswordAlert.show()
            }
        }

        forgot_password.setOnClickListener{
            val i = Intent(this, ForgotPassword::class.java)
            i.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(i)
        }
    }

    fun login() {
        val urls = resources.getStringArray(R.array.url)
        val currentServer = pref.getString("currentServer", "")
        if (Util.isValidUrl(currentServer, urls)) {
            val loginUrl = currentServer!! + getString(R.string.login_url)
            Log.d("login url", loginUrl)
            val params = ArrayList<Pair<String, String>>()
            var pair = Pair("operation", "login")
            params.add(pair)
            pair = Pair("username", email.text.toString())
            params.add(pair)
            pair = Pair("password", passcode.text.toString()) // android.telephony.TelephonyManager.getDeviceId()
            params.add(pair)
            pair = Pair("useragent", "android") // android.telephony.TelephonyManager.getDeviceId()
            params.add(pair)

            val postString = CreatePostString.createPostString(params)
            Log.i("param", postString)
            val loginTask = loginTask()
            loginTask.execute(loginUrl, postString)
        } else {
            Toast.makeText(context, if (pref.getString("language", "0") == "0") R.string.server_connection_issue else R.string.server_connection_issue_esp, Toast.LENGTH_SHORT).show()
            val i = Intent(context, SplashScreen::class.java)
            i.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(i)
        }
    }

    public override fun onDestroy() {
        super.onDestroy()
        if (pdia != null && pdia!!.isShowing) {
            pdia!!.cancel()
        }
    }

    private inner class loginTask : AsyncTask<String, Void, String>(), DialogInterface.OnCancelListener {

        override fun onPreExecute() {
            super.onPreExecute()
            if (!isFinishing && android.os.Build.VERSION.SDK_INT > 19) {
                pdia = ProgressDialog(context)
                pdia!!.setMessage(getString(if (pref.getString("language", "0") == "0") R.string.validating else R.string.validating_esp))
                pdia!!.setCancelable(false)
                pdia!!.show()
            }
        }

        override fun doInBackground(vararg url: String): String? {
            // TODO Auto-generated method stub
            var res: String? = null
            try {
                response = CustomHttpClient.executeHttpPost(url[0], url[1])
                res = response!!.toString()
                Log.d("Response", res)
                //res= res.replaceAll("\\s+","");
            } catch (e: Exception) {
                e.printStackTrace()
            }

            return res
        }//close doInBackground

        override fun onPostExecute(result: String) {
            if (!isFinishing) {
                if (pdia != null && pdia!!.isShowing) {
                    pdia!!.dismiss()
                }
            }
            response = result

            if (response != null && response != "") {
                Log.d(TAG, "Enable_login response: " + response!!)
                var respjson = JSONObject()
                try {
                    respjson = JSONObject(response)
                } catch (e: JSONException) {
                    e.printStackTrace()
                }

                try {
                    //Log.d(TAG, response);
                    if (response != null && response!!.contains("true")) {
                        var data = JSONObject()
                        try {
                            data = respjson.getJSONObject("data")
                            val authcode = data.getString("authcode")
                            val trackpermission = data.getString("trackpermission")
                            val reportpermission = data.getString("reportpermission")
                            /*
                            if (pref.getString("keepSession", "0") == "1") {
                                editor.putString("savedUsername", email.text.toString())
                                editor.putString("savedPassword", passcode.text.toString())
                                editor.commit()
                            } */

                            val isInserted = credentialStore.setFlag(1, email.text.toString(), passcode.text.toString(), authcode, trackpermission, reportpermission)
                            val i = Intent(this@MainActivity, HomeActivity::class.java)
                            i.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                            startActivity(i)
                        } catch (e: JSONException) {
                            e.printStackTrace()
                        }

                    } else {
                        val WrongPasswordAlert = AlertDialog.Builder(this@MainActivity).create()
                        WrongPasswordAlert.setTitle(if (pref.getString("language", "0") == "0") R.string.login_failed else R.string.login_failed_esp)
                        WrongPasswordAlert.setMessage(getString(if (pref.getString("language", "0") == "0") R.string.input_correct_credentials else R.string.input_correct_credentials_esp))
                        WrongPasswordAlert.setButton(AlertDialog.BUTTON_NEUTRAL, "OK"
                        ) { dialog, which -> dialog.dismiss() }
                        WrongPasswordAlert.show()
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }

            }//close onPostExecute
        }// close loginTask

        override fun onCancel(dialog: DialogInterface) {
            cancel(true)
        }
    }

    companion object {
        private val TAG = "MyActivity"
        lateinit var activity: Activity
    }
}
