package com.matrackinc.matrackgpsspanish.activity

import android.annotation.TargetApi
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.AsyncTask
import android.os.Build
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.TextView
import android.widget.Toast
import com.matrackinc.matrackgpsspanish.CustomHttpClient
import com.matrackinc.matrackgpsspanish.db.DatabaseHelper
import com.matrackinc.matrackgpsspanish.R
import com.matrackinc.matrackgpsspanish.WebkitCookieManagerProxy
import com.matrackinc.matrackgpsspanish.fragment.home
import com.squareup.okhttp.OkHttpClient
import org.json.JSONException
import org.json.JSONObject
import java.io.IOException
import java.net.CookieHandler
import java.net.InetAddress
import java.net.InetSocketAddress
import java.net.MalformedURLException
import java.net.Socket
import java.net.SocketAddress
import java.net.URL
import java.net.UnknownHostException

class SplashScreen : AppCompatActivity() {
    internal lateinit var credentialStore: DatabaseHelper
    lateinit var pref: SharedPreferences
    lateinit var editor: SharedPreferences.Editor
    lateinit var pleaseWait: TextView

    fun checkServer(url: String): Boolean {
        var exists: Boolean? = false
        val address: InetAddress
        try {
            address = InetAddress.getByName(URL(url).host)
            val sockaddr = InetSocketAddress(address, 80)
            // Create an unbound socket
            val sock = Socket()

            // This method will block no more than timeoutMs.
            // If the timeout occurs, SocketTimeoutException is thrown.
            val timeoutMs = 5000   // 5 seconds
            sock.connect(sockaddr, timeoutMs)
            exists = true
        } catch (e: UnknownHostException) {
            e.printStackTrace()
        } catch (e: MalformedURLException) {
            e.printStackTrace()
        } catch (e: IOException) {
            e.printStackTrace()
            val context = this@SplashScreen.applicationContext
            Toast.makeText(context, if (pref.getString("language", "0") == "0") R.string.internet_connectivity_check else R.string.internet_connectivity_check_esp, Toast.LENGTH_SHORT).show()
        }

        return exists!!
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        context = this
        activity = this
        credentialStore = DatabaseHelper()
        pref = context.getSharedPreferences("DataStore", Context.MODE_PRIVATE)
        editor = pref.edit()

        pleaseWait = findViewById<View>(R.id.please_wait) as TextView

        pleaseWait.setText(if (pref.getString("language", "0") == "0") R.string.please_wait else R.string.please_wait_esp)

        editor.putString("language", "1")
        editor.commit()

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            val window = activity.window

            // clear FLAG_TRANSLUCENT_STATUS flag:
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)

            // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)

            // finally change the color
            window.statusBarColor = ContextCompat.getColor(activity, R.color.colorNotification)
        }
        // This method will be executed once the timer is over
        // Start your app main activity
        checkServerTask().execute()
        // close this activity
    }

    fun getSessionCookie(activity_context: Context) {
        val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo

        val url = pref.getString("currentServer", "")!! + if (pref.getString("language", "0") == "0") R.string.login_url else R.string.login_url_esp
        val params = JSONObject()
        try {
            params.put("username", credentialStore.client.toString())
            params.put("password", credentialStore.passcode.toString())
        } catch (e: JSONException) {
            e.printStackTrace()
        }

        if (activeNetworkInfo != null && activeNetworkInfo.isConnected) {
            if (activity_context === this@SplashScreen) {
                val t1 = getSessionLoadMaps()
                t1.execute(url, params.toString())
            } else {
                val t2 = validateUserTask()
                t2.execute(url, params.toString())
            }
        } else {
            val context = activity_context.applicationContext
            Toast.makeText(context, if (pref.getString("language", "0") == "0") R.string.internet_connectivity_check else R.string.internet_connectivity_check_esp, Toast.LENGTH_SHORT).show()
            val i = Intent(activity_context, MainActivity::class.java)
            i.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(i)
        }
    }

    inner class validateUserTask : AsyncTask<String, Void, String>() {
        override fun doInBackground(url: Array<String>): String? {
            // TODO Auto-generated method stub
            var res: String? = null
            try {
                response = CustomHttpClient.executeHttpPost(url[0], url[1])
                res = response
                //res= res.replaceAll("\\s+","");
            } catch (e: Exception) {
                //txt_Error.setText(e.toString());
            }

            return res
        }//close doInBackground

        override fun onPostExecute(result: String) {
            response = result
            Log.d(TAG, "Enable_login response: $response")
        }
    }

    inner class checkServerTask : AsyncTask<String, Void, String>() {
        override fun onPreExecute() {
            //bar.setVisibility(View.VISIBLE);
        }

        override fun doInBackground(url: Array<String>): String {
            // TODO Auto-generated method stub
            val res: String? = null
            try {
                val urls = resources.getStringArray(R.array.url)
                while (true) {
                    if (checkServer(urls[0])) {
                        currentServer = urls[0]
                        editor.putString("currentServer", urls[0])
                        editor.commit()
                        break
                    } else if (checkServer(urls[1])) {
                        currentServer = urls[1]
                        editor.putString("currentServer", urls[1])
                        editor.commit()
                        break
                    }
                }
                //res= res.replaceAll("\\s+","");
            } catch (e: Exception) {
                //txt_Error.setText(e.toString());
            }

            //return res;
            return ""
        }//close doInBackground

        override fun onPostExecute(result: String) {
            //bar.setVisibility(View.GONE);

            Log.d(TAG, "CurrentServer $currentServer")
            if (credentialStore.checkFlag()) {
                getSessionCookie(this@SplashScreen)
            } else {
                val i = Intent(this@SplashScreen, MainActivity::class.java)
                i.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(i)
            }
        }
    }

    inner class getSessionLoadMaps : AsyncTask<String, Void, String>() {
        override fun doInBackground(url: Array<String>): String? {
            // TODO Auto-generated method stub
            var res: String? = null
            try {
                response = CustomHttpClient.executeHttpPost(url[0], url[1])
                res = response
                //res= res.replaceAll("\\s+","");
            } catch (e: Exception) {
                //txt_Error.setText(e.toString());
            }

            return res
        }//close doInBackground

        override fun onPostExecute(result: String) {
            response = result
            val i = Intent(this@SplashScreen, HomeActivity::class.java)
            i.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(i)
        }
    }

    companion object {

        var OkHTTPClient: OkHttpClient? = null
        lateinit var coreCookieManager: WebkitCookieManagerProxy
        lateinit var response: String
        var currentServer = ""
        lateinit var context: Context
        lateinit var activity: Activity
        var TAG = "SplashScreen"
        // Splash screen timer
        private val SPLASH_TIME_OUT = 2000

        // initialize WebkitCookieManagerProxy, set Cookie Policy, create android.webkit.webkitCookieManager
        val client: OkHttpClient?
            get() {
                if (OkHTTPClient == null) {
                    OkHTTPClient = OkHttpClient()
                    coreCookieManager = WebkitCookieManagerProxy(null, java.net.CookiePolicy.ACCEPT_ALL)
                    CookieHandler.setDefault(coreCookieManager)
                    OkHTTPClient!!.cookieHandler = coreCookieManager
                }
                return OkHTTPClient
            }
    }
}
