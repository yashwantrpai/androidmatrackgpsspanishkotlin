package com.matrackinc.matrackgpsspanish.activity

import android.app.Activity
import android.app.ProgressDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.SharedPreferences
import android.net.ConnectivityManager
import android.os.AsyncTask
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.util.Log
import android.util.Pair
import android.view.View
import android.widget.*
import com.matrackinc.matrackgpsspanish.CreatePostString
import com.matrackinc.matrackgpsspanish.CustomHttpClient
import com.matrackinc.matrackgpsspanish.R
import com.matrackinc.matrackgpsspanish.activity.SplashScreen
import com.matrackinc.matrackgpsspanish.fragment.dtcform
import com.matrackinc.matrackgpsspanish.util.Util
import org.json.JSONException
import org.json.JSONObject
import java.util.ArrayList

class ForgotPassword : AppCompatActivity() {

    lateinit var rootView: View
    lateinit var email_fgt: EditText
    lateinit var response:String
    lateinit var context: Activity
    lateinit var enter_email: TextView
    lateinit var fgt_title: TextView
    lateinit var submit: Button
    lateinit var pref: SharedPreferences
    private var pdia: ProgressDialog? = null
    lateinit var email:String
    lateinit var TAG:String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.forgot_password)

        email_fgt = findViewById<View>(R.id.email_fgt) as EditText
        enter_email = findViewById(R.id.enter_email) as TextView
        fgt_title = findViewById(R.id.fgt_title) as  TextView
        submit = findViewById(R.id.submit) as Button

        context = this

        pref = context.getSharedPreferences("DataStore", Context.MODE_PRIVATE)
        TAG = "ForgotPassword"

        fgt_title.text = getString(if (pref.getString("language", "0") == "0") R.string.forgot_password else R.string.forgot_password_esp)

        enter_email.text = getString(if (pref.getString("language", "0") == "0") R.string.enter_email else R.string.enter_email_esp)
        email_fgt.hint = getString(if (pref.getString("language", "0") == "0") R.string.email else R.string.email_esp)

        val back = findViewById<View>(R.id.back) as ImageView
        back.setOnClickListener {
            val i = Intent(this@ForgotPassword, MainActivity::class.java)
            i.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(i)
        }

        submit.setText(getString(if (pref.getString("language", "0") == "0") R.string.submit else R.string.submit_esp))
        submit.setOnClickListener{
            if (email_fgt.text.toString() != "" && email_fgt.text.toString() != "") {
                if(!Util.isValidEmail(email_fgt.text.toString())){
                    val EmptyCredentialAlert = AlertDialog.Builder(this@ForgotPassword).create()
                    EmptyCredentialAlert.setMessage(getString(if (pref.getString("language", "0") == "0") R.string.invalid_email else R.string.invalid_email_esp))
                    EmptyCredentialAlert.setButton(AlertDialog.BUTTON_NEUTRAL, getString(if (pref.getString("language", "0") == "0") R.string.ok else R.string.ok_esp)
                    ) { dialog, which -> dialog.dismiss() }
                    EmptyCredentialAlert.show()
                } else {
                    val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
                    val activeNetworkInfo = connectivityManager.activeNetworkInfo
                    if (activeNetworkInfo != null && activeNetworkInfo.isConnected) {
                        forgotpassword()
                    } else {
                        Toast.makeText(context, if (pref.getString("language", "0") == "0") R.string.internet_connectivity_check else R.string.internet_connectivity_check_esp, Toast.LENGTH_SHORT).show()
                    }
                }
            } else {
                val EmptyCredentialAlert = AlertDialog.Builder(this@ForgotPassword).create()
                EmptyCredentialAlert.setTitle(if (pref.getString("language", "0") == "0") R.string.empty_credentials else R.string.empty_credentials_esp)
                EmptyCredentialAlert.setMessage(getString(if (pref.getString("language", "0") == "0") R.string.enter_credentials_to_login else R.string.enter_credentials_to_login_esp))
                EmptyCredentialAlert.setButton(AlertDialog.BUTTON_NEUTRAL, getString(if (pref.getString("language", "0") == "0") R.string.ok else R.string.ok_esp)
                ) { dialog, which -> dialog.dismiss() }
                EmptyCredentialAlert.show()
            }
        }
    }

    fun forgotpassword() {
        val urls = resources.getStringArray(R.array.url)
        val currentServer = pref.getString("currentServer", "")
        if (Util.isValidUrl(currentServer, urls)) {
            val forgotPasswordUrl = currentServer!! + getString(R.string.forgot_password_url)
            Log.d("forgot password url", forgotPasswordUrl)
            val params = ArrayList<Pair<String, String>>()
            var pair = Pair("operation", "forgotPassword")
            params.add(pair)
            pair = Pair("email", email_fgt.text.toString())
            params.add(pair)

            val postString = CreatePostString.createPostString(params)
            Log.i("param", postString)
            val forgotPasswordTask = forgotPasswordTask()
            forgotPasswordTask.execute(forgotPasswordUrl, postString)
        } else {
            Toast.makeText(context, if (pref.getString("language", "0") == "0") R.string.server_connection_issue else R.string.server_connection_issue_esp, Toast.LENGTH_SHORT).show()
            val i = Intent(context, SplashScreen::class.java)
            i.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(i)
        }
    }

    public override fun onDestroy() {
        super.onDestroy()
        if (pdia != null && pdia!!.isShowing) {
            pdia!!.cancel()
        }
    }

    private inner class forgotPasswordTask : AsyncTask<String, Void, String>(), DialogInterface.OnCancelListener {

        override fun onPreExecute() {
            super.onPreExecute()
            if (!isFinishing && android.os.Build.VERSION.SDK_INT > 19) {
                pdia = ProgressDialog(context)
                pdia!!.setMessage(getString(if (pref.getString("language", "0") == "0") R.string.validating_email else R.string.validating_email_esp))
                pdia!!.setCancelable(false)
                pdia!!.show()
            }
        }
        override fun doInBackground(vararg url: String): String? {
            // TODO Auto-generated method stub
            var res: String? = null
            try {
                response = CustomHttpClient.executeHttpPost(url[0], url[1])
                res = response!!.toString()
                Log.d("Response", res)
                //res= res.replaceAll("\\s+","");
            } catch (e: Exception) {
                e.printStackTrace()
            }

            return res
        }//close doInBackground

        override fun onPostExecute(result: String) {
            if (!isFinishing) {
                if (pdia != null && pdia!!.isShowing) {
                    pdia!!.dismiss()
                }
            }

            response = result

            if (response != null && response != "") {
                Log.d(TAG, "Forgot_password response: " + response!!)
                var respjson = JSONObject()
                try {
                    respjson = JSONObject(response)
                } catch (e: JSONException) {
                    e.printStackTrace()
                }

                try {
                    //Log.d(TAG, response);
                    if (response != null) {
                        if(respjson.get("message").equals("Given email is not registered with our system. Please check.")){
                            val ForgotPasswordAlert = AlertDialog.Builder(context).create()
                            ForgotPasswordAlert.setMessage(getString(if (pref.getString("language", "0") == "0") R.string.given_email_invalid else R.string.given_email_invalid_esp))
                            ForgotPasswordAlert.setButton(AlertDialog.BUTTON_NEUTRAL, "OK"
                            ) { dialog, which -> dialog.dismiss() }
                            ForgotPasswordAlert.show()
                        } else if (respjson.get("message").equals("New password will be emailed to your registered email address.")){
                            val ForgotPasswordAlert = AlertDialog.Builder(context).create()
                            ForgotPasswordAlert.setTitle(getString(if (pref.getString("language", "0") == "0") R.string.new_password_generated else R.string.new_password_generated_esp))
                            ForgotPasswordAlert.setMessage(getString(if (pref.getString("language", "0") == "0") R.string.new_password_mailed else R.string.new_password_mailed_esp))
                            ForgotPasswordAlert.setButton(AlertDialog.BUTTON_NEUTRAL, "OK"
                            ) { dialog, which -> dialog.dismiss() }
                            ForgotPasswordAlert.show()
                        } else {
                            val ForgotPasswordAlert = AlertDialog.Builder(context).create()
                            ForgotPasswordAlert.setMessage(getString(if (pref.getString("language", "0") == "0") R.string.invalid_email else R.string.invalid_email_esp))
                            ForgotPasswordAlert.setButton(AlertDialog.BUTTON_NEUTRAL, "OK"
                            ) { dialog, which -> dialog.dismiss() }
                            ForgotPasswordAlert.show()
                        }
                    } else {
                        val ForgotPasswordAlert = AlertDialog.Builder(context).create()
                        ForgotPasswordAlert.setMessage(getString(if (pref.getString("language", "0") == "0") R.string.invalid_email else R.string.invalid_email_esp))
                        ForgotPasswordAlert.setButton(AlertDialog.BUTTON_NEUTRAL, "OK"
                        ) { dialog, which -> dialog.dismiss() }
                        ForgotPasswordAlert.show()
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }//close onPostExecute
        }// close loginTask

        override fun onCancel(dialog: DialogInterface) {
            cancel(true)
        }
    }
}
