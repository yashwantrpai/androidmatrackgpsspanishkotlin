package com.matrackinc.matrackgpsspanish.util

object ConvertUtil {    //

    fun getDouble(str: String?, defaultValue: String): Double? {
        var value = 0.0
        try {
            if (str != null) {
                value = java.lang.Double.parseDouble(str)
            } else {
                value = java.lang.Double.parseDouble(defaultValue)
            }
        } catch (ex: Exception) {
            ex.printStackTrace()

        }

        return value
    }
}
