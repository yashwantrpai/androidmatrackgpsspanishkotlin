package com.matrackinc.matrackgpsspanish.util

import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.Date

//This is test commit
object DateUtil {
    fun covertToCivilianTime(time: String): String {
        var dateFormat: Date? = null
        val format = SimpleDateFormat("H:mm")
        try {
            dateFormat = format.parse(time)
            return SimpleDateFormat("KK:mm a").format(dateFormat)
        } catch (e: ParseException) {
            e.printStackTrace()
        }

        return time
    }

    fun toSpanishDate(dt: String?): String {
        var spdate: String = ""

        var strs = dt!!.split("/").toTypedArray()
        spdate = strs[1] + "/" + strs[0] + "/" + strs[2]

        return spdate;
    }
}
