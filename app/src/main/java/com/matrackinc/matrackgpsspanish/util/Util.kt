package com.matrackinc.matrackgpsspanish.util

import com.matrackinc.matrackgpsspanish.R

object Util {

    fun isValidUrl(url: String, urls: Array<String>): Boolean {
        for (it in urls.indices) {
            if (urls[it] == url) {
                return true
            }
        }
        return false
    }

    fun isValidEmail(email: String): Boolean {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }

    fun getDatapoint(spanishDatapoint: String?): String {
        when (spanishDatapoint) {
            "TODO" -> return "ALL"

            "PARADAS UNICAMENTE" -> return "STOPSONLY"

            "MOTOR ENCENDIDO/APAGADO" -> return "IGNITION"

            "MOVIMIENTOS UNICAMENTE" -> return "MOVEMENT"
        }
        return "ALL"
    }
}
