package com.matrackinc.matrackgpsspanish.util

import com.google.android.gms.maps.model.LatLng
import com.google.maps.android.clustering.ClusterItem

class MyItem(private val mPosition: LatLng, var index: Int, var latitude: Double, var longitude: Double, var date: String?, var time: String?, var speed: String?, var voltage: String?, var event: String?, var direction: String?, var fuel: String?, var mileage: String?, var eventName: String?) : ClusterItem {

    override fun getPosition(): LatLng {
        return mPosition
    }

    override fun getTitle(): String? {
        return null
    }

    override fun getSnippet(): String? {
        return null
    }
}
