package com.matrackinc.matrackgpsspanish

import android.util.Pair

import java.util.ArrayList

object CreatePostString {

    fun createPostString(params: ArrayList<Pair<String, String>>): String {
        var postString = ""

        for (i in params.indices) {
            postString += params[i].first.toString() + "::" + params[i].second
            if (i < params.size - 1) {
                postString += "###"
            }
        }

        return postString
    }
}
