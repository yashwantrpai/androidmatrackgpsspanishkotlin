package com.matrackinc.matrackgpsspanish.fragment

import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.location.Geocoder
import android.location.Location
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.net.Uri
import android.os.AsyncTask
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.res.ResourcesCompat
import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.style.StyleSpan
import android.util.Log
import android.util.Pair
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.TextView
import android.content.Context
import android.widget.Toast

import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapView
import com.google.android.gms.maps.MapsInitializer
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.matrackinc.matrackgpsspanish.CreatePostString
import com.matrackinc.matrackgpsspanish.CustomHttpClient
// import com.matrackinc.matrackgpsspanish.FetchAddressIntentService;
import com.matrackinc.matrackgpsspanish.db.DatabaseHelper
import com.matrackinc.matrackgpsspanish.R
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject

import com.matrackinc.matrackgpsspanish.activity.HomeActivity
import com.matrackinc.matrackgpsspanish.activity.SplashScreen
import com.matrackinc.matrackgpsspanish.util.Util

import java.util.ArrayList
import java.util.concurrent.ScheduledThreadPoolExecutor
import java.util.concurrent.TimeUnit

import android.content.Context.MODE_PRIVATE

/**
 * A simple [Fragment] subclass.
 */
class home : Fragment(), GoogleMap.OnMapLoadedCallback, OnMapReadyCallback {

    internal var mapView: MapView? = null
    private var builder: LatLngBounds.Builder? = null
    private var bounds: LatLngBounds? = null
    private var isDestroyed = false
    lateinit var pref: SharedPreferences
    lateinit var editor: SharedPreferences.Editor
    internal lateinit var credentialStore: DatabaseHelper
    lateinit var authcode: String
    protected var mLastLocation: Location? = null
    var thisContext: Context? = null
    lateinit var infowindowlayout: LinearLayout
    lateinit var tvDate: TextView
    lateinit var tvBattery: TextView
    lateinit var tvAddress: TextView
    lateinit var tvName: TextView
    lateinit var tvMapIt: TextView
    lateinit var tvSpeed: TextView
    lateinit var tvEvent: TextView
    lateinit var tvDirection: TextView

    val isDataPresent: Boolean?
        get() = if (lat != null && lng != null && Name != null && Battery != null && LastRead != null) {
            true
        } else {
            false
        }

    internal var TimerTask: Runnable = Runnable {
        if (activity != null) {
            if (!activity!!.isFinishing) {
                val connectivityManager = activity!!.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
                val activeNetworkInfo = connectivityManager.activeNetworkInfo
                if (activeNetworkInfo != null && activeNetworkInfo.isConnected) {
                    activity!!.runOnUiThread { currentlocation() }
                } else {
                    activity!!.runOnUiThread { Toast.makeText(context, if (pref.getString("language", "0") == "0") R.string.internet_connectivity_check else R.string.internet_connectivity_check_esp, Toast.LENGTH_SHORT).show() }
                }
            }
        }
    }

    fun loadAddress() {
        if (activity != null) {
            if (!activity!!.isFinishing) {
                val connectivityManager = activity!!.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
                val activeNetworkInfo = connectivityManager.activeNetworkInfo
                if (activeNetworkInfo != null && activeNetworkInfo.isConnected) {
                    // String url = "http://maps.google.com/maps/geo?q=" + Double.toString(lat[current_ind]) + "," + Double.toString(lng[current_ind]) + "&output=xml&oe=utf8&sensor=true&key=" + R.string.googlemaps_api_key;
                    // final String url = "http://nominatim-load-1807280891.us-east-1.elb.amazonaws.com/reverse_geocode_tracking.php?lat=" + lat[current_ind] + "&lng=" + lng[current_ind];
                    // Log.i("--url--", url);
                    Address = ""
                    try {
                        val url = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + java.lang.Double.toString(lat!![current_ind]!!) + "," + java.lang.Double.toString(lng!![current_ind]!!) + "&key=" + google_maps_api
                        System.out.println("url: "+url)
                        val getGeocodeAddress = getGeocodeAddress()
                        getGeocodeAddress.execute(url)
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                } else {
                    Toast.makeText(context, if (pref.getString("language", "0") == "0") R.string.internet_connectivity_check else R.string.internet_connectivity_check_esp, Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    fun resizeMapIcons(iconName: String, width: Int, height: Int): Bitmap? {
        var imageBitmap: Bitmap? = null
        if (isAdded) {
            try {
                imageBitmap = BitmapFactory.decodeResource(resources, resources.getIdentifier(iconName, "drawable", context!!.packageName))
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }
        if (imageBitmap != null) {
            try {
                return Bitmap.createScaledBitmap(imageBitmap, width, height, false)
            } catch (ex: Exception) {
                ex.printStackTrace()
            }

        }
        return imageBitmap
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        rootView = inflater.inflate(R.layout.map_fragment, container, false)
        isDestroyed = false
        pref = context!!.getSharedPreferences("DataStore", MODE_PRIVATE)
        editor = pref.edit()

        rb_normal = rootView.findViewById<View>(R.id.rb_normal) as Button
        rb_normal.setText(if (pref.getString("language", "0") == "0") R.string.standard_view else R.string.standard_view_esp)
        rb_satellite = rootView.findViewById<View>(R.id.rb_satellite) as Button
        rb_satellite.setText(if (pref.getString("language", "0") == "0") R.string.satellite_view else R.string.satellite_view_esp)
        rb_hybrid = rootView.findViewById<View>(R.id.rb_hybrid) as Button
        rb_hybrid.setText(if (pref.getString("language", "0") == "0") R.string.hybrid_view else R.string.hybrid_view_esp)

        zoomminus = rootView.findViewById<View>(R.id.zoomminus) as ImageView
        zoomplus = rootView.findViewById<View>(R.id.zoomplus) as ImageView

        progressLayout = activity!!.findViewById<View>(R.id.progressLayout) as LinearLayout
        progressBar = activity!!.findViewById<View>(R.id.progressbar) as ProgressBar
        progressText = activity!!.findViewById<View>(R.id.progressText) as TextView

        infowindowlayout = rootView.findViewById<View>(R.id.infowindowlayout) as LinearLayout
        tvDate = rootView.findViewById<View>(R.id.tv_lastread) as TextView
        tvBattery = rootView.findViewById<View>(R.id.tv_battery) as TextView
        tvAddress = rootView.findViewById<View>(R.id.tv_address) as TextView
        tvName = rootView.findViewById<View>(R.id.tv_name) as TextView
        tvMapIt = rootView.findViewById<View>(R.id.tv_mapit) as TextView
        tvSpeed = rootView.findViewById<View>(R.id.tv_speed) as TextView
        tvEvent = rootView.findViewById<View>(R.id.tv_event) as TextView
        tvDirection = rootView.findViewById<View>(R.id.tv_direction) as TextView
        tvClose = rootView.findViewById<View>(R.id.tv_close) as ImageView
        batteryLevel = rootView.findViewById<View>(R.id.battery_level) as ImageView
        google_maps_api = resources.getString(R.string.googlemaps_api_key)

        credentialStore = DatabaseHelper()
        authcode = credentialStore.authcode

        if (pref.getString("curfile", "") == null || !HomeActivity.equalStrings(pref.getString("curfile", "")!!.toString(), "home")) {
            editor.putString("curfile", "home")
            editor.commit()
        }

        val refresh_btn = rootView.findViewById<View>(R.id.restart) as ImageView
        refresh_btn.setOnClickListener {
            // Reload current fragment
            if (activity != null) {
                if (!activity!!.isFinishing) {
                    val connectivityManager = activity!!.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
                    val activeNetworkInfo = connectivityManager.activeNetworkInfo
                    if (activeNetworkInfo != null && activeNetworkInfo.isConnected) {
                        currentlocation()
                    } else {
                        Toast.makeText(context, if (pref.getString("language", "0") == "0") R.string.internet_connectivity_check else R.string.internet_connectivity_check_esp, Toast.LENGTH_SHORT).show()
                    }
                }
            }
        }

        return rootView
    }

    fun currentlocation() {
        val currentServer = pref.getString("currentServer", "")
        val urls = activity!!.resources.getStringArray(R.array.url)
        if (Util.isValidUrl(currentServer, urls)) {
            val currentLocationUrl = currentServer!! + getString(if (pref.getString("language", "0") == "0") R.string.current_location_url else R.string.currentlocation_url_esp)
            val params = ArrayList<Pair<String, String>>()
            var pair = Pair("operation", "currentlocation")
            params.add(pair)
            pair = Pair("authcode", authcode)
            params.add(pair)

            val postString = CreatePostString.createPostString(params)
            // Log.i("url", currentLocationUrl);
            // Log.i("param", postString);
            val currentLocationTask = getCurrentLocation()
            currentLocationTask.execute(currentLocationUrl, postString)
        } else {
            Toast.makeText(context, if (pref.getString("language", "0") == "0") R.string.server_connection_issue else R.string.server_connection_issue_esp, Toast.LENGTH_SHORT).show()
            val i = Intent(activity, SplashScreen::class.java)
            i.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(i)
        }
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        thisContext = context
    }

    fun getSpannedText(str: String, startInd: Int, endInd: Int): SpannableStringBuilder {
        val sb = SpannableStringBuilder(str)

        val bss = StyleSpan(android.graphics.Typeface.BOLD)
        sb.setSpan(bss, startInd, endInd, Spannable.SPAN_INCLUSIVE_INCLUSIVE)

        return sb
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        isDestroyed = false

        mapView = rootView.findViewById<View>(R.id.mapView) as com.google.android.gms.maps.MapView
        try {
            if (mapView != null) {
                mapView!!.onCreate(savedInstanceState)
            }
            mapView!!.onResume() // needed for getting the map to display immediately
        } catch (e: Exception) {
            e.printStackTrace()
        }

        try {
            MapsInitializer.initialize(activity!!.applicationContext)
        } catch (e: Exception) {
            e.printStackTrace()
        }

        mapView!!.getMapAsync { mMap ->
            googleMap = mMap
            googleMap!!.isIndoorEnabled = false
            googleMap!!.uiSettings.isScrollGesturesEnabled = true
            googleMap!!.uiSettings.isZoomGesturesEnabled = true
            googleMap!!.uiSettings.isRotateGesturesEnabled = true

            when (pref.getString("mapSelect", "")) {
                "Normal" -> {
                    googleMap!!.mapType = GoogleMap.MAP_TYPE_NORMAL
                    rb_normal.isSelected = true
                    rb_satellite.isSelected = false
                    rb_hybrid.isSelected = false
                    editor.putString("mapSelect", "Normal")
                    editor.commit()
                }

                "Satellite" -> {
                    googleMap!!.mapType = GoogleMap.MAP_TYPE_SATELLITE
                    rb_satellite.isSelected = true
                    rb_normal.isSelected = false
                    rb_hybrid.isSelected = false
                    editor.putString("mapSelect", "Satellite")
                    editor.commit()
                }

                "Hybrid" -> {
                    googleMap!!.mapType = GoogleMap.MAP_TYPE_HYBRID
                    rb_hybrid.isSelected = true
                    rb_normal.isSelected = false
                    rb_satellite.isSelected = false
                    editor.putString("mapSelect", "Hybrid")
                    editor.commit()
                }

                else -> {
                    googleMap!!.mapType = GoogleMap.MAP_TYPE_NORMAL
                    rb_normal.isSelected = true
                    rb_satellite.isSelected = false
                    rb_hybrid.isSelected = false
                    editor.putString("mapSelect", "Normal")
                    editor.commit()
                }
            }
            // googleMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);

            if (HomeActivity.equalStrings(pref.getString("curfile", "")!!.toString(), "home")) {
                if (HomeActivity.equalStrings(pref.getString("mapSelect", "")!!.toString(), "Normal")) {
                    rb_normal.isSelected = true
                } else if (HomeActivity.equalStrings(pref.getString("mapSelect", "")!!.toString(), "Satellite")) {
                    rb_satellite.isSelected = true
                } else if (HomeActivity.equalStrings(pref.getString("mapSelect", "")!!.toString(), "Hybrid")) {
                    rb_hybrid.isSelected = true
                } else {
                    rb_normal.isSelected = true
                }
            } else {
                rb_normal.isSelected = true
            }

            rb_normal.setOnTouchListener { v, event ->
                googleMap!!.mapType = GoogleMap.MAP_TYPE_NORMAL
                rb_normal.isSelected = true
                rb_satellite.isSelected = false
                rb_hybrid.isSelected = false

                editor.putString("mapSelect", "Normal")
                editor.commit()
                true
            }

            rb_satellite.setOnTouchListener { v, event ->
                googleMap!!.mapType = GoogleMap.MAP_TYPE_SATELLITE
                rb_satellite.isSelected = true
                rb_normal.isSelected = false
                rb_hybrid.isSelected = false

                editor.putString("mapSelect", "Satellite")
                editor.commit()
                true
            }

            rb_hybrid.setOnTouchListener { v, event ->
                googleMap!!.mapType = GoogleMap.MAP_TYPE_HYBRID
                rb_hybrid.isSelected = true
                rb_normal.isSelected = false
                rb_satellite.isSelected = false

                editor.putString("mapSelect", "Hybrid")
                editor.commit()
                true
            }

            googleMap!!.setOnMapLoadedCallback {
                if (activity != null) {
                    if (!activity!!.isFinishing) {
                        val connectivityManager = activity!!.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
                        val activeNetworkInfo = connectivityManager.activeNetworkInfo
                        if (activeNetworkInfo != null && activeNetworkInfo.isConnected) {
                            currentlocation()
                        } else {
                            Toast.makeText(context, if (pref.getString("language", "0") == "0") R.string.internet_connectivity_check else R.string.internet_connectivity_check_esp, Toast.LENGTH_SHORT).show()
                        }
                    }
                }
            }

            zoomplus.setOnClickListener {
                if (googleMap != null) {
                    val newzoom = googleMap!!.cameraPosition.zoom + 1
                    val centerLatLng = googleMap!!.cameraPosition.target
                    val latitude = centerLatLng.latitude
                    val longitude = centerLatLng.longitude
                    googleMap!!.animateCamera(CameraUpdateFactory.newLatLngZoom(LatLng(latitude, longitude), newzoom))
                }
            }

            zoomminus.setOnClickListener {
                if (googleMap != null) {
                    val newzoom = googleMap!!.cameraPosition.zoom - 1
                    val centerLatLng = googleMap!!.cameraPosition.target
                    val latitude = centerLatLng.latitude
                    val longitude = centerLatLng.longitude
                    googleMap!!.animateCamera(CameraUpdateFactory.newLatLngZoom(LatLng(latitude, longitude), newzoom))
                }
            }
        }
    }


    fun loadMap() {
        if (googleMap == null) {
            if (activity != null) {
                if (!activity!!.isFinishing) {
                    val alertDialogBuilder = AlertDialog.Builder(activity)
                    val alertDialog = alertDialogBuilder.create()
                    alertDialogBuilder.setMessage(this.resources.getString(if (pref.getString("language", "0") == "0") R.string.google_map_reload else R.string.google_map_reload_esp))
                    alertDialogBuilder.setNeutralButton(this.resources.getString(if (pref.getString("language", "0") == "0") R.string.ok else R.string.ok_esp)) { arg0, arg1 -> arg0.dismiss() }
                    alertDialogBuilder.show()
                }
            }
        } else {
            googleMap!!.clear()
            if (isDataPresent!!) {
                if (lat!!.size > 0 && lng!!.size > 0) {
                    marker_list = arrayOfNulls(num_drivers)
                    builder = LatLngBounds.Builder()
                    var markersIncluded = false
                    if (marker_list != null) {
                        for (i in marker_list!!.indices) {
                            try {
                                if (lat!![i] != null && lng!![i] != null) {
                                    marker_list!![i] = googleMap!!.addMarker(MarkerOptions()
                                            .position(LatLng(lat!![i]!!, lng!![i]!!))
                                            .icon(BitmapDescriptorFactory.fromBitmap(resizeMapIcons("markericon", 90, 90))))
                                    if (marker_list!![i] != null) {
                                        builder!!.include(marker_list!![i]!!.position)
                                        markersIncluded = true
                                    } else {
                                        Log.w(TAG, "marker_list is null at index $i")
                                    }
                                }
                            } catch (e: Exception) {
                                e.printStackTrace()
                            }

                        }
                    } else {
                        Log.w(TAG, "marker_list is null")
                    }
                    //If marker is not included it causes IllegalStateException
                    if (markersIncluded) {
                        bounds = builder!!.build()
                        googleMap!!.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 80))
                        val currentzoom = googleMap!!.cameraPosition.zoom
                        if (currentzoom > 14) {
                            var lat_avg: Double? = 0.0
                            var lng_avg: Double? = 0.0
                            var lat_sum: Double? = 0.0
                            var lng_sum: Double? = 0.0
                            if (lat_sum != null) {
                                if (lng_sum != null) {
                                    for (i in lat!!.indices) {
                                        lat_sum += lat!![i]!!
                                        lng_sum += lng!![i]!!
                                    }
                                }
                            }
                            lat_avg = lat_sum!! / lat!!.size
                            lng_avg = lng_sum!! / lng!!.size
                            googleMap!!.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(lat_avg, lng_avg), 14f))
                        }
                    }

                    // Setting a custom info window adapter for the google map
                    // googleMap.setInfoWindowAdapter(new CustomInfoWindowAdapter());

                    googleMap!!.setOnMarkerClickListener { marker ->
                        for (i in marker_list!!.indices) {
                            if (marker_list!![i] != null) {
                                if (marker_list!![i]!!.position.latitude == marker.position.latitude && marker_list!![i]!!.position.longitude == marker.position.longitude) {
                                    current_marker = marker
                                    current_ind = i

                                    val markerbuilder: LatLngBounds.Builder
                                    val markerbounds: LatLngBounds
                                    markerbuilder = LatLngBounds.Builder()
                                    markerbuilder.include(marker.position)
                                    markerbounds = markerbuilder.build()
                                    googleMap!!.moveCamera(CameraUpdateFactory.newLatLngBounds(markerbounds, 60))
                                    val currentzoom = googleMap!!.cameraPosition.zoom
                                    if (currentzoom > 15) {
                                        googleMap!!.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(marker.position.latitude, marker.position.longitude), 13f))
                                    }
                                    break
                                }
                            } else {
                                Log.w(TAG, "marker_list is null at index $i")
                            }
                        }
                        loadAddress()
                        true
                    }

                    googleMap!!.setOnInfoWindowClickListener { }
                } else {
                    if (activity != null) {
                        if (!activity!!.isFinishing) {
                            val alertDialogBuilder = AlertDialog.Builder(activity)
                            val alertDialog = alertDialogBuilder.create()
                            alertDialogBuilder.setMessage(if (pref.getString("language", "0") == "0") R.string.no_data_available else R.string.no_data_available_esp)
                            alertDialogBuilder.setNeutralButton(activity!!.resources.getString(if (pref.getString("language", "0") == "0") R.string.ok else R.string.ok_esp)) { arg0, arg1 -> arg0.dismiss() }
                            alertDialogBuilder.show()
                        }
                    }
                }
            } else {
                if (activity != null) {
                    if (!activity!!.isFinishing) {
                        val alertDialogBuilder = AlertDialog.Builder(activity)
                        val alertDialog = alertDialogBuilder.create()
                        alertDialogBuilder.setTitle(if (pref.getString("language", "0") == "0") R.string.data_sync_issue else R.string.data_sync_issue_esp)
                        alertDialogBuilder.setMessage(if (pref.getString("language", "0") == "0") R.string.re_login_and_try_again else R.string.re_login_and_try_again_esp)
                        alertDialogBuilder.setNeutralButton(activity!!.resources.getString(if (pref.getString("language", "0") == "0") R.string.ok else R.string.ok_esp)
                        ) { dialog, which -> dialog.dismiss() }
                        alertDialogBuilder.show()
                    }
                }
            }
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        try {
            if (mapView != null) {
                mapView!!.onSaveInstanceState(outState)
            } else {
                Log.w(TAG, "MapView is null")
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    override fun onResume() {
        super.onResume()
        try {
            if (mapView != null) {
                mapView!!.onResume()
            } else {
                Log.w(TAG, "MapView is null")
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

        scheduledThreadPoolExecutor = ScheduledThreadPoolExecutor(1)
        scheduledThreadPoolExecutor!!.scheduleAtFixedRate(TimerTask, 60, 60, TimeUnit.SECONDS)

        dismissProgressDialog()
        closeInfoWindowLayout()

        isDestroyed = false
    }

    override fun onPause() {
        super.onPause()
        try {
            if (mapView != null) {
                mapView!!.onPause()
            } else {
                Log.w(TAG, "MapView is null")
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

        if (scheduledThreadPoolExecutor != null) {
            Log.i("lc", "timer shutdown")
            scheduledThreadPoolExecutor!!.shutdown()
        }
        dismissProgressDialog()
        closeInfoWindowLayout()
        // logout();

        isDestroyed = false
    }

    override fun onDestroy() {
        isDestroyed = true
        dismissProgressDialog()
        closeInfoWindowLayout()
        super.onDestroy()
        try {
            if (mapView != null) {
                mapView!!.onDestroy()
            } else {
                Log.w(TAG, "MapView is null")
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

        // logout();
    }

    override fun onStart() {
        super.onStart()
        scheduledThreadPoolExecutor = ScheduledThreadPoolExecutor(1)
        scheduledThreadPoolExecutor!!.scheduleAtFixedRate(TimerTask, 60, 60, TimeUnit.SECONDS)

        isDestroyed = false
    }

    override fun onStop() {
        super.onStop()
        if (scheduledThreadPoolExecutor != null) {
            scheduledThreadPoolExecutor!!.shutdown()
        }

        dismissProgressDialog()
        closeInfoWindowLayout()

        isDestroyed = false
        // logout();
    }

    override fun onLowMemory() {
        super.onLowMemory()
        try {
            if (mapView != null) {
                mapView!!.onLowMemory()
            } else {
                Log.w(TAG, "MapView is null")
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

        dismissProgressDialog()
        closeInfoWindowLayout()
    }

    private fun dismissProgressDialog() {
        if (progressLayout.visibility == View.VISIBLE) {
            progressText.text = ""
            progressLayout.visibility = View.GONE
            if (googleMap != null) {
                googleMap!!.uiSettings.isZoomGesturesEnabled = true
                googleMap!!.uiSettings.isRotateGesturesEnabled = true
                googleMap!!.uiSettings.isScrollGesturesEnabled = true
            }
        }
    }

    override fun onMapReady(map: GoogleMap) {
        googleMap = map
    }

    override fun onMapLoaded() {
        if (activity != null) {
            if (!activity!!.isFinishing) {
                val connectivityManager = activity!!.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
                val activeNetworkInfo = connectivityManager.activeNetworkInfo
                if (activeNetworkInfo != null && activeNetworkInfo.isConnected) {
                    currentlocation()
                } else {
                    Toast.makeText(context, if (pref.getString("language", "0") == "0") R.string.internet_connectivity_check else R.string.internet_connectivity_check_esp, Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    private inner class getGeocodeAddress : AsyncTask<String, Void, String>() {
        override fun doInBackground(vararg url: String): String? {
            // TODO Auto-generated method stub
            var res: String? = null
            try {
                response = CustomHttpClient.executeHttpPost(url[0])
                res = response!!.toString()
                //res= res.replaceAll("\\s+","");
            } catch (e: Exception) {
                e.printStackTrace()
            }

            return res
        }//close doInBackground

        override fun onPostExecute(result: String?) {
            if (result != null) {
                if (result !== "") {
                    try {
                        val addrObject = JSONObject(result)
                        val addressArray = addrObject.getJSONArray("results")
                        if (addressArray != null) {
                            if (addressArray.length() > 0) {
                                val addressObj = addressArray.getJSONObject(0)
                                var addr: String? = ""
                                addr = addressObj.get("formatted_address").toString()
                                if (addr != null && addr !== "") {
                                    Address = addr
                                } else {
                                    val address_components_str = addressObj.get("address_components").toString()
                                    val address_components_arr = JSONArray(address_components_str)
                                    if (address_components_arr != null) {
                                        for (i in 0 until address_components_arr.length()) {
                                            try {
                                                val obj = address_components_arr.getJSONObject(i)
                                                if (obj != null) {
                                                    try {
                                                        val component = obj.get("long_name").toString()
                                                        if (component != null) {
                                                            if (i == 0) {
                                                                addr += component
                                                            } else {
                                                                addr += ", $component"
                                                            }
                                                        }
                                                    } catch (e: Exception) {
                                                        e.printStackTrace()
                                                    }

                                                }
                                            } catch (e: Exception) {
                                                e.printStackTrace()
                                            }
                                        }

                                        if (addr != null && addr !== "") {
                                            Address = addr
                                        }
                                    }
                                }
                            }
                        }

                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                }
            }

            if (Address != null && Address !== "") {
                /* if (current_marker != null) {
                    current_marker.showInfoWindow();
                } else {
                    marker_list[current_ind].showInfoWindow();
                } */
                showInfoWindow()
            } else {
                val getLocationAndroidGeocoder = getLocationAndroidGeocoder()
                getLocationAndroidGeocoder.execute()
            }
        }
    }

    private inner class getLocationAndroidGeocoder : AsyncTask<String, Void, String>() {
        override fun doInBackground(vararg url: String): String {
            // TODO Auto-generated method stub
            val res: String? = null
            var address_full = ""

            if (thisContext != null) {
                val geoCoder = Geocoder(thisContext)
                var matches: List<android.location.Address>? = null
                try {
                    val latitude = lat!![current_ind]
                    val longitude = lng!![current_ind]
                    matches = geoCoder.getFromLocation(latitude!!, longitude!!, 1)
                } catch (e: Exception) {
                    e.printStackTrace()
                }

                var address: android.location.Address? = null
                if (matches != null) {
                    address = if (matches.isEmpty()) null else matches[0]
                }

                if (address != null) {
                    if (address.getAddressLine(0) == null) {
                        address_full += if (address.locality == null) "" else address.locality + " "
                        address_full += if (address.adminArea == null) "" else address.adminArea + " "
                        address_full += if (address.countryCode == null) "" else address.countryCode
                    } else {
                        address_full = address.getAddressLine(0)
                    }
                }
            }

            return address_full

        }//close doInBackground

        override fun onPostExecute(result: String?) {
            if (result != null && result !== "") {
                Address = result
            }

            /* if(current_marker != null) {
                current_marker.showInfoWindow();
            }else{
                marker_list[current_ind].showInfoWindow();
            } */
            showInfoWindow()
        }
    }

    inner class getCurrentLocation : AsyncTask<String, Void, String>(), DialogInterface.OnCancelListener {

        override fun onPreExecute() {
            super.onPreExecute()
            if (!isDestroyed) {
                progressLayout.visibility = View.VISIBLE
                progressText.setText(if (pref.getString("language", "0") == "0") R.string.please_wait else R.string.please_wait_esp)
            }
        }

        override fun doInBackground(vararg url: String): String? {
            // TODO Auto-generated method stub
            var res: String? = null
            try {
                response = CustomHttpClient.executeHttpPost(url[0], url[1])
                res = response!!.toString()
            } catch (e: Exception) {
                e.printStackTrace()
            }

            return res
        }//close doInBackground

        override fun onPostExecute(result: String) {
            var responsefailed: Boolean? = false
            dismissProgressDialog()
            closeInfoWindowLayout()

            if (!isDestroyed) {
                response = result

                if (response != null && response !== "") {
                    try {
                        val respjson = JSONObject(response)
                        if (respjson.get("status") == "true") {
                            var jsondata = JSONObject()
                            jsondata = respjson.getJSONObject("data")
                            val driverarray = jsondata.getJSONArray("driverdata")
                            // Log.d("-------------",driverarray.length()+" ");
                            num_drivers = driverarray.length()
                            Name = arrayOfNulls(num_drivers)
                            lat = arrayOfNulls(num_drivers)
                            lng = arrayOfNulls(num_drivers)
                            LastRead = arrayOfNulls(num_drivers)
                            Battery = arrayOfNulls(num_drivers)
                            Speed = arrayOfNulls(num_drivers)
                            Direction = arrayOfNulls(num_drivers)
                            Event = arrayOfNulls(num_drivers)

                            var driverobj = JSONObject()
                            for (i in 0 until num_drivers) {
                                driverobj = driverarray.getJSONObject(i)
                                Name!![i] = driverobj.get("drivername").toString()
                                lat!![i] = java.lang.Double.parseDouble(driverobj.get("lat").toString())
                                lng!![i] = java.lang.Double.parseDouble(driverobj.get("long").toString())
                                LastRead!![i] = driverobj.get("active").toString()
                                Battery!![i] = driverobj.get("batterylevel").toString()
                                Speed[i] = driverobj.get("speed").toString()
                                Direction[i] = driverobj.get("direction").toString()
                                Event[i] = driverobj.get("event").toString()
                            }
                        } else {
                            if (activity != null) {
                                if (!activity!!.isFinishing) {
                                    val alertDialogBuilder = AlertDialog.Builder(activity)
                                    val alertDialog = alertDialogBuilder.create()
                                    alertDialogBuilder.setTitle(if (pref.getString("language", "0") == "0") R.string.authentication_error else R.string.authentication_error_esp)
                                    alertDialogBuilder.setMessage(if (pref.getString("language", "0") == "0") R.string.re_login else R.string.re_login_esp)
                                    alertDialogBuilder.setNeutralButton(activity!!.resources.getString(if (pref.getString("language", "0") == "0") R.string.ok else R.string.ok_esp)
                                    ) { dialog, which ->
                                        val data = JSONObject()
                                        try {
                                            val isInserted = credentialStore.setFlag(0, "", "", "", "", "")
                                            dialog.dismiss()
                                            if (activity != null) {
                                                if (!activity!!.isFinishing) {
                                                    val i = Intent(activity, SplashScreen::class.java)
                                                    i.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                                                    startActivity(i)
                                                }
                                            }
                                        } catch (e: Exception) {
                                            e.printStackTrace()
                                            dialog.dismiss()
                                        }
                                    }
                                    alertDialogBuilder.show()
                                }
                            }
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                        responsefailed = true
                    }

                } else {
                    responsefailed = true
                }

                if (responsefailed!!) {
                    if (activity != null) {
                        if (!activity!!.isFinishing) {
                            val alertDialogBuilder = AlertDialog.Builder(activity)
                            val alertDialog = alertDialogBuilder.create()
                            alertDialogBuilder.setTitle(if (pref.getString("language", "0") == "0") R.string.data_sync_issue else R.string.data_sync_issue_esp)
                            alertDialogBuilder.setMessage(if (pref.getString("language", "0") == "0") R.string.re_login_and_try_again else R.string.re_login_and_try_again_esp)
                            alertDialogBuilder.setPositiveButton(if (pref.getString("language", "0") == "0") R.string.ok else R.string.ok_esp
                            ) { dialog, which -> dialog.dismiss() }
                            alertDialogBuilder.show()
                        }
                    }
                } else if (num_drivers == 0) {
                    if (activity != null) {
                        if (!activity!!.isFinishing) {
                            val alertDialogBuilder = AlertDialog.Builder(activity)
                            val alertDialog = alertDialogBuilder.create()
                            alertDialogBuilder.setMessage(if (pref.getString("language", "0") == "0") R.string.no_data_available else R.string.no_data_available_esp)
                            alertDialogBuilder.setNeutralButton(activity!!.resources.getString(if (pref.getString("language", "0") == "0") R.string.ok else R.string.ok_esp)) { arg0, arg1 -> arg0.dismiss() }
                            alertDialogBuilder.show()
                        }
                    }
                } else {
                    editor.putString("mapSelect", "Normal")
                    editor.commit()
                    try {
                        editor.putString("drivers", JSONArray(Name).toString())
                        editor.apply()
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }

                    loadMap()
                }
            }
        }

        override fun onCancel(dialog: DialogInterface) {
            cancel(true)
        }
    }

    fun closeInfoWindowLayout() {
        infowindowlayout.visibility = View.INVISIBLE
        if (googleMap != null) {
            googleMap!!.uiSettings.isZoomGesturesEnabled = true
            googleMap!!.uiSettings.isRotateGesturesEnabled = true
            googleMap!!.uiSettings.isScrollGesturesEnabled = true
            if (bounds != null) {
                googleMap!!.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 80))
            }
            val currentzoom = googleMap!!.cameraPosition.zoom
            if (currentzoom > 13) {
                if (lat != null && lng != null) {
                    var lat_avg: Double? = 0.0
                    var lng_avg: Double? = 0.0
                    var lat_sum: Double? = 0.0
                    var lng_sum: Double? = 0.0
                    if (lat_sum != null) {
                        if (lng_sum != null) {
                            for (i in lat!!.indices) {
                                lat_sum += lat!![i]!!
                                lng_sum += lng!![i]!!
                            }
                        }
                    }
                    lat_avg = lat_sum!! / lat!!.size
                    lng_avg = lng_sum!! / lng!!.size
                    googleMap!!.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(lat_avg, lng_avg), 13f))
                }
            }
        }
    }

    fun showInfoWindow() {
        tvClose.setOnClickListener { closeInfoWindowLayout() }

        if (Name!![current_ind] != null) {
            val drivername = Name!![current_ind]
            tvName.text = drivername
        } else {
            Log.w(TAG, "Name value null for index$current_ind")
        }

        if (Speed[current_ind] != null) {
            val speedStr = (if (pref.getString("language", "0") == "0") "Speed: " else "Velocidad: ") + Speed[current_ind] + (if (pref.getString("language", "0") == "0") " mi/h" else " km/h")
            val spannedStr = getSpannedText(speedStr, 0, if (pref.getString("language", "0") == "0") 5 else 9)
            tvSpeed.text = spannedStr
        } else {
            Log.w(TAG, "Speed value null for index$current_ind")
        }

        if (Direction[current_ind] != null) {
            var dir = Direction[current_ind]
            if(pref.getString("langauge", "0").equals("1"))
                if(dir.equals("W"))
                    dir = "O"
                else if(dir.equals("NW"))
                    dir = "NO"
                else if(dir.equals("SW"))
                    dir = "SO"

            val directionStr = "Dir.: " + dir
            val spannedStr = getSpannedText(directionStr, 0, 4)
            tvDirection.text = spannedStr
        } else {
            Log.w(TAG, "Direction value null for index$current_ind")
        }

        if (Event[current_ind] != null) {
            val eventStr = (if (pref.getString("language", "0") == "0") "Event: " else "Evento: ") + Event[current_ind]
            val spannedStr = getSpannedText(eventStr, 0, if (pref.getString("language", "0") == "0") 5 else 6)
            tvEvent.text = spannedStr
        } else {
            Log.w(TAG, "Event value null for index$current_ind")
        }

        if (LastRead!![current_ind] != null) {
            var currentStr = (if (pref.getString("language", "0") == "0") "Last Read: " else "Última lectura: Hace ") + LastRead!![current_ind]
            if (pref.getString("language", "0") == "1") {
                currentStr = currentStr.replace("days", "dias")
                currentStr = currentStr.replace("minutes", "minutos")
                currentStr = currentStr.replace("ago", "")
            }
            val spannedStr = getSpannedText(currentStr, 0, if (pref.getString("language", "0") == "0") 9 else 14)
            tvDate.text = spannedStr
        } else {
            Log.w(TAG, "LastRead value null for index$current_ind")
        }

        val currentOrientation = resources.configuration.orientation
        if (Battery!![current_ind] != null) {
            tvBattery.text = Battery!![current_ind] + "%"
            if (java.lang.Double.parseDouble(Battery!![current_ind]) == 0.0)
                batteryLevel.setImageDrawable(ResourcesCompat.getDrawable(resources, R.drawable.battery0, null))
            else if (java.lang.Double.parseDouble(Battery!![current_ind]) >= 0 && java.lang.Double.parseDouble(Battery!![current_ind]) < 17)
                batteryLevel.setImageDrawable(ResourcesCompat.getDrawable(resources, R.drawable.battery1, null))
            else if (java.lang.Double.parseDouble(Battery!![current_ind]) >= 17 && java.lang.Double.parseDouble(Battery!![current_ind]) < 34)
                batteryLevel.setImageDrawable(ResourcesCompat.getDrawable(resources, R.drawable.battery2, null))
            else if (java.lang.Double.parseDouble(Battery!![current_ind]) >= 34 && java.lang.Double.parseDouble(Battery!![current_ind]) < 51)
                batteryLevel.setImageDrawable(ResourcesCompat.getDrawable(resources, R.drawable.battery3, null))
            else if (java.lang.Double.parseDouble(Battery!![current_ind]) >= 51 && java.lang.Double.parseDouble(Battery!![current_ind]) < 68)
                batteryLevel.setImageDrawable(ResourcesCompat.getDrawable(resources, R.drawable.battery4, null))
            else if (java.lang.Double.parseDouble(Battery!![current_ind]) >= 68 && java.lang.Double.parseDouble(Battery!![current_ind]) < 85)
                batteryLevel.setImageDrawable(ResourcesCompat.getDrawable(resources, R.drawable.battery5, null))
            else if (java.lang.Double.parseDouble(Battery!![current_ind]) >= 85 && java.lang.Double.parseDouble(Battery!![current_ind]) < 97)
                batteryLevel.setImageDrawable(ResourcesCompat.getDrawable(resources, R.drawable.battery6, null))
            else if (java.lang.Double.parseDouble(Battery!![current_ind]) >= 97)
                batteryLevel.setImageDrawable(ResourcesCompat.getDrawable(resources, R.drawable.battery7, null))
            else
                batteryLevel.setImageDrawable(ResourcesCompat.getDrawable(resources, R.drawable.battery4, null))
        } else {
            Log.w(TAG, "Battery value null for index$current_ind")
        }

        if (Address != null) {
            val addressStr = (if (pref.getString("language", "0") == "0") "Address: " else "Localización: ") + Address!!
            val spannedStr = getSpannedText(addressStr, 0, if (pref.getString("language", "0") == "0") 7 else 12)
            tvAddress.text = spannedStr
        } else {
            Log.w(TAG, "Address null for index$current_ind")
        }

        tvMapIt.text = getString(if (pref.getString("language", "0") == "0") R.string.map_it else R.string.map_it_esp)
        tvMapIt.setOnClickListener {
            val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse("http://maps.google.com/maps?q=" + lat!![current_ind] + "," + lng!![current_ind]))
            startActivity(browserIntent)
        }

        infowindowlayout.visibility = View.VISIBLE
        googleMap!!.uiSettings.isZoomGesturesEnabled = false
        googleMap!!.uiSettings.isRotateGesturesEnabled = false
        googleMap!!.uiSettings.isScrollGesturesEnabled = false
    }

    companion object {
        lateinit var rootView: View
        private val TAG = "Maps Acitivty"
        var response: String? = null
        lateinit var rb_normal: Button
        lateinit var rb_satellite: Button
        lateinit var rb_hybrid: Button
        var googleMap: GoogleMap? = null
        var marker_list: Array<Marker?>? = null
        lateinit var current_marker: Marker
        var lat: Array<Double?>? = null
        var lng: Array<Double?>? = null
        var Address: String? = ""
        var Battery: Array<String?>? = null
        var LastRead: Array<String?>? = null
        var Name: Array<String?>? = null
        lateinit var Speed: Array<String?>
        lateinit var Event: Array<String?>
        lateinit var Direction: Array<String?>
        lateinit var zoomplus: ImageView
        lateinit var zoomminus: ImageView
        lateinit var tvClose: ImageView
        lateinit var batteryLevel: ImageView
        var num_drivers = 0
        var current_ind = 0
        lateinit var google_maps_api: String
        var scheduledThreadPoolExecutor: ScheduledThreadPoolExecutor? = null
        lateinit var progressLayout: LinearLayout
        lateinit var progressBar: ProgressBar
        lateinit var progressText: TextView
    }
}// Required empty public constructor
