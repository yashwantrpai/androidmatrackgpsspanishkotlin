package com.matrackinc.matrackgpsspanish.fragment

import android.app.DatePickerDialog
import android.content.Context
import android.content.DialogInterface
import android.content.SharedPreferences
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentTransaction
import android.app.AlertDialog
import android.text.Html
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.DatePicker
import android.widget.ImageView
import android.widget.Spinner
import android.widget.TextView

import com.matrackinc.matrackgpsspanish.activity.HomeActivity
import com.matrackinc.matrackgpsspanish.db.DatabaseHelper
import com.matrackinc.matrackgpsspanish.R

import org.json.JSONArray
import org.json.JSONException

import java.text.SimpleDateFormat
import java.util.ArrayList
import java.util.Calendar
import java.util.Date

import android.content.Context.MODE_PRIVATE

/**
 * A simple [Fragment] subclass.
 */
class travellogform : Fragment() {
    // public static ArrayList travellog_values = new ArrayList();
    internal var context: Context? = null
    internal lateinit var datePickerDialog: DatePickerDialog
    internal lateinit var dbHelper: DatabaseHelper
    lateinit var pref: SharedPreferences
    internal lateinit var dataPoint_str: String
    internal lateinit var driver_str: String
    lateinit var editor: SharedPreferences.Editor

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.travellog1, container, false)
        context = this.getContext()
        dbHelper = DatabaseHelper()

        pref = context!!.getSharedPreferences("DataStore", MODE_PRIVATE)
        editor = pref.edit()

        travellog_title = rootView.findViewById<View>(R.id.travellog_title) as TextView
        start_date_title = rootView.findViewById<View>(R.id.start_date_title) as TextView
        end_date_title = rootView.findViewById<View>(R.id.end_date_title) as TextView
        driver_title = rootView.findViewById<View>(R.id.driver_title) as TextView
        datapoint_title = rootView.findViewById<View>(R.id.datapoint_title) as TextView
        submit = rootView.findViewById<View>(R.id.submit) as Button

        travellog_title!!.setText(if (pref.getString("language", "0") == "0") R.string.travellog else R.string.travellog_esp)
        start_date_title!!.setText(if (pref.getString("language", "0") == "0") R.string.start_date else R.string.start_date_esp)
        end_date_title!!.setText(if (pref.getString("language", "0") == "0") R.string.end_date else R.string.end_date_esp)
        driver_title!!.setText(if (pref.getString("language", "0") == "0") R.string.driver else R.string.driver_esp)
        datapoint_title!!.setText(if (pref.getString("language", "0") == "0") R.string.datapoint else R.string.datapoint_esp)
        submit!!.setText(if (pref.getString("language", "0") == "0") R.string.submit else R.string.submit_esp)

        if (pref.getString("curfile", "") == null || !HomeActivity.equalStrings(pref.getString("curfile", "")!!.toString(), "travellogform")) {
            editor.putString("startdate", "")
            editor.putString("enddate", "")
            editor.putString("datapoint", "")
            editor.putString("driver", "")
            editor.putString("curfile", "travellogform")
        }
        editor.commit()


        // Setting default values
        // Prefilling start date and end date

        val SD = rootView.findViewById<View>(R.id.startDate_text) as TextView
        val ED = rootView.findViewById<View>(R.id.endDate_text) as TextView
        val DataPoint = rootView.findViewById<View>(R.id.selectDatapoint_text) as Spinner
        DataPoint.setSelection(0)
        val Driver = rootView.findViewById<View>(R.id.selectDriver_text) as Spinner
        Driver.setSelection(0)

        SD.setOnClickListener(object : View.OnClickListener {
            internal var c = Calendar.getInstance()
            internal var day = c.get(Calendar.DAY_OF_MONTH)
            internal var month = c.get(Calendar.MONTH)
            internal var year = c.get(Calendar.YEAR)
            internal lateinit var dt: String
            override fun onClick(view: View) {
                if (pref.getString("startdate", "") != null && pref.getString("startdate", "") !== "") {
                    val date_split = pref.getString("startdate", "")!!.split("/".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                    day = Integer.parseInt(if (pref.getString("language", "0") == "0") date_split[1] else date_split[0])
                    month = Integer.parseInt(if (pref.getString("language", "0") == "0") date_split[0] else date_split[1])
                    year = Integer.parseInt(date_split[2])
                }

                // Click action
                datePickerDialog = DatePickerDialog(context!!, R.style.datepicker, DatePickerDialog.OnDateSetListener { datePicker, selectedYear, selectedMonth, selectedDay ->
                    val date = Date()
                    val cal = Calendar.getInstance()
                    cal.set(selectedYear, selectedMonth, selectedDay)
                    date.time = cal.time.time

                    val dateFormat = SimpleDateFormat(getString(if (pref.getString("language", "0") == "0") R.string.date_format else R.string.date_format_esp))
                    dt = dateFormat.format(date)
                    SD.text = dt
                    // travellog_values.set(0,SD.getText());
                    editor.putString("startdate", SD.text.toString())
                    editor.commit()
                    Log.d("Selected Date", dt)
                }, year, month - 1, day)
                // Set Max date and Min date for add date
                datePickerDialog.datePicker.maxDate = Date().time
                // datePickerDialog.setTitle("Pick start date");
                datePickerDialog.show()
            }
        })

        ED.setOnClickListener(object : View.OnClickListener {
            internal var c = Calendar.getInstance()
            internal var day = c.get(Calendar.DAY_OF_MONTH)
            internal var month = c.get(Calendar.MONTH)
            internal var year = c.get(Calendar.YEAR)
            internal lateinit var dt: String
            override fun onClick(view: View) {
                if (pref.getString("enddate", "") != null && pref.getString("enddate", "") !== "") {
                    val date_split = pref.getString("enddate", "")!!.split("/".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                    day = Integer.parseInt(if (pref.getString("language", "0") == "0") date_split[1] else date_split[0])
                    month = Integer.parseInt(if (pref.getString("language", "0") == "0") date_split[0] else date_split[1])
                    year = Integer.parseInt(date_split[2])
                }

                // Click action
                datePickerDialog = DatePickerDialog(context!!, R.style.datepicker, DatePickerDialog.OnDateSetListener { datePicker, selectedYear, selectedMonth, selectedDay ->
                    val date = Date()
                    val cal = Calendar.getInstance()
                    cal.set(selectedYear, selectedMonth, selectedDay)
                    date.time = cal.time.time

                    val dateFormat = SimpleDateFormat(getString(if (pref.getString("language", "0") == "0") R.string.date_format else R.string.date_format_esp))
                    dt = dateFormat.format(date)
                    ED.text = dt
                    // travellog_values.set(1,ED.getText());
                    editor.putString("enddate", ED.text.toString())
                    editor.commit()
                    Log.d("Selected Date", dt)
                }, year, month - 1, day)
                // Set Max date and Min date for add date
                datePickerDialog.datePicker.maxDate = Date().time
                // datePickerDialog.setTitle("Pick end date");
                datePickerDialog.show()
            }
        })

        // End Date
        val date = Date()
        val curFormater = SimpleDateFormat(getString(if (pref.getString("language", "0") == "0") R.string.date_format else R.string.date_format_esp))
        val EndDate = curFormater.format(date)
        Log.d(TAG, EndDate)
        ED.text = EndDate
        SD.text = EndDate

        // driver adapter and selected item
        val dataPoint_adapter = ArrayAdapter<CharSequence>(context!!, R.layout.spinner_item, resources.getStringArray(if (pref.getString("language", "0") == "0") R.array.datapoints else R.array.datapoints_esp))
        dataPoint_adapter.setDropDownViewResource(R.layout.spinner_item_dropdown)
        DataPoint.adapter = dataPoint_adapter
        DataPoint.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(adapterView: AdapterView<*>, view: View, i: Int, l: Long) {
                dataPoint_str = adapterView.getItemAtPosition(i).toString()
            }

            override fun onNothingSelected(adapterView: AdapterView<*>) {

            }
        }

        val drivers_str = pref.getString("drivers", "")
        var drivers: Array<String?>? = null
        try {
            if (drivers_str != null && drivers_str !== "") {
                val drivers_arr = JSONArray(drivers_str)
                if (drivers_arr != null) {
                    drivers = arrayOfNulls(drivers_arr.length())
                    for (i in 0 until drivers_arr.length()) {
                        drivers[i] = drivers_arr.get(i).toString()
                    }
                }
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        }

        if (drivers != null && drivers.size > 0) {
            val driver_adapter = ArrayAdapter<CharSequence>(context!!, R.layout.spinner_item, drivers)
            driver_adapter.setDropDownViewResource(R.layout.spinner_item_dropdown)
            Driver.adapter = driver_adapter
            Driver.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(adapterView: AdapterView<*>, view: View, i: Int, l: Long) {
                    driver_str = adapterView.getItemAtPosition(i).toString()
                }

                override fun onNothingSelected(adapterView: AdapterView<*>) {

                }
            }
        } else {
            Log.w(TAG, "home.Name is null")
            val alertDialogBuilder = AlertDialog.Builder(activity)
            val alertDialog = alertDialogBuilder.create()
            alertDialogBuilder.setTitle(if (pref.getString("language", "0") == "0") R.string.data_sync_issue else R.string.data_sync_issue_esp)
            alertDialogBuilder.setMessage(if (pref.getString("language", "0") == "0") R.string.re_login_and_try_again else R.string.re_login_and_try_again_esp)
            alertDialogBuilder.setNeutralButton(activity!!.resources.getString(if (pref.getString("language", "0") == "0") R.string.ok else R.string.ok_esp)
            ) { dialog, which -> dialog.dismiss() }
            alertDialogBuilder.show()
        }

        editor.putString("startdate", EndDate)
        editor.putString("enddate", EndDate)
        editor.commit()

        // Back button
        val back_btn = rootView.findViewById<View>(R.id.back) as ImageView
        back_btn.setOnClickListener {
            val reportsFragment = reports()
            val fragmentTransaction = fragmentManager!!.beginTransaction()
            fragmentTransaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
            editor.putString("CURRENT_TAG", HomeActivity.TAG_REPORTS)
            editor.commit()
            fragmentTransaction.replace(R.id.frame, reportsFragment, HomeActivity.TAG_REPORTS)
            fragmentTransaction.commitAllowingStateLoss()
        }

        submit.setOnClickListener {
            if (SD.text.toString().matches("".toRegex()) || ED.text.toString().matches("".toRegex())) {
                val no_values = ArrayList<String>()
                no_values.add("")
                no_values.add("")
                if (SD.text.toString().matches("".toRegex())) {
                    no_values.set(0, getString(if (pref.getString("language", "0") == "0") R.string.start_date else R.string.start_date_esp))
                }
                if (ED.text.toString().matches("".toRegex())) {
                    no_values.set(1, getString(if (pref.getString("language", "0") == "0") R.string.end_date else R.string.end_date_esp))
                }
                val Alert = AlertDialog.Builder(context).create()
                Alert.setMessage(Html.fromHtml(getContext()!!.resources.getString(if (pref.getString("language", "0") == "0") R.string.please_fill_values else R.string.please_fill_values_esp) + ": <br>" + no_values.get(0).toString() + "<br>" + no_values.get(1).toString()))
                Alert.setButton(AlertDialog.BUTTON_NEUTRAL, activity!!.resources.getString(if (pref.getString("language", "0") == "0") R.string.ok else R.string.ok_esp)
                ) { dialog, which -> dialog.dismiss() }
                Alert.show()
            } else {
                // val sD = Date(SD.text.toString())
                // val eD = Date(ED.text.toString())
                // if (eD.time >= sD.time) {
                    editor.putString("datapoint", dataPoint_str)
                    editor.putString("driver", driver_str)
                    editor.commit()
                    val travellogFragment = travellog()
                    val fragmentTransaction = fragmentManager!!.beginTransaction()
                    fragmentTransaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                    editor.putString("CURRENT_TAG", HomeActivity.TAG_TRAVELLOG)
                    editor.commit()
                    fragmentTransaction.replace(R.id.frame, travellogFragment, HomeActivity.TAG_TRAVELLOG)
                    fragmentTransaction.commitAllowingStateLoss()
                /* } else {
                    val DateAlert = AlertDialog.Builder(activity).create()
                    DateAlert.setMessage(activity!!.resources.getString(if (pref.getString("language", "0") == "0") R.string.end_date_greater_than_start_date else R.string.end_date_greater_than_start_date_esp))
                    DateAlert.setButton(AlertDialog.BUTTON_NEUTRAL, activity!!.resources.getString(if (pref.getString("language", "0") == "0") R.string.ok else R.string.ok_esp)
                    ) { dialog, which -> dialog.dismiss() }
                    DateAlert.show()
                } */
            }
        }

        return rootView
    }

    companion object {
        // Travel Log Date-range activity
        private val TAG = "Travellog1-dateRange"
        lateinit var submit: Button
        var travellog_title: TextView? = null
        var start_date_title: TextView? = null
        var end_date_title: TextView? = null
        var datapoint_title: TextView? = null
        var driver_title: TextView? = null
    }
}// Required empty public constructor
