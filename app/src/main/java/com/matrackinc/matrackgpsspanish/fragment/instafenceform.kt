package com.matrackinc.matrackgpsspanish.fragment


import android.app.AlertDialog
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.matrackinc.matrackgpsspanish.R
import com.matrackinc.matrackgpsspanish.activity.HomeActivity
import com.matrackinc.matrackgpsspanish.db.DatabaseHelper
import org.json.JSONArray
import org.json.JSONException


// TODO: Rename parameter arguments, choose names that match

/**
 * A simple [Fragment] subclass.
 *
 */
class instafenceform : Fragment() {


    internal var context: Context? = null
    internal lateinit var dbHelper: DatabaseHelper
    lateinit var pref: SharedPreferences
    lateinit var editor: SharedPreferences.Editor
    lateinit var driver_str: String

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.instafenceform, container, false) // Loading instafence date-range layout
        context = this.getContext()
        dbHelper = DatabaseHelper()

        pref = context!!.getSharedPreferences("DataStore", Context.MODE_PRIVATE)
        editor = pref.edit()

        if (pref.getString("curfile", "") == null || !HomeActivity.equalStrings(pref.getString("curfile", "")!!.toString(), "instafenceform")) {
            editor.putString("driver", "")
            editor.putString("curfile", "instafenceform")
        }
        editor.commit()

        insta_title = rootView.findViewById<View>(R.id.insta_title) as TextView
        driver_title = rootView.findViewById<View>(R.id.driver_title) as TextView
        submit = rootView.findViewById<View>(R.id.submit) as Button

        insta_title!!.setText(R.string.nav_instafence_esp)
        driver_title!!.setText(R.string.driver_esp)
        submit!!.setText(R.string.submit_insta_esp)


        val Driver = rootView.findViewById<View>(R.id.selectDriver_text) as Spinner
        Driver.setSelection(0)
        // Driver adapter and selected item
        val drivers_str = pref.getString("drivers", "")
        var drivers: Array<String?>? = null
        try {
            if (drivers_str != null && drivers_str !== "") {
                val drivers_arr = JSONArray(drivers_str)
                if (drivers_arr != null) {
                    drivers = arrayOfNulls(drivers_arr.length())
                    for (i in 0 until drivers_arr.length()) {
                        drivers[i] = drivers_arr.get(i).toString()
                    }
                }
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        }

        if (drivers != null && drivers.size > 0) {
            val driver_adapter = ArrayAdapter<CharSequence>(context!!, R.layout.spinner_item, drivers)
            driver_adapter.setDropDownViewResource(R.layout.spinner_item_dropdown)
            Driver.adapter = driver_adapter
            Driver.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(adapterView: AdapterView<*>, view: View, i: Int, l: Long) {
                    driver_str = adapterView.getItemAtPosition(i).toString()
                }

                override fun onNothingSelected(adapterView: AdapterView<*>) {

                }
            }
        } else {
            // Log.w(TAG, "home.Name is null");
            val alertDialogBuilder = AlertDialog.Builder(activity)
            val alertDialog = alertDialogBuilder.create()
            alertDialogBuilder.setTitle(R.string.data_sync_issue_esp)
            alertDialogBuilder.setMessage(R.string.re_login_and_try_again_esp)
            alertDialogBuilder.setNeutralButton(activity!!.resources.getString(R.string.ok_esp)
            ) { dialog, which -> dialog.dismiss() }
            alertDialogBuilder.show()
        }


        editor.commit()

        submit.setOnClickListener {

            //                val Alert = AlertDialog.Builder(context).create()
//                Alert.setMessage(Html.fromHtml(getContext()!!.resources.getString(if (pref.getString("language", "0") == "0") R.string.please_fill_values else R.string.please_fill_values_esp) + ": <br>" + no_values.get(0).toString() + "<br>" + no_values.get(1).toString()))
//                Alert.setButton(AlertDialog.BUTTON_NEUTRAL, context!!.resources.getString(if (pref.getString("language", "0") == "0") R.string.ok else R.string.ok_esp)
//                ) { dialog, which -> dialog.dismiss() }
//                Alert.show()

            // val sD = Date(SD.text.toString())
            // val eD = Date(ED.text.toString())
            // if (eD.time >= sD.time) {
            editor.putString("driver", driver_str)
            editor.commit()
            val instaFragment = instafence()
            val fragmentTransaction = fragmentManager!!.beginTransaction()
            fragmentTransaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
            editor.putString("CURRENT_TAG", HomeActivity.TAG_INSTA)
            editor.commit()
            fragmentTransaction.replace(R.id.frame, instaFragment, HomeActivity.TAG_INSTA)
            fragmentTransaction.commitAllowingStateLoss()
            /* } else {
                val DateAlert = AlertDialog.Builder(activity).create()
                DateAlert.setMessage(activity!!.resources.getString(if (pref.getString("language", "0") == "0") R.string.end_date_greater_than_start_date else R.string.end_date_greater_than_start_date_esp))
                DateAlert.setButton(AlertDialog.BUTTON_NEUTRAL, activity!!.resources.getString(if (pref.getString("language", "0") == "0") R.string.ok else R.string.ok_esp)
                ) { dialog, which -> dialog.dismiss() }
                DateAlert.show()
            } */
        }


        return rootView
    }

    companion object {

        // Insta activity
        var insta_title: TextView? = null
        var driver_title: TextView? = null
        lateinit var submit: Button
    }
}