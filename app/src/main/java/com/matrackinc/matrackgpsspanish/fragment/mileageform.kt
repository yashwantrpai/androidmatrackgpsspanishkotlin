package com.matrackinc.matrackgpsspanish.fragment

import android.app.DatePickerDialog
import android.content.Context
import android.content.DialogInterface
import android.content.SharedPreferences
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentTransaction
import android.app.AlertDialog
import android.text.Html
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.DatePicker
import android.widget.ImageView
import android.widget.Spinner
import android.widget.TextView

import com.matrackinc.matrackgpsspanish.activity.HomeActivity
import com.matrackinc.matrackgpsspanish.db.DatabaseHelper
import com.matrackinc.matrackgpsspanish.fragment.home
import com.matrackinc.matrackgpsspanish.fragment.mileage
import com.matrackinc.matrackgpsspanish.fragment.reports
import com.matrackinc.matrackgpsspanish.R

import org.json.JSONArray
import org.json.JSONException

import java.text.SimpleDateFormat

import android.content.Context.MODE_PRIVATE
import java.util.*

/**
 * A simple [Fragment] subclass.
 */
class mileageform : Fragment() {
    // public static ArrayList mileage_values = new ArrayList();
    internal lateinit var datePickerDialog: DatePickerDialog
    internal var context: Context? = null
    internal lateinit var dbHelper: DatabaseHelper
    internal lateinit var driver_str: String
    lateinit var pref: SharedPreferences
    lateinit var editor: SharedPreferences.Editor

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.mileage1, container, false)
        context = this.getContext()
        dbHelper = DatabaseHelper()

        pref = context!!.getSharedPreferences("DataStore", MODE_PRIVATE)
        editor = pref.edit()
        submit = rootView.findViewById<View>(R.id.submit) as Button

        mileage_title = rootView.findViewById<View>(R.id.mileage_title) as TextView
        start_date_title = rootView.findViewById<View>(R.id.start_date_title) as TextView
        end_date_title = rootView.findViewById<View>(R.id.end_date_title) as TextView
        driver_title = rootView.findViewById<View>(R.id.driver_title) as TextView

        mileage_title!!.setText(if (pref.getString("language", "0") == "0") R.string.mileage else R.string.mileage_esp)
        start_date_title!!.setText(if (pref.getString("language", "0") == "0") R.string.start_date else R.string.start_date_esp)
        end_date_title!!.setText(if (pref.getString("language", "0") == "0") R.string.end_date else R.string.end_date_esp)
        driver_title!!.setText(if (pref.getString("language", "0") == "0") R.string.driver else R.string.driver_esp)
        submit!!.setText(if (pref.getString("language", "0") == "0") R.string.submit else R.string.submit_esp)

        if (pref.getString("curfile", "") == null || !HomeActivity.equalStrings(pref.getString("curfile", "")!!.toString(), "mileageform")) {
            editor.putString("startdate", "")
            editor.putString("enddate", "")
            editor.putString("driver", "")
            editor.putString("curfile", "mileageform")
        }
        editor.commit()

        val SD = rootView.findViewById<View>(R.id.startDate_text) as TextView
        val ED = rootView.findViewById<View>(R.id.endDate_text) as TextView

        SD.setOnClickListener(object : View.OnClickListener {
            internal var c = Calendar.getInstance()
            internal var day = c.get(Calendar.DAY_OF_MONTH)
            internal var month = c.get(Calendar.MONTH)
            internal var year = c.get(Calendar.YEAR)
            internal lateinit var dt: String
            override fun onClick(view: View) {
                if (pref.getString("startdate", "") != null && pref.getString("startdate", "") !== "") {
                    val date_split = pref.getString("startdate", "")!!.split("/".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                    day = Integer.parseInt(if (pref.getString("language", "0") == "0") date_split[1] else date_split[0])
                    month = Integer.parseInt(if (pref.getString("language", "0") == "0") date_split[0] else date_split[1])
                    year = Integer.parseInt(date_split[2])
                }

                // Click action
                datePickerDialog = DatePickerDialog(context!!, R.style.datepicker, DatePickerDialog.OnDateSetListener { datePicker, selectedYear, selectedMonth, selectedDay ->
                    Log.d("Selected Date", "$selectedDay $selectedMonth $selectedYear")

                    val date = Date()
                    val cal = Calendar.getInstance()
                    cal.set(selectedYear, selectedMonth, selectedDay)
                    date.time = cal.time.time

                    val dateFormat = SimpleDateFormat(getString(if (pref.getString("language", "0") == "0") R.string.date_format else R.string.date_format_esp))
                    dt = dateFormat.format(date)
                    SD.text = dt
                    // mileage_values.set(0,SD.getText());
                    editor.putString("startdate", SD.text.toString())
                    editor.commit()
                    Log.d("Selected Date", dt)
                }, year, month - 1, day)
                // Set Max date and Min date for add date
                datePickerDialog.datePicker.maxDate = Date().time
                // datePickerDialog.setTitle("Pick start date");
                datePickerDialog.show()
            }
        })

        ED.setOnClickListener(object : View.OnClickListener {
            internal var c = Calendar.getInstance()
            internal var day = c.get(Calendar.DAY_OF_MONTH)
            internal var month = c.get(Calendar.MONTH)
            internal var year = c.get(Calendar.YEAR)
            internal lateinit var dt: String
            override fun onClick(view: View) {
                if (pref.getString("endate", "") != null && pref.getString("enddate", "") !== "") {
                    val date_split = pref.getString("enddate", "")!!.split("/".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                    day = Integer.parseInt(if (pref.getString("language", "0") == "0") date_split[1] else date_split[0])
                    month = Integer.parseInt(if (pref.getString("language", "0") == "0") date_split[0] else date_split[1])
                    year = Integer.parseInt(date_split[2])
                }

                // Click action
                datePickerDialog = DatePickerDialog(context!!, R.style.datepicker, DatePickerDialog.OnDateSetListener { datePicker, selectedYear, selectedMonth, selectedDay ->
                    Log.d("Selected Date", "$selectedDay $selectedMonth $selectedYear")

                    val date = Date()
                    val cal = Calendar.getInstance()
                    cal.set(selectedYear, selectedMonth, selectedDay)
                    date.time = cal.time.time

                    val dateFormat = SimpleDateFormat(getString(if (pref.getString("language", "0") == "0") R.string.date_format else R.string.date_format_esp))
                    dt = dateFormat.format(date)
                    ED.text = dt
                    // mileage_values.set(1,ED.getText());
                    editor.putString("enddate", ED.text.toString())
                    editor.commit()
                    Log.d("Selected Date", dt)
                }, year, month - 1, day)
                // Set Max date and Min date for add date
                datePickerDialog.datePicker.maxDate = Date().time
                // datePickerDialog.setTitle("Pick end date");
                datePickerDialog.show()
            }
        })

        // End Date
        val date = Date()
        val curFormater = SimpleDateFormat(getString(if (pref.getString("language", "0") == "0") R.string.date_format else R.string.date_format_esp))
        val EndDate = curFormater.format(date)
        Log.d(TAG, EndDate)
        ED.text = EndDate
        SD.text = EndDate

        val Driver = rootView.findViewById<View>(R.id.selectDriver_text) as Spinner
        Driver.setSelection(0)
        // units adapter and selected item
        val drivers_str = pref.getString("drivers", "")
        var drivers: Array<String?>? = null
        try {
            if (drivers_str != null && drivers_str !== "") {
                val drivers_arr = JSONArray(drivers_str)
                if (drivers_arr != null) {
                    drivers = arrayOfNulls(drivers_arr.length())
                    for (i in 0 until drivers_arr.length()) {
                        drivers[i] = drivers_arr.get(i).toString()
                    }
                }
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        }

        if (drivers != null && drivers.size > 0) {
            val driver_adapter = ArrayAdapter<CharSequence>(context!!, R.layout.spinner_item, drivers)
            driver_adapter.setDropDownViewResource(R.layout.spinner_item_dropdown)
            Driver.adapter = driver_adapter
            Driver.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(adapterView: AdapterView<*>, view: View, i: Int, l: Long) {
                    driver_str = adapterView.getItemAtPosition(i).toString()
                }

                override fun onNothingSelected(adapterView: AdapterView<*>) {

                }
            }
        } else {
            Log.w(TAG, "home.Name is null")
            val alertDialogBuilder = AlertDialog.Builder(activity)
            val alertDialog = alertDialogBuilder.create()
            alertDialogBuilder.setTitle(if (pref.getString("language", "0") == "0") R.string.data_sync_issue else R.string.data_sync_issue_esp)
            alertDialogBuilder.setMessage(if (pref.getString("language", "0") == "0") R.string.re_login_and_try_again else R.string.re_login_and_try_again_esp)
            alertDialogBuilder.setNeutralButton(activity!!.resources.getString(if (pref.getString("language", "0") == "0") R.string.ok else R.string.ok_esp)
            ) { dialog, which -> dialog.dismiss() }
            alertDialogBuilder.show()
        }

        editor.putString("startdate", EndDate)
        editor.putString("enddate", EndDate)
        editor.commit()

        // Back button
        val back_btn = rootView.findViewById<View>(R.id.back) as ImageView
        back_btn.setOnClickListener {
            val reportsFragment = reports()
            val fragmentTransaction = fragmentManager!!.beginTransaction()
            fragmentTransaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
            editor.putString("CURRENT_TAG", HomeActivity.TAG_REPORTS)
            editor.commit()
            fragmentTransaction.replace(R.id.frame, reportsFragment, HomeActivity.TAG_REPORTS)
            fragmentTransaction.commitAllowingStateLoss()
        }

        submit.setOnClickListener {
            if (SD.text.toString().matches("".toRegex()) || ED.text.toString().matches("".toRegex())) {
                val no_values = ArrayList<String>()
                no_values.add("")
                no_values.add("")
                if (SD.text.toString().matches("".toRegex())) {
                    no_values.set(0, getString(if (pref.getString("language", "0") == "0") R.string.start_date else R.string.start_date_esp))
                }
                if (ED.text.toString().matches("".toRegex())) {
                    no_values.set(1, getString(if (pref.getString("language", "0") == "0") R.string.end_date else R.string.end_date_esp))
                }
                val Alert = AlertDialog.Builder(context).create()
                Alert.setMessage(Html.fromHtml(getContext()!!.resources.getString(if (pref.getString("language", "0") == "0") R.string.please_fill_values else R.string.please_fill_values_esp) + ": <br>" + no_values.get(0).toString() + "<br>" + no_values.get(1).toString()))
                Alert.setButton(AlertDialog.BUTTON_NEUTRAL, activity!!.resources.getString(if (pref.getString("language", "0") == "0") R.string.ok else R.string.ok_esp)
                ) { dialog, which -> dialog.dismiss() }
                Alert.show()
            } else {
                // val sD = Date(SD.text.toString())
                // val eD = Date(ED.text.toString())
                // if (eD.time >= sD.time) {
                    editor.putString("driver", driver_str)
                    editor.commit()
                    val mileageFragment = mileage()
                    val fragmentTransaction = fragmentManager!!.beginTransaction()
                    fragmentTransaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                    editor.putString("CURRENT_TAG", HomeActivity.TAG_MILEAGE)
                    editor.commit()
                    fragmentTransaction.replace(R.id.frame, mileageFragment, HomeActivity.TAG_MILEAGE)
                    fragmentTransaction.commitAllowingStateLoss()
                /* } else {
                    val DateAlert = AlertDialog.Builder(context).create()
                    DateAlert.setMessage(activity!!.resources.getString(if (pref.getString("language", "0") == "0") R.string.end_date_greater_than_start_date else R.string.end_date_greater_than_start_date_esp))
                    DateAlert.setButton(AlertDialog.BUTTON_NEUTRAL, activity!!.resources.getString(if (pref.getString("language", "0") == "0") R.string.ok else R.string.ok_esp)
                    ) { dialog, which -> dialog.dismiss() }
                    DateAlert.show()
                } */
            }
        }

        return rootView
    }

    companion object {
        // Mileage Date-range activity
        private val TAG = "Mileage1-dateRange"

        var mileage_title: TextView? = null
        var start_date_title: TextView? = null
        var end_date_title: TextView? = null
        var driver_title: TextView? = null
        lateinit var submit: Button
    }

}// Required empty public constructor
