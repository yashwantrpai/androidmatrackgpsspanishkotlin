package com.matrackinc.matrackgpsspanish.fragment

import android.app.AlertDialog
import android.content.DialogInterface
import android.content.SharedPreferences
import android.graphics.Bitmap
import android.net.http.SslError
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.ClientCertRequest
import android.webkit.CookieManager
import android.webkit.SslErrorHandler
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.ProgressBar

import com.matrackinc.matrackgpsspanish.R

import android.content.Context.MODE_PRIVATE

/**
 * A simple [Fragment] subclass.
 */
class RoadsideAssistance : Fragment() {

    lateinit var wv1: WebView
    lateinit var pref: SharedPreferences
    var editor: SharedPreferences.Editor? = null
    lateinit var progress: ProgressBar

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.roadsideassistance, container, false)
        wv1 = rootView.findViewById(R.id.webView)
        pref = context!!.getSharedPreferences("DataStore", MODE_PRIVATE)

        if (android.os.Build.VERSION.SDK_INT >= 21) {
            CookieManager.getInstance().setAcceptThirdPartyCookies(wv1, true)
        } else {
            CookieManager.getInstance().setAcceptCookie(true)
        }

        // Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://107.170.196.31//gpstracking/matrackApp/help.html"));
        // startActivity(browserIntent);

        // String currentServer = pref.getString("currentServer", "");
        // String urls[] = getResources().getStringArray(R.array.url);
        // if(Util.isValidUrl(currentServer, urls)) {
        val url = "https://www.blinkroadside.com/app.html#/disablement/confirm-location/"
        // String url = currentServer + Serverfilenames.roadsideAssistanceUrl;
        // Log.d("Roadside Assistance", url.toString());
        try {
            wv1 = rootView.findViewById<View>(R.id.webView) as WebView
            wv1.webViewClient = MyBrowser()
            progress = rootView.findViewById<View>(R.id.progressBar) as ProgressBar
            wv1.settings.loadWithOverviewMode = true
            wv1.settings.useWideViewPort = true
            wv1.settings.loadsImagesAutomatically = true
            wv1.settings.javaScriptEnabled = true
            wv1.scrollBarStyle = View.SCROLLBARS_INSIDE_OVERLAY
            wv1.loadUrl(url)
        } catch (e: Exception) {
            e.printStackTrace()
        }

        // } else {
        //  Toast.makeText(getContext(), "Server connection issue..", Toast.LENGTH_SHORT).show();
        //  Intent i = new Intent(getContext(), SplashScreen.class);
        //  i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        //  startActivity(i);
        // }

        return rootView
    }

    private inner class MyBrowser : WebViewClient() {
        override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
            view.loadUrl(url)
            return true
        }

        override fun onPageFinished(view: WebView, url: String) {
            progress.visibility = View.GONE
            super.onPageFinished(view, url)
        }

        override fun onPageStarted(view: WebView, url: String, favicon: Bitmap) {
            progress.visibility = View.VISIBLE
            super.onPageStarted(view, url, favicon)
        }

        override fun onReceivedSslError(view: WebView, handler: SslErrorHandler, error: SslError) {
            val builder = AlertDialog.Builder(context)
            builder.setTitle(if (pref.getString("language", "0") == "0") R.string.security_certificate_title else R.string.security_certificate_title_esp)
            builder.setMessage(if (pref.getString("language", "0") == "0") R.string.security_certificate_message else R.string.security_certificate_message_esp)
            builder.setPositiveButton("Continue") { dialog, which -> handler.proceed() }
            builder.setNegativeButton("Cancel") { dialog, which -> handler.cancel() }
            val dialog = builder.create()
            dialog.show()
        }

        override fun onReceivedClientCertRequest(view: WebView, request: ClientCertRequest) {
            super.onReceivedClientCertRequest(view, request)
        }
    }
}// Required empty public constructor
