package com.matrackinc.matrackgpsspanish.fragment

import android.app.AlertDialog
import android.content.Context
import android.content.Context.MODE_PRIVATE
import android.content.DialogInterface
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Canvas
import android.graphics.Typeface
import android.location.Geocoder
import android.net.ConnectivityManager
import android.os.AsyncTask
import android.os.Bundle
import android.support.annotation.DrawableRes
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.style.StyleSpan
import android.util.Log
import android.util.Pair
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.*
import com.google.maps.android.clustering.Cluster
import com.google.maps.android.clustering.ClusterManager
import com.google.maps.android.clustering.view.DefaultClusterRenderer
import com.google.maps.android.ui.IconGenerator
import com.matrackinc.matrackgpsspanish.CreatePostString
import com.matrackinc.matrackgpsspanish.CustomHttpClient
import com.matrackinc.matrackgpsspanish.R
import com.matrackinc.matrackgpsspanish.activity.HomeActivity
import com.matrackinc.matrackgpsspanish.activity.SplashScreen
import com.matrackinc.matrackgpsspanish.db.DatabaseHelper
import com.matrackinc.matrackgpsspanish.util.ConvertUtil
import com.matrackinc.matrackgpsspanish.util.DateUtil
import com.matrackinc.matrackgpsspanish.util.MyItem
import com.matrackinc.matrackgpsspanish.util.Util
import com.matrackinc.matrackgpsspanish.util.Util.getDatapoint
import org.json.JSONArray
import org.json.JSONObject
import java.io.IOException
import java.util.*

/**
 * A simple [Fragment] subclass.
 */
class trackhistory_clustering : Fragment(), OnMapReadyCallback {

    internal var response: String? = ""
    internal var result_json: JSONArray? = JSONArray()
    internal lateinit var dbHelper: DatabaseHelper
    var authcode = ""
    var Address: String? = null
    var lat: Array<Double?>? = null
    var lng: Array<Double?>? = null
    lateinit var direction: Array<String?>
    lateinit var date: Array<String?>
    lateinit var time: Array<String?>
    lateinit var speed: Array<String?>
    lateinit var voltage: Array<String?>
    lateinit var event: Array<String?>
    lateinit var fuel: Array<String?>
    lateinit var mileage: Array<String?>
    lateinit var eventName: Array<String?>
    var address: Array<String>? = null
    internal var mapView: MapView? = null
    private var googleMap: GoogleMap? = null
    private var builder: LatLngBounds.Builder? = null
    private val bounds: LatLngBounds? = null
    lateinit var pref: SharedPreferences
    lateinit var editor: SharedPreferences.Editor
    private var mClusterManager: ClusterManager<MyItem>? = null
    internal var initialbounds: LatLngBounds? = null
    var current_ind = 0

    fun plotPoints(output: String) {
        response = output
        p_bar.visibility = View.VISIBLE
        var no_data: Boolean? = false
        var data_issue: Boolean? = false
        if (response != null && response != "") {
            try {
                val resp_json = JSONArray(response)
                result_json = resp_json
                //resp_json.getString(1).replace('/',' ');
                // Log.d(TAG, String.valueOf(resp_json));
            } catch (e: Exception) {
                e.printStackTrace()
            }

            var line: String? = ""
            if (result_json != null) {
                if (result_json!!.length() > 1) {
                    lat = arrayOfNulls(result_json!!.length() - 1)
                    lng = arrayOfNulls(result_json!!.length() - 1)
                    date = arrayOfNulls(result_json!!.length() - 1)
                    time = arrayOfNulls(result_json!!.length() - 1)
                    speed = arrayOfNulls(result_json!!.length() - 1)
                    direction = arrayOfNulls(result_json!!.length() - 1)
                    voltage = arrayOfNulls(result_json!!.length() - 1)
                    event = arrayOfNulls(result_json!!.length() - 1)
                    fuel = arrayOfNulls(result_json!!.length() - 1)
                    mileage = arrayOfNulls(result_json!!.length() - 1)
                    eventName = arrayOfNulls(result_json!!.length() - 1)

                    for (i in 0 until result_json!!.length()) {
                        try {
                            line = result_json!!.getString(i).replace('/', ' ')
                        } catch (e: Exception) {
                            e.printStackTrace()
                            data_issue = true
                        }

                        if (line != null) {
                            try {
                                val jsonobje = JSONObject(line)
                                if (jsonobje.length() > 0) {
                                    var date_ = jsonobje.getString("date").trim { it <= ' ' }
                                    date_ = date_.replace(' ', '/')
                                    val lat_ = jsonobje.getString("lat")
                                    val lng_ = jsonobje.getString("lng")
                                    val time_ = jsonobje.getString("time")
                                    val speed_ = jsonobje.getString("speed")
                                    val direction_ = jsonobje.getString("directionString")
                                    val voltage_ = jsonobje.getString("battery")
                                    val event_ = jsonobje.getString("event")
                                    val fuel_ = jsonobje.getString("fuel")
                                    val mileage_ = jsonobje.getString("mileage")
                                    val eventName_ = jsonobje.getString("eventName")
                                    lat!![i - 1] = ConvertUtil.getDouble(lat_, "0.0")
                                    lng!![i - 1] = ConvertUtil.getDouble(lng_, "0.0")
                                    date[i - 1] = date_
                                    time[i - 1] = time_
                                    speed[i - 1] = speed_
                                    direction[i - 1] = direction_
//                                    if (direction[i-1] != null) {
//                                        var dir = direction[current_ind]
//                                        if (pref.getString("langauge", "0").equals("1")) {
//                                            if (dir.equals("W"))
//                                                direction[i-1] = "O"
//                                            else if (dir.equals("NW"))
//                                                direction[i-1] = "NO"
//                                            else if (dir.equals("SW"))
//                                                direction[i-1] = "SO"
//                                        }
//                                    } else {
//                                        Log.w(TAG, "Direction value null for index$current_ind")
//                                    }
                                    voltage[i - 1] = voltage_
                                    event[i - 1] = event_
                                    fuel[i - 1] = fuel_
                                    mileage[i - 1] = mileage_
                                    eventName[i - 1] = eventName_
                                    current_ind = i
                                }
                            } catch (ex: Exception) {
                                ex.printStackTrace()
                                data_issue = true
                            }

                        } else {
                            // Log.w(TAG, "Server data issue: " + line );
                            Toast.makeText(getContext(), "Server data issue", Toast.LENGTH_SHORT).show()
                            data_issue = true
                        }
                    }
                    loadMap()
                } else {
                    no_data = true
                }
            } else {
                Log.w(TAG, "result_json is null")
                no_data = true
            }
            // Log.d("-------------------","plotpoints");
        } else {
            no_data = true
        }

        if (no_data!!) {
            if (activity != null) {
                if (!activity!!.isFinishing) {
                    val NoDataAlert = AlertDialog.Builder(activity).create()
                    NoDataAlert.setMessage(getString(if (pref.getString("language", "0") == "0") R.string.no_data_available else R.string.no_data_available_esp))
                    NoDataAlert.setButton(AlertDialog.BUTTON_POSITIVE, activity!!.resources.getString(if (pref.getString("language", "0") == "0") R.string.ok else R.string.ok_esp)
                    ) { dialog, which -> dialog.dismiss() }
                    NoDataAlert.show()
                }
            }
        } else if (data_issue!!) {
            if (activity == null) {
                if (!activity!!.isFinishing) {
                    val alertDialogBuilder = AlertDialog.Builder(activity)
                    val alertDialog = alertDialogBuilder.create()
                    alertDialogBuilder.setTitle(if (pref.getString("language", "0") == "0") R.string.data_sync_issue else R.string.data_sync_issue_esp)
                    alertDialogBuilder.setMessage(if (pref.getString("language", "0") == "0") R.string.re_login_and_try_again else R.string.re_login_and_try_again_esp)
                    alertDialogBuilder.setNeutralButton(activity!!.resources.getString(if (pref.getString("language", "0") == "0") R.string.ok else R.string.ok_esp)
                    ) { dialog, which -> dialog.dismiss() }
                    alertDialogBuilder.show()
                }
            }
        }
        p_bar.visibility = View.GONE
    }

    fun getSpannedText(str: String, startInd: Int, endInd: Int): SpannableStringBuilder {
        val sb = SpannableStringBuilder(str)

        val bss = StyleSpan(Typeface.BOLD)
        sb.setSpan(bss, startInd, endInd, Spannable.SPAN_INCLUSIVE_INCLUSIVE)

        return sb
    }

    fun loadMap() {
        try {
            p_bar.visibility = View.VISIBLE
            // activity.getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            if (mapView != null) {
                try {
                    mapView!!.onResume() // needed to get the map to display immediately
                } catch (e: Exception) {
                    e.printStackTrace()
                }

                try {
                    MapsInitializer.initialize(activity!!.applicationContext)
                } catch (e: Exception) {
                    e.printStackTrace()
                }

                builder = LatLngBounds.Builder()

                try {
                    mapView!!.getMapAsync { mMap ->
                        googleMap = mMap
                        googleMap!!.uiSettings.isRotateGesturesEnabled = false
                        googleMap!!.isIndoorEnabled = false

                        /* try {
                                // Customise the styling of the base map using a JSON object defined
                                // in a raw resource file.
                                boolean success = googleMap.setMapStyle(
                                        MapStyleOptions.loadRawResourceStyle(
                                                getContext(), R.raw.map_json));

                                if (!success) {
                                    Log.e(TAG, "Style parsing failed.");
                                }
                            } catch (Resources.NotFoundException e) {
                                Log.e(TAG, "Can't find style. Error: ", e);
                            } */

                        if (pref.getString("mapSelect", "") != null) {
                            when (pref.getString("mapSelect", "")) {

                                "Normal" -> {
                                    googleMap!!.mapType = GoogleMap.MAP_TYPE_NORMAL
                                    rb_normal.isSelected = true
                                    rb_satellite.isSelected = false
                                    rb_hybrid.isSelected = false
                                    editor.putString("mapSelect", "Normal")
                                    editor.commit()
                                }
                                "Satellite" -> {
                                    googleMap!!.mapType = GoogleMap.MAP_TYPE_SATELLITE
                                    rb_satellite.isSelected = true
                                    rb_normal.isSelected = false
                                    rb_hybrid.isSelected = false
                                    editor.putString("mapSelect", "Satellite")
                                    editor.commit()
                                }
                                "Hybrid" -> {
                                    googleMap!!.mapType = GoogleMap.MAP_TYPE_HYBRID
                                    rb_hybrid.isSelected = true
                                    rb_normal.isSelected = false
                                    rb_satellite.isSelected = false
                                    editor.putString("mapSelect", "Hybrid")
                                    editor.commit()
                                }
                                else -> {
                                    googleMap!!.mapType = GoogleMap.MAP_TYPE_NORMAL
                                    rb_normal.isSelected = true
                                    rb_satellite.isSelected = false
                                    rb_hybrid.isSelected = false
                                    editor.putString("mapSelect", "Normal")
                                    editor.commit()
                                }
                            }
                        } else {
                            googleMap!!.mapType = GoogleMap.MAP_TYPE_NORMAL
                            rb_normal.isSelected = true
                            rb_satellite.isSelected = false
                            rb_hybrid.isSelected = false
                            editor.putString("mapSelect", "Normal")
                            editor.commit()
                        }

                        if (HomeActivity.equalStrings(pref.getString("curfile", "")!!.toString(), "trackhistory")) {
                            if (HomeActivity.equalStrings(pref.getString("mapSelect", "")!!.toString(), "Normal")) {
                                rb_normal.isSelected = true
                            } else if (HomeActivity.equalStrings(pref.getString("mapSelect", "")!!.toString(), "Satellite")) {
                                rb_satellite.isSelected = true
                            } else if (HomeActivity.equalStrings(pref.getString("mapSelect", "")!!.toString(), "Hybrid")) {
                                rb_hybrid.isSelected = true
                            } else {
                                rb_normal.isSelected = true
                            }
                        } else {
                            rb_normal.isSelected = true
                        }

                        rb_normal.setOnTouchListener { v, event ->
                            googleMap!!.mapType = GoogleMap.MAP_TYPE_NORMAL
                            rb_normal.isSelected = true
                            rb_satellite.isSelected = false
                            rb_hybrid.isSelected = false

                            editor.putString("mapSelect", "Normal")
                            editor.commit()
                            true
                        }

                        rb_satellite.setOnTouchListener { v, event ->
                            googleMap!!.mapType = GoogleMap.MAP_TYPE_SATELLITE
                            rb_satellite.isSelected = true
                            rb_normal.isSelected = false
                            rb_hybrid.isSelected = false

                            editor.putString("mapSelect", "Satellite")
                            editor.commit()
                            true
                        }

                        rb_hybrid.setOnTouchListener { v, event ->
                            googleMap!!.mapType = GoogleMap.MAP_TYPE_HYBRID
                            rb_hybrid.isSelected = true
                            rb_normal.isSelected = false
                            rb_satellite.isSelected = false

                            editor.putString("mapSelect", "Hybrid")
                            editor.commit()
                            true
                        }

                        mClusterManager = ClusterManager(getContext()!!, googleMap)
                        mRenderer = ItemRenderer()
                        mClusterManager!!.renderer = mRenderer

                        googleMap!!.setOnCameraIdleListener(mClusterManager)

                        googleMap!!.setInfoWindowAdapter(mClusterManager!!.markerManager)
                        mClusterManager!!.markerCollection.setOnInfoWindowAdapter(CustomInfoWindowAdapter())
                        googleMap!!.setOnMarkerClickListener(mClusterManager)
                        googleMap!!.setOnInfoWindowClickListener(mClusterManager)
                        googleMap!!.setOnMapClickListener {
                            val marker = mRenderer!!.getMarker(clickedMarker)
                            if (marker != null && marker.isInfoWindowShown) {
                                marker.hideInfoWindow()
                                // Animate camera to the bounds
                                if (initialbounds != null) {
                                    try {
                                        googleMap!!.moveCamera(CameraUpdateFactory.newLatLngBounds(initialbounds, 100))
                                    } catch (e: Exception) {
                                        e.printStackTrace()
                                    }

                                    initialbounds = null
                                }
                            }
                        }

                        mClusterManager!!.setOnClusterClickListener { cluster ->
                            // Show a toast with some info when the cluster is clicked.
                            Toast.makeText(getContext(), cluster.size.toString() + " points", Toast.LENGTH_SHORT).show()

                            val BoundCalculateTask = BoundsCalculate()
                            BoundCalculateTask.execute(cluster)

                            true
                        }

                        mClusterManager!!.setOnClusterItemClickListener { myItem ->
                            if (activity != null) {
                                if (!activity!!.isFinishing) {
                                    val connectivityManager = activity!!.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
                                    val activeNetworkInfo = connectivityManager.activeNetworkInfo
                                    if (activeNetworkInfo != null && activeNetworkInfo.isConnected) {
                                        val clusteritemclickboundsTask = clusteritemclickbounds()
                                        clusteritemclickboundsTask.execute(myItem)
                                    } else {
                                        Toast.makeText(getContext(), if (pref.getString("language", "0") == "0") R.string.internet_connectivity_check else R.string.internet_connectivity_check_esp, Toast.LENGTH_SHORT).show()
                                    }
                                }
                            }

                            true
                        }

                        mClusterManager!!.setOnClusterItemInfoWindowClickListener {
                            val marker = mRenderer!!.getMarker(clickedMarker)
                            if (marker != null && marker.isInfoWindowShown) {
                                marker.hideInfoWindow()
                                // Animate camera to the bounds
                                /* if(initialbounds != null){
                                            try {
                                                googleMap.moveCamera(CameraUpdateFactory.newLatLngBounds(initialbounds, 100));
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                            initialbounds = null;
                                        } */
                            }
                        }

                        try {
                            val clusterTask = Clustering()
                            clusterTask.execute()
                            // addItems();
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }

                        // Draw polyline through async thread
                        val DrawPolylineTask = DrawPolyline()
                        DrawPolylineTask.execute()
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }

            } else {
                Log.w(TAG, "MapView is null")
            }
        } catch (e: Exception) {
            // Log.d("------------------",e.getMessage());
            e.printStackTrace()
        }

        p_bar.visibility = View.GONE
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        rootView = inflater.inflate(R.layout.trackhistory, container, false)
        p_bar = rootView.findViewById<View>(R.id.text_p_bar) as TextView
        google_maps_api = resources.getString(R.string.googlemaps_api_key)
        pref = getContext()!!.getSharedPreferences("DataStore", MODE_PRIVATE)
        editor = pref.edit()

        dbHelper = DatabaseHelper()
        authcode = dbHelper.authcode

        trackhistory_title = rootView.findViewById<View>(R.id.trackhistory_title) as TextView
        trackhistory_title.setText(if (pref.getString("language", "0") == "0") R.string.trackhistory else R.string.trackhistory_esp)

        isDestroyed = false

        if (pref.getString("curfile", "") == null || !HomeActivity.equalStrings(pref.getString("curfile", "")!!.toString(), "trackhistory")) {
            editor.putString("curfile", "trackhistory")
            editor.commit()
        }

        zoomminus = rootView.findViewById<View>(R.id.zoomminus) as ImageView
        zoomplus = rootView.findViewById<View>(R.id.zoomplus) as ImageView

        progressLayout = activity!!.findViewById<View>(R.id.progressLayout) as LinearLayout
        progressBar = activity!!.findViewById<View>(R.id.progressbar) as ProgressBar
        progressText = activity!!.findViewById<View>(R.id.progressText) as TextView
        back_btn = rootView.findViewById<View>(R.id.back) as ImageView

        start_date = pref.getString("startdate", "")
        // end_date = String.valueOf(trackhistoryform.values.get(1));
        end_date = pref.getString("enddate", "")
        // data_point = String.valueOf(trackhistoryform.values.get(2));
        data_point = pref.getString("datapoint", "")
        // driver = String.valueOf(trackhistoryform.values.get(3));
        driver = pref.getString("driver", "")
        client = dbHelper.client

        // Back button
        back_btn.setOnClickListener {
            // closeInfowindow();
            if (isInfoWindowOpen) {
                Toast.makeText(getContext(), if (pref.getString("language", "0") == "0") R.string.close_infowindow_before_navigating else R.string.close_infowindow_before_navigating_esp, Toast.LENGTH_LONG).show()
            } else {
                val reportsFragment = trackhistoryform()
                val fragmentTransaction = fragmentManager!!.beginTransaction()
                fragmentTransaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                editor.putString("CURRENT_TAG", HomeActivity.TAG_TRACKHISTORY_FORM)
                editor.commit()
                fragmentTransaction.replace(R.id.frame, reportsFragment, HomeActivity.TAG_TRACKHISTORY_FORM)
                fragmentTransaction.commitAllowingStateLoss()
            }
        }

        rb_normal = rootView.findViewById<View>(R.id.rb_normal) as Button
        rb_normal.setText(if (pref.getString("language", "0") == "0") R.string.standard_view else R.string.standard_view_esp)
        rb_satellite = rootView.findViewById<View>(R.id.rb_satellite) as Button
        rb_satellite.setText(if (pref.getString("language", "0") == "0") R.string.satellite_view else R.string.satellite_view_esp)
        rb_hybrid = rootView.findViewById<View>(R.id.rb_hybrid) as Button
        rb_hybrid.setText(if (pref.getString("language", "0") == "0") R.string.hybrid_view else R.string.hybrid_view_esp)

        if (data_point == "STOPS ONLY") {
            data_point = data_point!!.replace(" ", "")
        } else if (data_point == "ENGINE ON/OFF") {
            data_point = "IGNITION"
        } else if (data_point == "MOVEMENTS ONLY") {
            data_point = "MOVEMENT"
        }

        if (activity != null) {
            if (!activity!!.isFinishing) {
                val connectivityManager = activity!!.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
                val activeNetworkInfo = connectivityManager.activeNetworkInfo
                if (activeNetworkInfo != null && activeNetworkInfo.isConnected) {
                    try {
                        getTrackhistoryPoints()
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                } else {
                    Toast.makeText(getContext(), "Unable to load points. Please check your internet connectivity..", Toast.LENGTH_SHORT).show()
                }
            }
        }

        return rootView
    }

    fun getTrackhistoryPoints() {
        val currentServer = pref.getString("currentServer", "")
        val urls = resources.getStringArray(R.array.url)
        if (Util.isValidUrl(currentServer, urls)) {
            val trackhistoryUrl = currentServer!! + getString(if (pref.getString("language", "0") == "0") R.string.trackhistory_url else R.string.trackhistory_url_esp)

            val datapointStr = if (pref.getString("language", "0") == "1") getDatapoint(data_point) else data_point

            val params = ArrayList<Pair<String, String>>()
            var pair = Pair("operation", "trackhistory")
            params.add(pair)
            pair = Pair("driver", driver!!.replace(" ", ""))
            params.add(pair)
            pair = Pair("startdate", if (pref.getString("language", "0") == "1") DateUtil.toSpanishDate(start_date) else start_date)
            params.add(pair)
            pair = Pair("enddate", if (pref.getString("language", "0") == "1") DateUtil.toSpanishDate(end_date) else end_date)
            params.add(pair)
            pair = Pair("datapoint", datapointStr)
            params.add(pair)
            pair = Pair("authcode", authcode)
            params.add(pair)

            val postString = CreatePostString.createPostString(params)
            // Log.i("url", trackhistoryUrl);
            // Log.i("param", postString);
            val dataPointsTask = getDataPoints()
            dataPointsTask.execute(trackhistoryUrl, postString)
        } else {
            Toast.makeText(getContext(), if (pref.getString("language", "0") == "0") R.string.server_connection_issue else R.string.server_connection_issue_esp, Toast.LENGTH_SHORT).show()
            val i = Intent(activity, SplashScreen::class.java)
            i.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(i)
        }
    }

    private inner class ItemRenderer : DefaultClusterRenderer<MyItem>(context, googleMap, mClusterManager) {
        private val mIconGenerator = IconGenerator(context)
        private val mImageView: ImageView
        private val mDimension: Int

        init {

            mImageView = ImageView(context)
            mDimension = resources.getDimension(R.dimen.custom_profile_image).toInt()
            mImageView.layoutParams = ViewGroup.LayoutParams(mDimension, mDimension)
            val padding = resources.getDimension(R.dimen.custom_profile_padding).toInt()
            // mImageView.setPadding(padding, padding, padding, padding);
            mIconGenerator.setContentView(mImageView)
        }

        override fun onBeforeClusterItemRendered(item: MyItem?, markerOptions: MarkerOptions?) {
            super.onBeforeClusterItemRendered(item, markerOptions)
            mImageView.setImageResource(R.drawable.trackicon)
            val icon = mIconGenerator.makeIcon()
            /* if(item.getIndex() == 0){
                try {
                    markerOptions.icon(BitmapDescriptorFactory.fromBitmap(resizeMapIcons("start", 90, 90)));
                } catch (Exception e){
                    e.printStackTrace();
                }

            }else if(item.getIndex() == lat.length - 1){
                try {
                    markerOptions.icon(BitmapDescriptorFactory.fromBitmap(resizeMapIcons("finish", 90, 90)));
                } catch (Exception e){
                    e.printStackTrace();
                }
            } */
            /*try {
             if(resizeMapIcons("blueicon", 90, 90) != null) {
             markerOptions.icon(BitmapDescriptorFactory.fromBitmap(resizeMapIcons("blueicon", 90, 90)));
             }
             } catch(Exception e){
             e.printStackTrace();
             } */
            try {
                markerOptions!!.icon(resizeMapIcons("trackicon", 36, 36))
                        .rotation(direction[item!!.index]!!.toFloat())
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }

        /* @Override
        protected void onBeforeClusterRendered(Cluster<Person> cluster, MarkerOptions markerOptions) {
            // Draw multiple people.
            // Note: this method runs on the UI thread. Don't spend too much time in here (like in this example).
            List<Drawable> profilePhotos = new ArrayList<Drawable>(Math.min(4, cluster.getSize()));
            int width = mDimension;
            int height = mDimension;

            for (Person p : cluster.getItems()) {
                // Draw 4 at most.
                if (profilePhotos.size() == 4) break;
                Drawable drawable = getResources().getDrawable(p.profilePhoto);
                drawable.setBounds(0, 0, width, height);
                profilePhotos.add(drawable);
            }
            MultiDrawable multiDrawable = new MultiDrawable(profilePhotos);
            multiDrawable.setBounds(0, 0, width, height);

            //mClusterImageView.setImageDrawable(multiDrawable);
            //Bitmap icon = mClusterIconGenerator.makeIcon(String.valueOf(cluster.getSize()));
            //markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon));
        } */

        protected override fun shouldRenderAsCluster(cluster: Cluster<MyItem>): Boolean {
            // Always render clusters.
            return cluster.size > 50
        }
    }

    inner class CustomInfoWindowAdapter : GoogleMap.InfoWindowAdapter {
        // Use default InfoWindow frame
        // Getting view from the layout file info_window_layout
        internal var view = activity!!.layoutInflater.inflate(R.layout.trackhistory_windowlayout, null)

        override fun getInfoWindow(arg0: Marker): View? {
            return null
        }

        override fun getInfoContents(marker: Marker): View {
            // Getting the position from the marker
            val latLng = marker.position

            view = activity!!.layoutInflater.inflate(R.layout.trackhistory_windowlayout, null)
            //TextView tvLat = (TextView) view.findViewById(R.id.tv_lat);
            //TextView tvLng = (TextView) view.findViewById(R.id.tv_lng);
            val tvDate = view.findViewById<View>(R.id.tv_date) as TextView
            // TextView tvTime = (TextView) view.findViewById(R.id.tv_time);
            val tvSpeed = view.findViewById<View>(R.id.tv_speed) as TextView
            val tvVoltage = view.findViewById<View>(R.id.tv_voltage) as TextView
            val tvAddress = view.findViewById<View>(R.id.tv_address) as TextView
            val tvEvent = view.findViewById<View>(R.id.tv_event) as TextView
            val tvDirection = view.findViewById<View>(R.id.tv_direction) as TextView
            val tvMileage = view.findViewById<View>(R.id.tv_mileage) as TextView
            val tvFuel = view.findViewById<View>(R.id.tv_fuel) as TextView
            val tvClose = view.findViewById<View>(R.id.tv_close) as ImageView
            // mLastShownInfoWindowMarker = marker;

            if (clickedMarker != null) {
                if (clickedMarker!!.voltage != null) {
                    val voltageString = SpannableStringBuilder((if (pref.getString("language", "0") == "0") "Voltage: " else "Voltaje: ") + clickedMarker!!.voltage + "%")
                    voltageString.setSpan(android.text.style.StyleSpan(android.graphics.Typeface.BOLD), 0, 8, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
                    tvVoltage.text = voltageString
                }

                if (Address != null) {
                    val addressString = SpannableStringBuilder((if (pref.getString("language", "0") == "0") "Address: " else "Localización: ") + Address!!)
                    addressString.setSpan(android.text.style.StyleSpan(android.graphics.Typeface.BOLD), 0, if (pref.getString("language", "0") == "0") 8 else 13, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
                    tvAddress.text = addressString
                }

                if (clickedMarker!!.date != null) {
                    tvDate.text = clickedMarker!!.date!!.replace("\\s+".toRegex(), "/") + "   " + DateUtil.covertToCivilianTime(clickedMarker!!.time!!)
                }

                /* if(clickedMarker.getTime() != null) {
                    // Log.d("time",clickedMarker.getTime());
                    tvTime.setText(Html.fromHtml( "Time: ") + DateUtil.covertToCivilianTime(clickedMarker.getTime()));
                } else {
                    // Log.w(TAG, "time is null at index "+ clickedMarker.getIndex()+" is "+clickedMarker.getTime());
                } */

                if (clickedMarker!!.speed != null) {
                    val speedString = SpannableStringBuilder((if (pref.getString("language", "0") == "0") "Speed: " else "Velocidad: ") + clickedMarker!!.speed + (if (pref.getString("language", "0") == "0") "m/h" else "km/h"))
                    speedString.setSpan(android.text.style.StyleSpan(android.graphics.Typeface.BOLD), 0, if (pref.getString("language", "0") == "0") 6 else 9, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
                    tvSpeed.text = speedString
                }

                if (clickedMarker!!.eventName != null) {
                    val eventString = SpannableStringBuilder((if (pref.getString("language", "0") == "0") "Event: " else "Evento: ") + clickedMarker!!.eventName)
                    eventString.setSpan(android.text.style.StyleSpan(android.graphics.Typeface.BOLD), 0, if (pref.getString("language", "0") == "0") 6 else 7, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
                    tvEvent.text = eventString
                }

                // if(clickedMarker.getDirection() > 0) {
                val directionString = SpannableStringBuilder((if (pref.getString("language", "0") == "0") "Direction: " else "Dirección: ") + clickedMarker!!.direction.toString())
                directionString.setSpan(android.text.style.StyleSpan(android.graphics.Typeface.BOLD), 0, 10, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
                tvDirection.text = directionString
                // }

                if (clickedMarker!!.mileage != null) {
                    val mileageString = SpannableStringBuilder((if (pref.getString("language", "0") == "0") "Mileage: " else "Kilometraje: ") + clickedMarker!!.mileage + if (pref.getString("language", "0") == "0") " miles" else " km")
                    mileageString.setSpan(android.text.style.StyleSpan(android.graphics.Typeface.BOLD), 0, if (pref.getString("language", "0") == "0") 8 else 11, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
                    tvMileage.text = mileageString
                }

                if (clickedMarker!!.fuel != null) {
                    val fuelString = SpannableStringBuilder((if (pref.getString("language", "0") == "0") "Fuel: " else "Combustible: ") + clickedMarker!!.fuel + (if (pref.getString("language", "0") == "0") " litres" else " litros"))
                    fuelString.setSpan(android.text.style.StyleSpan(android.graphics.Typeface.BOLD), 0, if (pref.getString("language", "0") == "0") 5 else 11, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
                    tvFuel.text = fuelString
                }
                tvClose.setOnClickListener { mRenderer!!.getMarker(clickedMarker).hideInfoWindow() }
            }
            // Returning the view containing InfoWindow contents
            return view
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mapView = rootView.findViewById<View>(R.id.mapView) as com.google.android.gms.maps.MapView
        try {
            if (mapView != null) {
                mapView!!.onCreate(savedInstanceState)
            }
            mapView!!.onResume() // needed for getting the map to display immediately
        } catch (e: Exception) {
            e.printStackTrace()
        }

        isDestroyed = false
        try {
            if (mapView != null) {
                mapView!!.onCreate(savedInstanceState)
            } else {
                Log.w(TAG, "MapView is null")
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

        zoomplus.setOnClickListener {
            if (googleMap != null) {
                val newzoom = googleMap!!.cameraPosition.zoom + 1
                val centerLatLng = googleMap!!.cameraPosition.target
                val latitude = centerLatLng.latitude
                val longitude = centerLatLng.longitude
                googleMap!!.animateCamera(CameraUpdateFactory.newLatLngZoom(LatLng(latitude, longitude), newzoom))
            }
        }

        zoomminus.setOnClickListener {
            if (googleMap != null) {
                val newzoom = googleMap!!.cameraPosition.zoom - 1
                val centerLatLng = googleMap!!.cameraPosition.target
                val latitude = centerLatLng.latitude
                val longitude = centerLatLng.longitude
                googleMap!!.animateCamera(CameraUpdateFactory.newLatLngZoom(LatLng(latitude, longitude), newzoom))
            }
        }
    }

    override fun onResume() {
        super.onResume()
        try {
            if (mapView != null) {
                mapView!!.onResume()
            } else {
                Log.w(TAG, "MapView is null")
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

        isDestroyed = false
    }

    override fun onPause() {
        super.onPause()
        if (progressLayout.visibility == View.VISIBLE) {
            progressText.text = ""
            progressLayout.visibility = View.GONE
            trackhistory_title.visibility = View.VISIBLE
            back_btn.visibility = View.VISIBLE
        }

        try {
            if (mapView != null) {
                mapView!!.onPause()
            } else {
                Log.w(TAG, "MapView is null")
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

        isDestroyed = false
        // logout();
    }

    override fun onDestroy() {

        super.onDestroy()
        if (progressLayout.visibility == View.VISIBLE) {
            progressText.text = ""
            progressLayout.visibility = View.GONE
            trackhistory_title.visibility = View.VISIBLE
            back_btn.visibility = View.VISIBLE
        }

        try {
            if (mapView != null) {
                mapView!!.onDestroy()
            } else {
                Log.w(TAG, "MapView is null")
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

        isDestroyed = true
        // logout();
    }

    override fun onStart() {
        super.onStart()
        isDestroyed = false

        try {
            if (mapView != null) {
                mapView!!.onStart()
            } else {
                Log.w(TAG, "MapView is null")
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    override fun onStop() {
        super.onStop()
        isDestroyed = false

        try {
            if (mapView != null) {
                mapView!!.onStop()
            } else {
                Log.w(TAG, "MapView is null")
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

        // logout();
    }

    override fun onLowMemory() {
        super.onLowMemory()

        try {
            if (mapView != null) {
                mapView!!.onLowMemory()
            } else {
                Log.w(TAG, "MapView is null")
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    override fun onMapReady(map: GoogleMap) {
        googleMap = map
        googleMap!!.uiSettings.isRotateGesturesEnabled = false
    }

    inner class getDataPoints : AsyncTask<String, Void, String>(), DialogInterface.OnCancelListener {

        override fun onPreExecute() {
            super.onPreExecute()
            if ((!isDestroyed!!)!!) {
                progressLayout.visibility = View.VISIBLE
                progressText.setText(if (pref.getString("language", "0") == "0") R.string.please_wait else R.string.please_wait_esp)
                trackhistory_title.visibility = View.GONE
                back_btn.visibility = View.GONE
            }
        }

        override fun doInBackground(url: Array<String>): String? {
            // TODO Auto-generated method stub
            var res: String? = null
            try {
                response = CustomHttpClient.executeHttpPost(url[0], url[1])
                res = response!!.toString().trim()
                //res= res.replaceAll("\\s+","");
            } catch (e: Exception) {
                e.printStackTrace()
            }

            return res
        }//close doInBackground

        override fun onPostExecute(result: String?) {
            var data_issue: Boolean? = false
            if (progressLayout.visibility == View.VISIBLE) {
                progressText.text = ""
                progressLayout.visibility = View.GONE
                trackhistory_title.visibility = View.VISIBLE
                back_btn.visibility = View.VISIBLE
            }

            var respjson = JSONObject()
            var data = JSONObject()
            if (result == null || result != "") {
                try {
                    respjson = JSONObject(result)
                    if (respjson.get("status") == "true") {
                        data = respjson.getJSONObject("data")
                        val trackhistorydata = data.getString("trackhistorydata")
                        if (trackhistorydata != null && trackhistorydata != "") {
                            plotPoints(trackhistorydata)
                        } else {
                            plotPoints("")
                        }
                    } else {
                        if (activity != null) {
                            if (!activity!!.isFinishing) {
                                val authcodeError = AlertDialog.Builder(activity).create()
                                authcodeError.setMessage(getString(if (pref.getString("language", "0") == "0") R.string.no_data_available else R.string.no_data_available_esp))
                                authcodeError.setButton(AlertDialog.BUTTON_NEUTRAL, activity!!.resources.getString(if (pref.getString("language", "0") == "0") R.string.ok else R.string.ok_esp)
                                ) { dialog, which ->
                                    val data = JSONObject()
                                    try {
                                        // boolean isInserted = dbHelper.setFlag(0, "", "", "", "", "");
                                        dialog.dismiss()
                                        // Intent i = new Intent(getActivity(), SplashScreen.class);
                                        // i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        // startActivity(i);
                                    } catch (e: Exception) {
                                        e.printStackTrace()
                                        dialog.dismiss()
                                    }
                                }
                                authcodeError.show()
                            }
                        }
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                    data_issue = true
                }

            } else {
                data_issue = true
            }

            if (data_issue!!) {
                if (activity != null) {
                    if (!activity!!.isFinishing) {
                        val alertDialogBuilder = AlertDialog.Builder(activity)
                        val alertDialog = alertDialogBuilder.create()
                        alertDialogBuilder.setTitle(if (pref.getString("language", "0") == "0") R.string.data_sync_issue else R.string.data_sync_issue_esp)
                        alertDialogBuilder.setMessage(if (pref.getString("language", "0") == "0") R.string.re_login_and_try_again else R.string.re_login_and_try_again_esp)
                        alertDialogBuilder.setNeutralButton(activity!!.resources.getString(if (pref.getString("language", "0") == "0") R.string.ok else R.string.ok_esp)
                        ) { dialog, which -> dialog.dismiss() }
                        alertDialogBuilder.show()
                    }
                }
            }
        }

        override fun onCancel(dialog: DialogInterface) {
            cancel(true)
        }
    }

    inner class BoundsCalculate : AsyncTask<Cluster<MyItem>, Void, String>(), DialogInterface.OnCancelListener {

        internal lateinit var bounds: LatLngBounds
        override fun onPreExecute() {
            super.onPreExecute()
            if (activity != null) {
                if (!activity!!.isFinishing) {
                    if ((!isDestroyed!!)!!) {
                        progressLayout.visibility = View.VISIBLE
                        progressText.setText(if (pref.getString("language", "0") == "0") R.string.please_wait else R.string.please_wait_esp)
                        trackhistory_title.visibility = View.GONE
                        back_btn.visibility = View.GONE
                    }
                }
            }
        }

        override fun doInBackground(clusters: Array<Cluster<MyItem>>): String? {
            val builder = LatLngBounds.builder()
            for (item in clusters[0].items) {
                builder.include(item.position)
            }
            // Get the LatLngBounds
            bounds = builder.build()
            return ""
        }


        override fun onPostExecute(result: String) {
            if (progressLayout.visibility == View.VISIBLE) {
                progressText.text = ""
                progressLayout.visibility = View.GONE
                trackhistory_title.visibility = View.VISIBLE
                back_btn.visibility = View.VISIBLE
            }

            try {
                googleMap!!.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 60))
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }

        override fun onCancel(dialog: DialogInterface) {
            cancel(true)
        }
    }

    inner class Clustering : AsyncTask<String, Void, String>(), DialogInterface.OnCancelListener {

        internal lateinit var bounds: LatLngBounds

        override fun onPreExecute() {
            super.onPreExecute()
            p_bar.visibility = View.VISIBLE
            /* if(getActivity() != null) {
                if (!getActivity().isFinishing()) {
                   if(!isDestroyed && android.os.Build.VERSION.SDK_INT > 19) {
                      pdia = new ProgressDialog(getActivity());
                      pdia.setMessage(pref.getString("language", "0").equals("0")?R.string.please_wait:R.string.please_wait_esp);
                      // Log.d("----------------","Please wait..");
                      pdia.setCancelable(false);
                      pdia.show();
                   }
                }
            } */
        }

        override fun doInBackground(vararg strings: String): String? {
            val builder = LatLngBounds.builder()
            try {
                for (i in lat!!.indices) {
                    if (lat!![i] != 0.0 && lng!![i] != 0.0) {
                        // if (i == 0 || i == (lat.length - 1)) {
                        builder.include(LatLng(lat!![i]!!, lng!![i]!!))
                        // }
                        val offsetItem = MyItem(LatLng(lat!![i]!!, lng!![i]!!), i, lat!![i]!!, lng!![i]!!, date[i], time[i], speed[i], voltage[i], event[i], direction[i], fuel[i], mileage[i], eventName[i])
                        mClusterManager!!.addItem(offsetItem)
                    } else {
                        // Log.d("0 coordinates", "Index: "+i+" " +lat[i]+" "+lng[i]);
                    }
                }
            } catch (ex: Exception) {
                ex.printStackTrace()
            }

            // for(int i = 0; i < ((lat.length < 4)?lat.length:4); i++){
            // builder.include(new LatLng(lat[lat.length - i - 1], lng[lng.length - i - 1]));
            // }

            // Get the LatLngBounds
            bounds = builder.build()
            return ""
        }

        override fun onPostExecute(result: String) {
            // Animate camera to the bounds
            p_bar.visibility = View.GONE
            try {
                googleMap!!.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 60))
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }

        override fun onCancel(dialog: DialogInterface) {
            cancel(true)
        }
    }

    inner class clusteritemclickbounds : AsyncTask<MyItem, Void, String>(), DialogInterface.OnCancelListener {

        internal lateinit var bounds: LatLngBounds

        override fun onPreExecute() {
            super.onPreExecute()
            p_bar.visibility = View.VISIBLE
            /* if(getActivity() != null) {
                if (!getActivity().isFinishing()) {
                   if(!isDestroyed && android.os.Build.VERSION.SDK_INT > 19) {
                      pdia = new ProgressDialog(getActivity());
                      pdia.setMessage(pref.getString("language", "0").equals("0")?R.string.please_wait:R.string.please_wait_esp);
                      // Log.d("----------------","Please wait..");
                      pdia.setCancelable(false);
                      pdia.show();
                   }
                }
            } */
        }

        override fun doInBackground(vararg myItems: MyItem): String? {
            // Does nothing, but you could go into the user's profile page, for example.
            val builder = LatLngBounds.builder()
            builder.include(myItems[0].position)
            clickedMarker = myItems[0]
            // Get the LatLngBounds
            bounds = builder.build()

            return ""
        }

        override fun onPostExecute(result: String) {
            // Animate camera to the bounds
            p_bar.visibility = View.GONE
            initialbounds = googleMap!!.projection.visibleRegion.latLngBounds
            // Animate camera to the bounds
            try {
                googleMap!!.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 100))
            } catch (e: Exception) {
                e.printStackTrace()
            }

            if (activity != null) {
                if (!activity!!.isFinishing) {
                    // final String url = "http://nominatim-load-1807280891.us-east-1.elb.amazonaws.com/reverse_geocode_tracking.php?lat=" + lat[clickedMarker.getIndex()] + "&lng=" + lng[clickedMarker.getIndex()];
                    val connectivityManager = activity!!.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
                    val activeNetworkInfo = connectivityManager.activeNetworkInfo
                    if (activeNetworkInfo != null && activeNetworkInfo.isConnected) {
                        Address = ""
                        try {
                            val url = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + java.lang.Double.toString(clickedMarker!!.latitude) + "," + java.lang.Double.toString(clickedMarker!!.longitude) + google_maps_api
                            val loc = getLocationAddress()
                            loc.execute(url)
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }

                    } else {
                        Toast.makeText(getContext(), if (pref.getString("language", "0") == "0") R.string.internet_connectivity_check else R.string.internet_connectivity_check_esp, Toast.LENGTH_SHORT).show()
                    }
                }
            }
        }

        override fun onCancel(dialog: DialogInterface) {
            cancel(true)
        }
    }

    inner class LatLngBuilder : AsyncTask<String, Void, String>() {
        override fun onPreExecute() {
            super.onPreExecute()
            if (activity != null) {
                if (!activity!!.isFinishing) {
                    if ((!isDestroyed!!)!!) {
                        progressLayout.visibility = View.VISIBLE
                        progressText.setText(if (pref.getString("language", "0") == "0") R.string.please_wait else R.string.please_wait_esp)
                        trackhistory_title.visibility = View.GONE
                        back_btn.visibility = View.GONE
                    }
                }
            }
        }

        override fun doInBackground(vararg strings: String): String? {
            return ""
        }

        override fun onPostExecute(result: String) {
            if (progressLayout.visibility == View.VISIBLE) {
                progressText.text = ""
                progressLayout.visibility = View.GONE
                trackhistory_title.visibility = View.VISIBLE
                back_btn.visibility = View.VISIBLE
            }
        }
    }

    inner class DrawPolyline : AsyncTask<String, Void, Any>() {

        override fun onPreExecute() {
            super.onPreExecute()
            p_bar.visibility = View.VISIBLE
        }

        override fun doInBackground(url: Array<String>): Any {
            // TODO Auto-generated method stub
            val polylineOptions = PolylineOptions()
                    .width(3f)
                    .color(context!!.resources.getColor(R.color.colorMatrack))

            try {
                if (lat != null && lng != null) {
                    for (i in lat!!.indices) {
                        if (lat!![i] != 0.0 && lng!![i] != 0.0) {
                            val latLngPoint = LatLng(lat!![i]!!, lng!![i]!!)
                            polylineOptions.add(latLngPoint)
                        }
                    }
                }
            } catch (ex: Exception) {
                ex.printStackTrace()
            }

            return polylineOptions
        }//close doInBackground

        override fun onPostExecute(result: Any) {
            p_bar.visibility = View.GONE
            val polyline = googleMap!!.addPolyline(result as PolylineOptions)
        }
    }

    private inner class getLocationAddress : AsyncTask<String, Void, String>() {
        override fun doInBackground(vararg url: String): String? {
            // TODO Auto-generated method stub
            var res: String? = null
            try {
                response = CustomHttpClient.executeHttpPost(url[0])
                res = response!!.toString()
                //res= res.replaceAll("\\s+","");
            } catch (e: Exception) {
                e.printStackTrace()
            }

            return res
        }//close doInBackground

        override fun onPostExecute(result: String?) {
            if (result != null) {
                if (result !== "") {
                    // Log.i("geocode address: ", result);
                    try {
                        val addrObject = JSONObject(result)
                        val addressArray = addrObject.getJSONArray("results")
                        if (addressArray != null) {
                            if (addressArray.length() > 0) {
                                val addressObj = addressArray.getJSONObject(0)
                                var addr: String? = ""
                                addr = addressObj.get("formatted_address").toString()
                                if (addr != null && addr !== "") {
                                    Address = addr
                                } else {
                                    val address_components_str = addressObj.get("address_components").toString()
                                    val address_components_arr = JSONArray(address_components_str)
                                    if (address_components_arr != null) {
                                        for (i in 0 until address_components_arr.length()) {
                                            try {
                                                val obj = address_components_arr.getJSONObject(i)
                                                if (obj != null) {
                                                    try {
                                                        val component = obj.get("long_name").toString()
                                                        if (component != null) {
                                                            if (i == 0) {
                                                                addr += component
                                                            } else {
                                                                addr += ", $component"
                                                            }
                                                        }
                                                    } catch (e: Exception) {
                                                        e.printStackTrace()
                                                    }

                                                }
                                            } catch (e: Exception) {
                                                e.printStackTrace()
                                            }

                                        }

                                        if (addr != null && addr !== "") {
                                            Address = addr
                                        }
                                    }
                                }
                            }
                        }

                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                }
            }
            if (Address != null && Address !== "") {
                try {
                    val marker = mRenderer!!.getMarker(clickedMarker)
                    marker?.showInfoWindow()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            } else {
                val getLocationAndroidGeocoder = getLocationAndroidGeocoder()
                getLocationAndroidGeocoder.execute()
            }
        }
    }

    inner class getLocationAndroidGeocoder : AsyncTask<String, Void, String>() {
        override fun doInBackground(url: Array<String>): String {
            // TODO Auto-generated method stub
            /* String res = null;
            try {
                response = CustomHttpClient.executeHttpPost(url[0]);
                res = response.toString();
                //res= res.replaceAll("\\s+","");
            } catch (Exception e) {
                //txt_Error.setText(e.toString());
                e.printStackTrace();
            } */

            val geoCoder = Geocoder(context)
            var matches: List<android.location.Address>? = null
            try {
                val latitude = lat!![clickedMarker!!.index]
                val longitude = lng!![clickedMarker!!.index]
                matches = geoCoder.getFromLocation(latitude!!, longitude!!, 1)
            } catch (e: IOException) {
                e.printStackTrace()
            }

            var address: android.location.Address? = null
            if (matches != null) {
                address = if (matches.isEmpty()) null else matches[0]
            }
            var address_full = ""

            if (address != null) {
                if (address.getAddressLine(0) == null) {
                    address_full += if (address.locality == null) "" else address.locality + " "
                    address_full += if (address.adminArea == null) "" else address.adminArea + " "
                    address_full += if (address.countryCode == null) "" else address.countryCode
                } else {
                    address_full = address.getAddressLine(0)
                }
            }

            return address_full

            //return res;
        }//close doInBackground

        override fun onPostExecute(result: String?) {
            if (result != null && result !== "") {
                Address = result
            }

            try {
                val marker = mRenderer!!.getMarker(clickedMarker)
                marker?.showInfoWindow()
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }
    }

    fun resizeMapIcons(iconName: String, width: Int, height: Int): BitmapDescriptor {
        var imageBitmap: Bitmap? = null
        if (isAdded) {
            try {
                imageBitmap = BitmapFactory.decodeResource(resources, resources.getIdentifier(iconName, "drawable", getContext()!!.packageName))
                // Drawable mapicon = ContextCompat.getDrawable(context, R.drawable.spyteciconfields);
                // imageBitmap = Bitmap.createBitmap(mapicon.getIntrinsicWidth(), mapicon.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }
        if (imageBitmap != null) {
            try {
                val resizedBitmap = Bitmap.createScaledBitmap(imageBitmap, width, height, false)
                return BitmapDescriptorFactory.fromBitmap(resizedBitmap)
            } catch (ex: Exception) {
                ex.printStackTrace()
            }

        }

        return BitmapDescriptorFactory.fromBitmap(imageBitmap)
    }

    private fun bitmapDescriptorFromVector(context: Context, @DrawableRes vectorDrawableResourceId: Int): BitmapDescriptor {
        val mapicon = ContextCompat.getDrawable(context, vectorDrawableResourceId)
        val bitmap = Bitmap.createBitmap(mapicon!!.intrinsicWidth, mapicon.intrinsicHeight, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        mapicon.draw(canvas)
        return BitmapDescriptorFactory.fromBitmap(bitmap)
    }

    companion object {
        lateinit var rootView: View
        lateinit var zoomplus: ImageView
        lateinit var zoomminus: ImageView
        lateinit var rb_normal: Button
        lateinit var rb_satellite: Button
        lateinit var rb_hybrid: Button

        var TAG = "Track History"
        var isDestroyed: Boolean? = false
        internal lateinit var p_bar: TextView
        var clickedMarker: MyItem? = null

        var mRenderer: DefaultClusterRenderer<MyItem>? = null
        var start_date: String? = ""
        var end_date: String? = ""
        var data_point: String? = ""
        var driver: String? = ""
        var client = ""
        lateinit var google_maps_api: String
        lateinit var progressLayout: LinearLayout
        lateinit var progressBar: ProgressBar
        lateinit var progressText: TextView

        lateinit var trackhistory_title: TextView
        lateinit var back_btn: ImageView

        fun closeInfowindow() {
            if (clickedMarker != null) {
                if (mRenderer != null) {
                    val marker = mRenderer!!.getMarker(clickedMarker)
                    if (marker != null) {
                        if (marker.isInfoWindowShown) {
                            try {
                                marker.hideInfoWindow()
                            } catch (e: Exception) {
                                e.printStackTrace()
                            }

                        }
                    }
                }
            }
        }

        val isInfoWindowOpen: Boolean
            get() {
                try {
                    if (clickedMarker != null) {
                        if (mRenderer != null) {
                            val marker = mRenderer!!.getMarker(clickedMarker)
                            if (marker != null) {
                                if (marker.isInfoWindowShown) {
                                    return true
                                }
                            }
                        }
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }

                return false
            }
    }
}// Required empty public constructor