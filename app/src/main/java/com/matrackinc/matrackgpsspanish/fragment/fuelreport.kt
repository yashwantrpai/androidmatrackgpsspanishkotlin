package com.matrackinc.matrackgpsspanish.fragment

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Bitmap
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentTransaction
import android.util.Log
import android.util.Pair
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.Toast

import com.matrackinc.matrackgpsspanish.CreatePostString
import com.matrackinc.matrackgpsspanish.db.DatabaseHelper
import com.matrackinc.matrackgpsspanish.R
import com.matrackinc.matrackgpsspanish.activity.HomeActivity
import com.matrackinc.matrackgpsspanish.activity.SplashScreen
import com.matrackinc.matrackgpsspanish.util.Util

import java.util.ArrayList

import android.content.Context.MODE_PRIVATE
import android.widget.TextView
import com.example.webview.WebViewClt
import com.matrackinc.matrackgpsspanish.util.DateUtil
import com.tutorialwing.webview.WebChromeClt

/**
 * A simple [Fragment] subclass.
 */
class fuelreport : Fragment() {
    internal lateinit var dbHelper: DatabaseHelper
    internal var TAG = "FuelReport"
    private var wv1: WebView? = null
    lateinit var pref: SharedPreferences
    lateinit var editor: SharedPreferences.Editor
    // lateinit var progress: ProgressBar

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.fuelreport, container, false)
        dbHelper = DatabaseHelper()

        pref = context!!.getSharedPreferences("DataStore", MODE_PRIVATE)
        editor = pref.edit()
        if (pref.getString("curfile", "") == null || !HomeActivity.equalStrings(pref.getString("curfile", "")!!.toString(), "fuelreport")) {
            editor.putString("curfile", "fuelreport")
            editor.commit()
        }

        fuel_title = rootView.findViewById<View>(R.id.fuel_title) as TextView
        fuel_title!!.setText(if (pref.getString("language", "0") == "0") R.string.fuel else R.string.fuel_esp)
        wv1 = rootView.findViewById<View>(R.id.webView) as WebView

        // String start_date = String.valueOf(fuelreportform.fuel_values.get(0));
        val start_date = pref.getString("startdate", "")
        // String end_date = String.valueOf(fuelreportform.fuel_values.get(1));
        val end_date = pref.getString("enddate", "")
        // String driver = String.valueOf(fuelreportform.fuel_values.get(2));
        val driver = pref.getString("driver", "")
        val client = dbHelper.client
        //Log.i(TAG, start_date + " " + end_date + " " + driver + " " + client);

        // Back button
        val back_btn = rootView.findViewById<View>(R.id.back) as ImageView
        back_btn.setOnClickListener {
            val reportsFragment = fuelreportform()
            val fragmentTransaction = fragmentManager!!.beginTransaction()
            fragmentTransaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
            editor.putString("CURRENT_TAG", HomeActivity.TAG_FUELREPORT_FORM)
            editor.commit()
            fragmentTransaction.replace(R.id.frame, reportsFragment, HomeActivity.TAG_FUELREPORT_FORM)
            fragmentTransaction.commitAllowingStateLoss()
        }

        if (driver !== "" && start_date !== "" && end_date !== "" && client !== "") {
            val currentServer = pref.getString("currentServer", "")
            val urls = resources.getStringArray(R.array.url)
            if (Util.isValidUrl(currentServer, urls)) {
                val url = currentServer!! + getString(if (pref.getString("language", "0") == "0") R.string.fuelreport_url else R.string.fuelreport_url_esp)
                val params = ArrayList<Pair<String, String>>()
                var pair = Pair("operation", "fuelreport")
                params.add(pair)
                pair = Pair("driver", driver!!.replace(" ", ""))
                params.add(pair)
                pair = Pair("startdate", if (pref.getString("language", "0") == "1") DateUtil.toSpanishDate(start_date) else start_date)
                params.add(pair)
                pair = Pair("enddate", if (pref.getString("language", "0") == "1") DateUtil.toSpanishDate(end_date) else end_date)
                params.add(pair)
                pair = Pair("authcode", dbHelper.authcode)
                params.add(pair)
                val postString = "payload=" + CreatePostString.createPostString(params)

                // Log.d("fuelreport", "url: " + url + "postdata: " + postString);
                if (wv1 != null) {

                    val webSettings = wv1!!.settings
                    wv1!!.settings.javaScriptEnabled = true
                    // webSettings.builtInZoomControls = true
                    wv1!!.settings.setSupportZoom(true)

                    wv1!!.settings.setBuiltInZoomControls(true);
                    wv1!!.settings.setDisplayZoomControls(true);
                    // wv1!!.settings.textZoom = 12

                    wv1!!.webViewClient = WebViewClt(activity)
                    wv1!!.webChromeClient = WebChromeClt(activity)

                    wv1!!.postUrl(url, postString.toByteArray())
                }
            } else {
                Toast.makeText(context, if (pref.getString("language", "0") == "0") R.string.server_connection_issue else R.string.server_connection_issue_esp, Toast.LENGTH_SHORT).show()
                val i = Intent(context, SplashScreen::class.java)
                i.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(i)
            }
        } else {
            Log.d(TAG, "Input values not inserted properly in Fuel Report")
        }

        return rootView
    }

    companion object {
        var fuel_title: TextView? = null
        var context: Context? = null
    }
}// Required empty public constructor
