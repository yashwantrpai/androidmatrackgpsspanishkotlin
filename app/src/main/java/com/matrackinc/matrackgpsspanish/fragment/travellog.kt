package com.matrackinc.matrackgpsspanish.fragment

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Bitmap
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentTransaction
import android.util.Log
import android.util.Pair
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.Toast

import com.matrackinc.matrackgpsspanish.CreatePostString
import com.matrackinc.matrackgpsspanish.db.DatabaseHelper
import com.matrackinc.matrackgpsspanish.R
import com.matrackinc.matrackgpsspanish.activity.HomeActivity
import com.matrackinc.matrackgpsspanish.activity.SplashScreen
import com.matrackinc.matrackgpsspanish.util.Util

import java.util.ArrayList

import android.content.Context.MODE_PRIVATE
import android.widget.TextView
import com.example.webview.WebViewClt
import com.matrackinc.matrackgpsspanish.util.DateUtil.toSpanishDate
import com.matrackinc.matrackgpsspanish.util.Util.getDatapoint
import com.tutorialwing.webview.WebChromeClt

/**
 * A simple [Fragment] subclass.
 */
class travellog : Fragment() {
    internal lateinit var dbHelper: DatabaseHelper
    internal var TAG = "Travel Log"
    private var wv1: WebView? = null
    lateinit var pref: SharedPreferences
    lateinit var editor: SharedPreferences.Editor
    // lateinit var progress: ProgressBar

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.travellog, container, false)
        dbHelper = DatabaseHelper()

        pref = context!!.getSharedPreferences("DataStore", MODE_PRIVATE)
        editor = pref.edit()
        if (pref.getString("curfile", "") == null || !HomeActivity.equalStrings(pref.getString("curfile", "")!!.toString(), "travellog")) {
            editor.putString("curfile", "travellog")
            editor.commit()
        }


        travellog_title = rootView.findViewById<View>(R.id.travellog_title) as TextView
        travellog_title!!.setText(if (pref.getString("language", "0") == "0") R.string.travellog else R.string.travellog_esp)
        wv1 = rootView.findViewById<View>(R.id.webView) as WebView

        // String[] startDatesplit = String.valueOf(travellogform.travellog_values.get(0)).split("/");
        // String[] endDatesplit = String.valueOf(travellogform.travellog_values.get(1)).split("/");

        // String start_date = String.valueOf(travellogform.travellog_values.get(0));
        val start_date = pref.getString("startdate", "")
        // String end_date = String.valueOf(travellogform.travellog_values.get(1));
        val end_date = pref.getString("enddate", "")
        // String datapoint = String.valueOf(travellogform.travellog_values.get(2));
        var datapoint = pref.getString("datapoint", "")
        // String driver = String.valueOf(travellogform.travellog_values.get(3));
        val driver = pref.getString("driver", "")
        val client = dbHelper.client

        // Back button
        val back_btn = rootView.findViewById<View>(R.id.back) as ImageView
        back_btn.setOnClickListener {
            val reportsFragment = travellogform()
            val fragmentTransaction = fragmentManager!!.beginTransaction()
            fragmentTransaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
            editor.putString("CURRENT_TAG", HomeActivity.TAG_TRAVELLOG_FORM)
            editor.commit()
            fragmentTransaction.replace(R.id.frame, reportsFragment, HomeActivity.TAG_TRAVELLOG_FORM)
            fragmentTransaction.commitAllowingStateLoss()
        }

        // Log.i(TAG,"Entered values "+ start_date+" "+end_date+" "+" "+datapoint+" "+driver+" "+client);

        if (datapoint == "STOPS ONLY") {
            datapoint = datapoint.replace(" ", "")
        } else if (datapoint == "ENGINE ON/OFF") {
            datapoint = "IGNITION"
        } else if (datapoint == "MOVEMENTS ONLY") {
            datapoint = "MOVEMENT"
        }

        if (driver !== "" && start_date !== "" && end_date !== "" && datapoint !== "" && client !== "") {
            val currentServer = pref.getString("currentServer", "")
            val urls = resources.getStringArray(R.array.url)
            if (Util.isValidUrl(currentServer, urls)) {
                val url = currentServer!! + getString(if (pref.getString("language", "0") == "0") R.string.travellogreport_url else R.string.travellog_url_esp)
                val params = ArrayList<Pair<String, String>>()

                val datapointStr = if (pref.getString("language", "0") == "1") getDatapoint(datapoint) else datapoint
                var pair = Pair("operation", "travellogreport")
                params.add(pair)
                pair = Pair("driver", driver!!.replace(" ", ""))
                params.add(pair)
                pair = Pair("startdate", if (pref.getString("language", "0") == "1") toSpanishDate(start_date) else start_date)
                params.add(pair)
                pair = Pair("enddate", if (pref.getString("language", "0") == "1") toSpanishDate(end_date) else end_date)
                params.add(pair)
                pair = Pair("datapoint", datapointStr)
                params.add(pair)
                pair = Pair("authcode", dbHelper.authcode)
                params.add(pair)
                val postString = "payload=" + CreatePostString.createPostString(params)

                Log.i("url", url)
                Log.i("param", postString)
                if (wv1 != null) {

                    val webSettings = wv1!!.settings
                    wv1!!.settings.javaScriptEnabled = true
                    // webSettings.builtInZoomControls = true
                    wv1!!.settings.setSupportZoom(true)

                    wv1!!.settings.setBuiltInZoomControls(true);
                    wv1!!.settings.setDisplayZoomControls(true);
                    // wv1!!.settings.textZoom =

                    wv1!!.webViewClient = WebViewClt(activity)
                    wv1!!.webChromeClient = WebChromeClt(activity)

                    wv1!!.postUrl(url, postString.toByteArray())
                }
            } else {
                Toast.makeText(context, if (pref.getString("language", "0") == "0") R.string.server_connection_issue else R.string.server_connection_issue_esp, Toast.LENGTH_SHORT).show()
                val i = Intent(context, SplashScreen::class.java)
                i.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(i)
            }
        } else {
            Log.d(TAG, "Input values not inserted properly in Travel Log Report")
        }

        return rootView
    }

    companion object {

        var travellog_title: TextView? = null
        var context: Context? = null
    }
}// Required empty public constructor
