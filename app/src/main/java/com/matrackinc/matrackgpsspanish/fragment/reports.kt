package com.matrackinc.matrackgpsspanish.fragment

import android.content.SharedPreferences
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentTransaction
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView

import com.matrackinc.matrackgpsspanish.R
import com.matrackinc.matrackgpsspanish.activity.HomeActivity

import java.util.ArrayList

import android.content.Context.MODE_PRIVATE

/**
 * A simple [Fragment] subclass.
 */
class reports : Fragment() {

    internal var Image_icon = ArrayList<Int>()
    internal var text = ArrayList<String>()
    internal var Image = ArrayList<Int>()
    internal lateinit var Alert_recycler_View: RecyclerView
    internal lateinit var Adapter: ReportsAdapter
    lateinit var pref: SharedPreferences
    lateinit var editor: SharedPreferences.Editor


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.reports, container, false)

        pref = context!!.getSharedPreferences("DataStore", MODE_PRIVATE)
        editor = pref.edit()

        Image_icon.clear()
        text.clear()
        Image.clear()

        Alert_recycler_View = rootView.findViewById<View>(R.id.recycler_view) as RecyclerView
        Adapter = ReportsAdapter(Image_icon, text, Image)
        Alert_recycler_View.adapter = Adapter
        Alert_recycler_View.setHasFixedSize(true)
        Alert_recycler_View.layoutManager = LinearLayoutManager(activity)

        setNewLayout(R.drawable.reporticon, if (pref.getString("language", "0") == "0") getString(R.string.dtc) else getString(R.string.dtc_esp), R.drawable.next)
        setNewLayout(R.drawable.reporticon, if (pref.getString("language", "0") == "0") getString(R.string.travellog) else getString(R.string.travellog_esp), R.drawable.next)
        setNewLayout(R.drawable.reporticon, if (pref.getString("language", "0") == "0") getString(R.string.mileage) else getString(R.string.mileage_esp), R.drawable.next)
        setNewLayout(R.drawable.reporticon, if (pref.getString("language", "0") == "0") getString(R.string.fuel) else getString(R.string.fuel_esp), R.drawable.next)

        return rootView
    }

    fun setNewLayout(image_icon: Int, txt: String, image: Int) {
        Image_icon.add(image_icon)
        text.add(txt)
        Image.add(image)

        Adapter.notifyDataSetChanged()
    }

    fun goToFragment(position: Int) {
        val CURRENT_TAG = HomeActivity.TAG_REPORTS
        var targetFragment: Fragment = reports()
        when (position) {
            0 -> {
                targetFragment = dtcform()
                editor.putString("CURRENT_TAG", HomeActivity.TAG_DTC_FORM)
                editor.commit()
            }

            1 -> {
                targetFragment = travellogform()
                editor.putString("CURRENT_TAG", HomeActivity.TAG_TRAVELLOG_FORM)
                editor.commit()
            }

            2 -> {
                targetFragment = mileageform()
                editor.putString("CURRENT_TAG", HomeActivity.TAG_MILEAGE_FORM)
                editor.commit()
            }

            3 -> {
                targetFragment = fuelreportform()
                editor.putString("CURRENT_TAG", HomeActivity.TAG_FUELREPORT_FORM)
                editor.commit()
            }

            else -> {
                targetFragment = reports()
                editor.putString("CURRENT_TAG", HomeActivity.TAG_REPORTS)
                editor.commit()
            }
        }

        val fragmentTransaction = fragmentManager!!.beginTransaction()
        fragmentTransaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
        fragmentTransaction.replace(R.id.frame, targetFragment, CURRENT_TAG)
        fragmentTransaction.commitAllowingStateLoss()
    }

    inner class ReportsAdapter(Image_icon: ArrayList<Int>, text: ArrayList<String>, Image: ArrayList<Int>) : RecyclerView.Adapter<ReportsAdapter.MyViewHolder>() {

        internal var Image_icon = ArrayList<Int>()
        internal var Text = ArrayList<String>()
        internal var Image = ArrayList<Int>()

        init {
            this.Image_icon = Image_icon
            this.Text = text
            this.Image = Image
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ReportsAdapter.MyViewHolder {
            val card_view = LayoutInflater.from(parent.context).inflate(R.layout.viewlayout, parent, false)
            return MyViewHolder(card_view)
        }

        override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
            try {
                holder.text.text = Text[position]
                holder.image_icon.setImageResource(Image_icon[position])
                holder.image.setImageResource(Image[position])

                holder.image.setOnClickListener { goToFragment(position) }
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }

        override fun getItemCount(): Int {
            return Image_icon.size
        }

        inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            var text: TextView
            internal var image_icon: ImageView
            internal var image: ImageView

            init {
                image_icon = view.findViewById<View>(R.id.image_icon) as ImageView
                text = view.findViewById<View>(R.id.text) as TextView
                image = view.findViewById<View>(R.id.next) as ImageView
            }
        }
    }
}// Required empty public constructor
