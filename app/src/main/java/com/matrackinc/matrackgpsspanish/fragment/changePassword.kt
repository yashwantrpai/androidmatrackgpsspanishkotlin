package com.matrackinc.matrackgpsspanish.fragment

import android.content.Context
import android.content.DialogInterface
import android.content.SharedPreferences
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.AsyncTask
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentTransaction
import android.app.AlertDialog
import android.util.Log
import android.util.Pair
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast

import com.matrackinc.matrackgpsspanish.CreatePostString
import com.matrackinc.matrackgpsspanish.CustomHttpClient
import com.matrackinc.matrackgpsspanish.R
import com.matrackinc.matrackgpsspanish.activity.HomeActivity
import com.matrackinc.matrackgpsspanish.db.DatabaseHelper

import java.lang.reflect.Field
import java.util.ArrayList

import android.content.Context.MODE_PRIVATE

class changePassword : Fragment() {
    lateinit var editor: SharedPreferences.Editor
    var indicator_layout: LinearLayout? = null
    var indicator_img: ImageView? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.change_password, container, false)

        activity_context = this.activity
        dbHelper = DatabaseHelper()

        pref = context!!.getSharedPreferences("DataStore", MODE_PRIVATE)
        editor = pref.edit()

        changePassword_title = rootView.findViewById<View>(R.id.change_password_title) as TextView
        changePassword_title.setText(if (pref.getString("language", "0") == "0") R.string.change_password else R.string.change_password_esp)
        val old_password_txt = rootView.findViewById<View>(R.id.old_password) as EditText
        old_password_txt.setHint(if (pref.getString("language", "0") == "0") R.string.old_password else R.string.old_password_esp)
        val new_password_txt = rootView.findViewById<View>(R.id.new_password) as EditText
        new_password_txt.setHint(if (pref.getString("language", "0") == "0") R.string.new_password else R.string.new_password_esp)
        val confirm_new_password_txt = rootView.findViewById<View>(R.id.re_enter_new_password) as EditText
        confirm_new_password_txt.setHint(if (pref.getString("language", "0") == "0") R.string.re_enter_password else R.string.re_enter_password_esp)

        connectivityManager = activity!!.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        // retain this fragment
        retainInstance = true

        val submit = rootView.findViewById<View>(R.id.submit) as Button
        submit.setText(if (pref.getString("language", "0") == "0") R.string.submit else R.string.submit_esp)
        submit.setOnClickListener {
            if (old_password_txt.text.toString() != "" && new_password_txt.text.toString() != "" && confirm_new_password_txt.text.toString() != "") {
                old_password_str = old_password_txt.text.toString().trim { it <= ' ' }
                new_password_str = new_password_txt.text.toString().trim { it <= ' ' }
                confirm_new_password_str = confirm_new_password_txt.text.toString().trim { it <= ' ' }

                if (new_password_str == confirm_new_password_str) {
                    val activeNetworkInfo = connectivityManager.activeNetworkInfo
                    if (activeNetworkInfo != null && activeNetworkInfo.isConnected) {
                        resetPassword()
                    } else {
                        Toast.makeText(context, if (pref.getString("language", "0") == "0") R.string.internet_connectivity_check else R.string.internet_connectivity_check_esp, Toast.LENGTH_SHORT).show()
                    }
                } else {
                    val builder = AlertDialog.Builder(activity)
                    builder.setMessage(if (pref.getString("language", "0") == "0") R.string.confirm_password_correctly else R.string.confirm_password_correctly_esp)
                            .setCancelable(false)
                            .setNeutralButton(activity!!.resources.getString(if (pref.getString("language", "0") == "0") R.string.ok else R.string.ok_esp)) { dialog, id -> dialog.cancel() }
                    val alert = builder.create()
                    alert.show()
                }
            } else {
                Toast.makeText(context, if (pref.getString("language", "0") == "0") R.string.enter_valid_data_for_fields else R.string.enter_valid_data_for_fields_esp, Toast.LENGTH_SHORT).show()
            }
            // } else {
            //  Toast.makeText(getContext(), (pref.getString("language", "0").equals("0"))?R.string.enter_valid_data_for_fields:R.string.enter_valid_data_for_fields_esp, Toast.LENGTH_LONG).show();
            // }
        }

        val back = rootView.findViewById<View>(R.id.back) as ImageView
        back.setOnClickListener {
            if (!activity!!.isFinishing) {
                val CURRENT_TAG = HomeActivity.TAG_HELP
                val fragmentManager = fragmentManager
                val transaction = fragmentManager!!.beginTransaction()
                transaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                transaction.replace(R.id.frame, help(), CURRENT_TAG)
                transaction.addToBackStack(null)
                transaction.commitAllowingStateLoss()
            }
        }

        return rootView
    }

    override fun onDetach() {
        super.onDetach()

        try {
            val childFragmentManager = Fragment::class.java.getDeclaredField("mChildFragmentManager")
            childFragmentManager.isAccessible = true
            childFragmentManager.set(this, null)

        } catch (e: NoSuchFieldException) {
            throw RuntimeException(e)
        } catch (e: IllegalAccessException) {
            throw RuntimeException(e)
        }

    }

    class resetPasswordTask : AsyncTask<String, Void, String>(), DialogInterface.OnCancelListener {

        override fun doInBackground(vararg url: String): String? {
            // TODO Auto-generated method stub
            var res: String? = null
            try {
                response = CustomHttpClient.executeHttpPost(url[0], url[1])
                res = response
                //res= res.replaceAll("\\s+","");
            } catch (e: Exception) {
                e.printStackTrace()
            }

            return res
        }//close doInBackground

        override fun onPostExecute(result: String?) {
            var responsefailed: Boolean? = false
            if (result != null && result !== "" && !response.isEmpty()) {

                if (response.contains("updated New password")) {
                    Log.d("changePassword", "Password changed successfully")
                    val builder = AlertDialog.Builder(activity_context)
                    builder.setMessage(if (pref.getString("language", "0") == "0") R.string.password_changed_successfully else R.string.password_changed_successfully_esp)
                            .setCancelable(false)
                            .setNeutralButton(activity_context!!.resources.getString(if (pref.getString("language", "0") == "0") R.string.ok else R.string.ok_esp)) { dialog, id -> dialog.cancel() }
                    val alert = builder.create()
                    alert.show()
                } else {
                    responsefailed = true
                }
            } else {
                responsefailed = true
            }

            if (responsefailed!!) {
                val builder = AlertDialog.Builder(activity_context)
                builder.setMessage(if (pref.getString("language", "0") == "0") R.string.reset_password_failed else R.string.reset_password_failed_esp)
                        .setCancelable(false)
                        .setNeutralButton(activity_context!!.resources.getString(if (pref.getString("language", "0") == "0") R.string.ok else R.string.ok_esp)) { dialog, id -> dialog.cancel() }
                val alert = builder.create()
                alert.show()
            }
        }

        override fun onCancel(dialog: DialogInterface) {
            cancel(true)
        }
    }

    companion object {
        lateinit var old_password_str: String
        lateinit var new_password_str: String
        lateinit var confirm_new_password_str: String
        lateinit var connectivityManager: ConnectivityManager
        var activity_context: Context? = null
        var response = ""
        lateinit var pref: SharedPreferences
        lateinit var dbHelper: DatabaseHelper
        private val TAG = "ChangePassword"
        lateinit var changePassword_title: TextView

        fun resetPassword() {
            val changePassword = if (pref.getString("language", "0") == "0") activity_context!!.resources.getString(R.string.change_password_url) else activity_context!!.getString(R.string.change_password_url_esp)
            var resetPasswordUrl: String? = null
            try {
                val currentServer = pref.getString("currentServer", "")
                resetPasswordUrl = currentServer!! + changePassword
            } catch (e: Exception) {
                e.printStackTrace()
            }

            val params = ArrayList<Pair<String, String>>()
            var pair = Pair("operation", "changePassword")
            params.add(pair)
            pair = Pair("authcode", dbHelper.authcode)
            params.add(pair)
            pair = Pair("oldpassword", old_password_str)
            params.add(pair)
            pair = Pair("newpassword", new_password_str)
            params.add(pair)

            val postString = CreatePostString.createPostString(params)
            val resetPasswordTask = resetPasswordTask()
            resetPasswordTask.execute(resetPasswordUrl, postString)
        }
    }
}// Required empty public constructor