package com.matrackinc.matrackgpsspanish.fragment

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.res.Resources
import android.net.Uri
import android.os.AsyncTask
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentTransaction
import android.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.DisplayMetrics
import android.util.Log
import android.util.Pair
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.RatingBar
import android.widget.Switch
import android.widget.TextView
import android.widget.Toast

import com.matrackinc.matrackgpsspanish.CreatePostString
import com.matrackinc.matrackgpsspanish.CustomHttpClient
import com.matrackinc.matrackgpsspanish.R
import com.matrackinc.matrackgpsspanish.activity.HomeActivity
import com.matrackinc.matrackgpsspanish.db.DatabaseHelper

import android.content.Context.MODE_PRIVATE

import android.content.SharedPreferences

import java.util.ArrayList
import java.util.Locale

/**
 * A simple [Fragment] subclass.
 */
class help : Fragment() {
    internal var loggingSwitch: Switch? = null
    lateinit var pref: SharedPreferences
    lateinit var editor: SharedPreferences.Editor
    var activity: Activity? = null
    internal var Image_icon = ArrayList<Int>()
    internal var text = ArrayList<String>()
    internal var Image = ArrayList<Int>()
    internal lateinit var Alert_recycler_View: RecyclerView
    internal lateinit var Adapter: SettingsAdapter
    lateinit var ratingAlert: AlertDialog.Builder
    lateinit var selectLanguageAlert: AlertDialog.Builder
    lateinit var ratingDialog: AlertDialog
    lateinit var languageSelectDialog: AlertDialog
    lateinit var ratingView: View
    lateinit var languageSelectView: View
    lateinit var ratingLayout: LinearLayout
    lateinit var radioGroup: RadioGroup
    lateinit var dbHelper: DatabaseHelper
    lateinit var response: String
    var isLanguageModified = false

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val rootView = inflater.inflate(R.layout.fragment_help, container, false)
        pref = context!!.getSharedPreferences("DataStore", MODE_PRIVATE)
        editor = pref.edit()
        activity = getActivity()
        dbHelper = DatabaseHelper()

        Image_icon.clear()
        text.clear()
        Image.clear()

        Alert_recycler_View = rootView.findViewById<View>(R.id.recycler_view) as RecyclerView
        Adapter = SettingsAdapter(Image_icon, text, Image)
        Alert_recycler_View.adapter = Adapter
        Alert_recycler_View.setHasFixedSize(true)
        Alert_recycler_View.layoutManager = LinearLayoutManager(getActivity())

        // setNewLayout(R.drawable.languageselect, getString(if (pref.getString("language", "0") == "0") R.string.change_language else R.string.change_language_esp), R.drawable.next)
        setNewLayout(R.drawable.changepassword, getString(if (pref.getString("language", "0") == "0") R.string.change_password else R.string.change_password_esp), R.drawable.next)
        setNewLayout(R.drawable.mailus, getString(if (pref.getString("language", "0") == "0") R.string.contact_us else R.string.contact_us_esp), R.drawable.next)
        setNewLayout(R.drawable.rateus, getString(if (pref.getString("language", "0") == "0") R.string.rate_us else R.string.rate_us_esp), R.drawable.next)

        return rootView
    }

    fun sendMail() {
        val emailIntent = Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto", "support.zarzegps@matrackinc.com", null))
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, if (pref.getString("language", "0") == "0") R.string.support_mail else R.string.support_mail_esp )
        emailIntent.putExtra(Intent.EXTRA_TEXT, "")
        try {
            startActivity(Intent.createChooser(emailIntent, if (pref.getString("language", "0") == "0") getString(R.string.send_mail) else getString(R.string.send_mail_esp)))
        } catch (ex:java.lang.Exception){
            ex.printStackTrace()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

    }

    fun inflateRatingLayout() {
        val inflater = layoutInflater
        ratingView = inflater.inflate(R.layout.ratinglayout, null)
        ratingLayout = ratingView.findViewById<View>(R.id.rating_layout) as LinearLayout
        ratingLayout.visibility = View.VISIBLE
        val rating_layout_title = ratingView.findViewById<View>(R.id.rating_layout_title) as TextView
        rating_layout_title.setText(if (pref.getString("language", "0") == "0") R.string.rate_us else R.string.rate_us_esp)
        val rate_me = ratingView.findViewById<View>(R.id.rate_me) as TextView
        rate_me.setText(if (pref.getString("language", "0") == "0") R.string.rating_text else R.string.rating_text_esp)
        val close = ratingView.findViewById<View>(R.id.close) as ImageView
        close.setOnClickListener { ratingDialog.dismiss() }
        val ratingBar = ratingView.findViewById<View>(R.id.ratingBar) as RatingBar
        val ratingText = ratingView.findViewById<View>(R.id.ratingText) as EditText
        ratingText.setHint(if (pref.getString("language", "0") == "0") R.string.review_hint else R.string.review_hint_esp)
        val submitRating = ratingView.findViewById<View>(R.id.submit) as Button
        submitRating.setText(if (pref.getString("language", "0") == "0") R.string.submit else R.string.submit_esp)

        ratingAlert = AlertDialog.Builder(activity)
        // this is set the view from XML inside AlertDialog
        ratingAlert.setView(ratingView)
        // disallow cancel of AlertDialog on click of back button and outside touch
        ratingAlert.setCancelable(false)
        ratingDialog = ratingAlert.create()

        submitRating.setOnClickListener {
            val rating = ratingBar.rating
            val reviewText = ratingText.text.toString()
            submitReview(rating, reviewText)
        }

        ratingDialog.show()
    }

    fun submitReview(rating: Float, review: String) {
        val pref: SharedPreferences
        pref = context!!.getSharedPreferences("DataStore", Context.MODE_PRIVATE)
        val currentServer = pref.getString("currentServer", "")
        val sendUserFeedbackUrl = currentServer!! + if (pref.getString("language", "0") == "0") getString(R.string.star_rating_url) else getString(R.string.star_rating_url_esp)

        val params = ArrayList<Pair<String, String>>()
        var pair = Pair("operation", "SubmitRatings")
        params.add(pair)
        pair = Pair("authcode", dbHelper.authcode)
        params.add(pair)
        pair = Pair("ratings", rating.toString())
        params.add(pair)
        pair = Pair("comments", review)
        params.add(pair)

        val postString = CreatePostString.createPostString(params)
        val sendUserFeedbackTask = sendUserFeedbackTask()
        sendUserFeedbackTask.execute(sendUserFeedbackUrl, postString)
    }

    inner class sendUserFeedbackTask : AsyncTask<String, Void, String>() {

        override fun doInBackground(vararg url: String): String? {
            // TODO Auto-generated method stub
            var res: String? = null
            try {
                response = CustomHttpClient.executeHttpPost(url[0], url[1])
                res = response
            } catch (e: Exception) {

            }

            return res
        }//close doInBackground

        override fun onPostExecute(result: String?) {
            var responsefailed: Boolean? = false

            if (result != null && result !== "" && !result.isEmpty()) {
                response = result
                Log.d("sendFeedback", response)

                if (response.contains("success")) {
                    Log.d("sendFeedback", "Feedback sent successfully")
                    if (ratingDialog.isShowing) {
                        ratingLayout.visibility = View.GONE
                        val thanksLayout = ratingView.findViewById<View>(R.id.thanks_layout) as LinearLayout
                        thanksLayout.visibility = View.VISIBLE
                        val thank_you_text = ratingView.findViewById<View>(R.id.thanksText) as TextView
                        thank_you_text.setText(if (pref.getString("language", "0") == "0") R.string.thank_you_text else R.string.thank_you_text_esp)
                        val backBtn = ratingView.findViewById<Button>(R.id.backBtn)
                        backBtn.setText(if (pref.getString("language", "0") == "0") R.string.back else R.string.back_esp)
                        backBtn.setOnClickListener { ratingDialog.dismiss() }
                    }
                } else {
                    responsefailed = true
                }
            } else {
                responsefailed = true
            }

            if (responsefailed!!) {
                if (ratingDialog.isShowing) {
                    ratingDialog.dismiss()
                }
                Toast.makeText(context, if (pref.getString("language", "0") == "0") R.string.send_feedback else R.string.send_feedback_esp, Toast.LENGTH_SHORT).show()
            }
        }
    }

    fun setNewLayout(image_icon: Int, txt: String, image: Int) {
        Image_icon.add(image_icon)
        text.add(txt)
        Image.add(image)

        Adapter.notifyDataSetChanged()
    }

    override fun onResume() {
        super.onResume()

        val settings = getActivity()!!.getSharedPreferences(PREFS_NAME, 0)
        val isLogEnabled = settings.getBoolean("logEnabled", false)
        Log.i("help", "### onResume: $isLogEnabled")
        if (isLogEnabled) {
            if (loggingSwitch != null) {
                loggingSwitch!!.isChecked = isLogEnabled
            }
        }
    }

    inner class SettingsAdapter(Image_icon: ArrayList<Int>, text: ArrayList<String>, Image: ArrayList<Int>) : RecyclerView.Adapter<SettingsAdapter.MyViewHolder>() {

        internal var Image_icon = ArrayList<Int>()
        internal var Text = ArrayList<String>()
        internal var Image = ArrayList<Int>()

        init {
            this.Image_icon = Image_icon
            this.Text = text
            this.Image = Image
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SettingsAdapter.MyViewHolder {
            val card_view = LayoutInflater.from(parent.context).inflate(R.layout.viewlayout, parent, false)
            return MyViewHolder(card_view)
        }

        override fun onBindViewHolder(holder: SettingsAdapter.MyViewHolder, position: Int) {
            try {
                holder.text.text = Text[position]
                holder.image_icon.setImageResource(Image_icon[position])
                holder.image.setImageResource(Image[position])

                holder.image.setOnClickListener { action(position) }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        override fun getItemCount(): Int {
            return Image_icon.size
        }

        inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            var text: TextView
            internal var image_icon: ImageView
            internal var image: ImageView

            init {
                image_icon = view.findViewById<View>(R.id.image_icon) as ImageView
                text = view.findViewById<View>(R.id.text) as TextView
                image = view.findViewById<View>(R.id.next) as ImageView
            }
        }
    }

    fun inflateLanguageSelectLayout() {
        val inflater = layoutInflater
        languageSelectView = inflater.inflate(R.layout.language_select_layout, null)
        radioGroup = languageSelectView.findViewById<View>(R.id.radioGroup) as RadioGroup
        val languageSelectTitle = languageSelectView.findViewById<TextView>(R.id.select_language_layout_title)
        val english = languageSelectView.findViewById<View>(R.id.english) as RadioButton
        val spanish = languageSelectView.findViewById<View>(R.id.spanish) as RadioButton
        val submit = languageSelectView.findViewById<View>(R.id.submit) as Button
        submit.setText(if (pref.getString("language", "0") == "0") R.string.save else R.string.save_esp)
        val close = languageSelectView.findViewById<View>(R.id.close) as ImageView
        close.setOnClickListener { languageSelectDialog.dismiss() }

        languageSelectTitle.setText(if (pref.getString("language", "0") == "0") R.string.change_language else R.string.change_language_esp)

        selectLanguageAlert = AlertDialog.Builder(activity)
        // this is set the view from XML inside AlertDialog
        selectLanguageAlert.setView(languageSelectView)
        // disallow cancel of AlertDialog on click of back button and outside touch
        selectLanguageAlert.setCancelable(false)
        languageSelectDialog = selectLanguageAlert.create()

        languageSelectDialog.show()
        if (pref.getString("language", "0") == "1") {
            radioGroup.check(R.id.spanish)
        } else {
            radioGroup.check(R.id.english)
        }

        submit.setOnClickListener {
            languageSelectDialog.dismiss()
            val checkedId = radioGroup.checkedRadioButtonId
            val checked = Integer.parseInt(pref.getString("language", "0")!!)
            var checked_btn = R.id.english
            if (checked == 0) {
                checked_btn = R.id.english
            } else {
                checked_btn = R.id.spanish
            }

            if (checked_btn != checkedId) {
                when (checkedId) {
                    R.id.english -> {
                        editor.putString("language", "0")
                        editor.apply()
                    }

                    R.id.spanish -> {
                        editor.putString("language", "1")
                        editor.apply()
                    }
                }

                val intent = getActivity()!!.intent
                getActivity()!!.finish()
                startActivity(intent)
            }
        }
    }

    fun loadChangePasswordFragment() {
        var targetFragment: Fragment = help()
        val CURRENT_TAG = HomeActivity.TAG_CHANGE_PASSWORD
        targetFragment = changePassword()
        editor.putString("CURRENT_TAG", HomeActivity.TAG_CHANGE_PASSWORD)
        editor.commit()
        val fragmentTransaction = fragmentManager!!.beginTransaction()
        fragmentTransaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
        fragmentTransaction.replace(R.id.frame, targetFragment, CURRENT_TAG)
        fragmentTransaction.commitAllowingStateLoss()
    }

    fun action(position: Int) {
        when (position) {
            // 0 -> inflateLanguageSelectLayout()

            0 -> loadChangePasswordFragment()

            1 -> sendMail()

            2 -> inflateRatingLayout()

            else -> {
            }
        }
    }

    fun changeLocale(locale: String) {
        val res = getActivity()!!.resources
        // Change locale settings in the app.
        val dm = res.displayMetrics
        val conf = res.configuration
        conf.setLocale(Locale(locale.toLowerCase())) // API 17+ only.
        // Use conf.locale = new Locale(...) if targeting lower versions
        res.updateConfiguration(conf, dm)
    }

    companion object {
        val PREFS_NAME = "logPrefFile"
    }
}// Required empty public constructor