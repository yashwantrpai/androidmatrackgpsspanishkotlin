/*
 * Copyright 2013 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.matrackinc.matrackgpsspanish

import com.matrackinc.matrackgpsspanish.util.MyItem

import java.io.InputStream
import java.util.ArrayList
import java.util.Scanner

import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject

class MyItemReader {

    @Throws(JSONException::class)
    fun read(inputStream: InputStream): List<MyItem> {
        val items = ArrayList<MyItem>()
        val json = Scanner(inputStream).useDelimiter(REGEX_INPUT_BOUNDARY_BEGINNING).next()
        val array = JSONArray(json)
        for (i in 0 until array.length()) {
            var title: String? = null
            var snippet: String? = null
            val `object` = array.getJSONObject(i)
            val lat = `object`.getDouble("lat")
            val lng = `object`.getDouble("lng")
            if (!`object`.isNull("title")) {
                title = `object`.getString("title")
            }
            if (!`object`.isNull("snippet")) {
                snippet = `object`.getString("snippet")
            }
            // items.add(new MyItem(lat, lng, i, , snippet));
        }
        return items
    }

    companion object {

        /*
     * This matches only once in whole input,
     * so Scanner.next returns whole InputStream as a String.
     * http://stackoverflow.com/a/5445161/2183804
     */
        private val REGEX_INPUT_BOUNDARY_BEGINNING = "\\A"
    }

}
