package com.matrackinc.matrackgpsspanish

import android.util.Log
import com.matrackinc.matrackgpsspanish.activity.SplashScreen
import com.squareup.okhttp.FormEncodingBuilder
import com.squareup.okhttp.MediaType
import com.squareup.okhttp.Request
import com.squareup.okhttp.RequestBody
import com.squareup.okhttp.Response
import org.json.JSONObject
import java.io.IOException
import java.io.InputStream

object CustomHttpClient {
    /**
     * The time it takes for our client to timeout
     */
    val HTTP_TIMEOUT = 30 * 1000 // milliseconds

    /**
     * Single instance of our HttpClient
     */
    internal lateinit var ret: String
    private val TAG = "CustomHttpClient"

    /**
     * Get our single instance of our HttpClient object
     * @return an HttpClient object with connection parameters set
     */
    /**
     * Performs an HTTP Post request to the specified url with the
     * specified parameters.
     *
     * @param url The web address to post the request to
     * @return The result of the request
     * @throws Exception
     */

    // Send a POST request
    @Throws(Exception::class)
    fun executeHttpPost(url: String, postString: String): String {
        Log.d("POST Request", url)
        val `in`: InputStream? = null
        var request: Request? = null

        try {
            val body = FormEncodingBuilder()
                    .add("payload", postString)
                    .build()
            request = Request.Builder().url(url).post(body).build()

            val response = SplashScreen.client!!.newCall(request).execute()
            ret = response.body().string()

            // Get cookieManager content
            //Log.d(url, SplashScreen.coreCookieManager.retCookie(url));

        } catch (e: IOException) {
            e.printStackTrace()
        }

        return ret
    }

    // Send a GET request
    @Throws(Exception::class)
    fun executeHttpPost(url: String): String {
        Log.d("GET Request", url)
        val `in`: InputStream? = null
        var request: Request? = null

        try {
            request = Request.Builder().url(url).build()
            val response = SplashScreen.client!!.newCall(request).execute()
            ret = response.body().string()

            // Get cookieManager content
            Log.d(url, SplashScreen.coreCookieManager.retCookie(url))

        } catch (e: IOException) {
            e.printStackTrace()
        }

        return ret
    }
}
