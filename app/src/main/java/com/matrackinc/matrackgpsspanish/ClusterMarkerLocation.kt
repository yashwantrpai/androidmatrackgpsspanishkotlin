package com.matrackinc.matrackgpsspanish

import com.google.android.gms.maps.model.LatLng
import com.google.maps.android.clustering.ClusterItem

class ClusterMarkerLocation(private var position: LatLng?) : ClusterItem {

    override fun getPosition(): LatLng? {
        return position
    }

    override fun getTitle(): String? {
        return null
    }

    override fun getSnippet(): String? {
        return null
    }

    fun setPosition(position: LatLng) {
        this.position = position
    }
}
